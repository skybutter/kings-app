<?php

return [
    'empty_bet' => 'Empty bet',
    'invalid_bet' => 'Invalid bet',
    'comm_exceed_limit' => 'Commission exceed limit'
];
