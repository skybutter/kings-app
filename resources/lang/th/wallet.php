<?php

return [
    'CP' => 'คะแนนเครดิต',
    'RP' => 'คะแนนสะสม',
    'BP' => 'คะแนนโบนัส',
    'PT' => 'คะแนนเงินเดิมพัน',
];
