@extends('layouts.auth')
Kings188
@section('content')

<div class="content shadow-sm position-relative">
    <header class="header">
        
        <nav class="container-fluid">
            <div class="row">
                <div class="col align-self-center">
                    <a href="/" class="logo text-white">
                        <img src="{{asset('logo.png')}}" style="height:40px;" alt="" class="logo-icon">
                        <div class="logo-text">
                            <h5 class="fs22 mb-0">{{config('app.name')}}</h5>
                        </div>
                    </a>
                </div>
                <div class="col text-right align-self-center">
                    <a href="{{route('register')}}"
                        class="btn btn-sm btn-primary text-uppercase">{{__('app.register')}}</a>
                </div>
            </div>
        </nav>
    </header>
    <div class="background">
        <img src="../assets/img/team3.jpg" alt="">
    </div>

    
    <div style="padding-top:4%" class="container main-container" id="main-container">
        <div class="row login-row-height">
            <div class="col-12 col-md-6 col-lg-7 d-none d-md-flex"></div>
            <div class="col-12 col-md-6 col-lg-5 col-xl-4  align-self-center">
                <form class="js-validation-signin" action="{{ url('/login') }}" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="card border-1 bg">
                        <div class="card-body py-5">
                            <h5 class="font-weight-light mb-1 text-mute-high">{{__('app.welcome')}}</h5>
                            <h3 class="font-weight-normal mb-4">{{__('app.login')}}</h3>
                            <div class="card  mb-2 overflow-hidden">
                                <div class="card-body p-0">
                                    <input type="text" class="form-control rounded-0 border-0"
                                        placeholder="{{__('app.username')}}" id="login-username" name="username" />
                                    <hr class="my-0" />
                                    <input type="password" class="form-control rounded-0 border-0"
                                        placeholder="{{__('app.password')}}" id="login-password" name="password" />
                                </div>
                            </div>
                            <div class="my-3 row">
                                <div class="col-12 col-md py-1">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customCheck1"
                                            name="remember" checked="">
                                        <label class="custom-control-label"
                                            for="customCheck1">{{__('app.remember')}}</label>
                                    </div>
                                </div>
                                <div class="col-12 col-md py-1 text-left text-md-right">
                                    <a href="{{url('/password/reset')}}"
                                        class="template-primary">{{__('app.forgot_password')}}</a>
                                </div>
                            </div>
                            <div class="mb-4">
                                <button type="submit" class=" btn btn-primary btn-block">{{__('app.login')}} <i
                                        class="material-icons md-18">arrow_forward</i></a>
                            </div>
                            <div class="">
                                <p class="text-mute">

                                    <!--<a href="{{route('register')}}" class="text-white">{{__('app.register')}}</a>-->
                                    
                                        </br>
                                        
                                            <a class="" href="{{url('/lang/en')}}"><span
                                                    class="flag-icon flag-icon-gb"></span> <span
                                                    class="ml-2">English</span></a>
                                            <a class="" href="{{url('/lang/zh-CN')}}"><span
                                                    class="flag-icon flag-icon-cn"></span> <span
                                                    class="ml-2">中文</span></a>
                                            <a class="" href="{{url('/lang/vi-VN')}}"><span
                                                    class="flag-icon flag-icon-vn"></span> <span class="ml-2">Tiếng
                                                    Việt</span></a>
                                        
                                    
                                </p>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
</div>
@endsection