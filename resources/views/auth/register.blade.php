@extends('layouts.auth')

@section('content')

<div class="content shadow-sm position-relative">
    <header class="header">
        <!-- Fixed navbar -->
        <nav class="container-fluid">
            <div class="row">
                <div class="col align-self-center">
                    <a href="/" class="logo text-white">
                        <img src="{{asset('logo.png')}}" style="height:40px;" alt="" class="logo-icon">
                        <div class="logo-text">
                            <h5 class="fs22 mb-0">{{config('app.name')}}</h5>
                        </div>
                    </a>
                </div>
                <div class="col text-right align-self-center">
                    <a href="{{route('login')}}" class="btn btn-sm btn-primary text-uppercase">{{__('app.login')}}</a>
                </div>
            </div>
        </nav>
    </header>
    <div class="background ">
        <img src="../assets/img/team3.jpg" alt="">
    </div>

    <!-- Main container starts -->
    <div class="container main-container" id="main-container">
        <div class="row login-row-height">
            <div class="col-12 col-md-6 col-lg-7 d-none d-md-flex"></div>
            <div class="col-12 col-md-6 col-lg-5 col-xl-4  align-self-center">
                <div class="card border-0 shadow-lg blur">
                    <form class="js-validation-signup" action="{{ route('register') }}" method="post">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="card-body py-5">
                            <h5 class="font-weight-light mb-1 text-mute-high">{{__('app.register_welcome')}}</h5>
                            <h3 class="font-weight-normal mb-4">{{__('app.register')}}</h3>
                            <div class="card  mb-2 overflow-hidden">
                                <div class="card-body p-0">
                                    <input type="text" name="name" value="{{ old('name') }}"
                                        class="form-control rounded-0 border-0" id="input-name"
                                        placeholder="{{__('app.fullname')}}">
                                    <hr class="my-0">
                                    <input type="text" name="username" value="{{ old('username') }}"
                                        class="form-control rounded-0 border-0" id="input-username"
                                        placeholder="{{__('app.username')}}">
                                    <hr class="my-0">
                                    <input type="email" name="email" value="{{ old('email') }}"
                                        class="form-control rounded-0 border-0" id="input-email"
                                        placeholder="{{__('app.email')}}">
                                    <hr class="my-0">
                                    <input type="password" name="password" value=""
                                        class="form-control rounded-0 border-0" id="input-password"
                                        placeholder="{{__('app.password')}}">
                                    <hr class="my-0">
                                    <input type="password" name="password_confirmation" value=""
                                        class="form-control rounded-0 border-0" id="input-password_confirmation"
                                        placeholder="{{__('app.password_confirmation')}}">
                                    <hr class="my-0">
                                    <input type="text" name="refcode" required="required" readonly value="{{ old('refcode', $refcode) }}"
                                        class="form-control rounded-0 border-0" id="input-refcode"
                                        placeholder="{{__('app.referral_code')}}">
                                </div>
                            </div>

                            <div class="text-left mb-4">
                                <button type="submit" class=" btn btn-primary btn-block" class="material-icons md-18">
                                    {{ __('app.register') }} <i class="material-icons md-18">arrow_forward</i></button>
                            </div>

                            <div class="">
                                <p class="text-mute">

                                    <a class="text-white" href="{{route('login')}}"
                                        class="template-primary">{{__('app.login')}}</a></br>
                                    <a class="" href="{{url('/lang/en')}}"><span
                                    class="flag-icon flag-icon-gb"></span> <span
                                    class="ml-2">English</span></a>
                                    <a class="" href="{{url('/lang/zh-CN')}}"><span
                                    class="flag-icon flag-icon-cn"></span> <span
                                    class="ml-2">中文</span></a>
                                    <!--<a class="" href="{{url('/lang/vi-VN')}}"><span
                                    class="flag-icon flag-icon-vn"></span> <span class="ml-2">Tiếng
                                    Việt</span></a>-->
                                </p>

                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Main container ends -->
</div>
@endsection