@extends('layouts.auth')

@section('content')

<div class="content shadow-sm position-relative">
    <header class="header">
        <!-- Fixed navbar -->
        <nav class="container-fluid">
            <div class="row">
                <div class="col align-self-center">
                    <a href="/" class="logo text-white">
                        <img src="{{asset('logo.png')}}" style="height:40px;" alt="" class="logo-icon">
                        <div class="logo-text">
                            <h5 class="fs22 mb-0">{{config('app.name')}}</h5>
                        </div>
                    </a>
                </div>
                <div class="col text-right align-self-center">
                    <a href="{{route('login')}}" class="btn btn-sm btn-primary text-uppercase">{{__('app.login')}}</a>
                </div>
            </div>
        </nav>
    </header>
    <div class="background opac blur">
        <img src="{{asset('assets/img/team.jpg')}}" alt="">
    </div>

    <!-- Main container starts -->
    <div class="container main-container" id="main-container">
        <div class="row login-row-height">
            <div class="col-12 col-md-6 col-lg-5 col-xl-4 mx-auto align-self-center">
                <div class="card border-0 shadow-lg blur">
                    <div class="card-body py-5">
                        @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                        @endif
                        <form method="POST" action="{{ route('password.update') }}">
                            @csrf
                            <input type="hidden" name="token" value="{{ $token }}">
                            <h5 class="font-weight-light mb-1 text-mute-high">{{__('app.reset_password')}}</h5>
                            <div class="card  mb-2 overflow-hidden">
                                <div class="card-body p-0">
                                    <input type="email" id="inputEmail" name="email"
                                        class="form-control rounded-0 border-0" placeholder="{{__('app.email')}}"
                                        required="" autofocus="" value="{{ $email ?? old('email') }}">
                                    <hr class="my-0">
                                    <input type="text" id="inputUsername" name="username"
                                        class="form-control rounded-0 border-0" placeholder="{{__('app.username')}}"
                                        required="" autofocus="" value="{{ old('username') }}">
                                    <hr class="my-0">
                                    <input type="password" name="password" value=""
                                        class="form-control rounded-0 border-0" id="input-password"
                                        placeholder="{{__('app.password')}}">
                                    <hr class="my-0">
                                    <input type="password" name="password_confirmation" value=""
                                        class="form-control rounded-0 border-0" id="input-password_confirmation"
                                        placeholder="{{__('app.password_confirmation')}}">
                                </div>
                            </div>
                            <div class="mb-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('app.submit') }}
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Main container ends -->
</div>
@endsection


{{--
@extends('layouts.auth')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Reset Password') }}</div>

<div class="card-body">
    <form method="POST" action="{{ route('password.update') }}">
        @csrf

        <input type="hidden" name="token" value="{{ $token }}">

        <div class="form-group row">
            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

            <div class="col-md-6">
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email"
                    value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>

                @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>

        <div class="form-group row">
            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

            <div class="col-md-6">
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror"
                    name="password" required autocomplete="new-password">

                @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>

        <div class="form-group row">
            <label for="password-confirm"
                class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

            <div class="col-md-6">
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required
                    autocomplete="new-password">
            </div>
        </div>

        <div class="form-group row mb-0">
            <div class="col-md-6 offset-md-4">
                <button type="submit" class="btn btn-primary">
                    {{ __('Reset Password') }}
                </button>
            </div>
        </div>
    </form>
</div>
</div>
</div>
</div>
</div>
@endsection
--}}