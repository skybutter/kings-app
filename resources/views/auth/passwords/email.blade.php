@extends('layouts.auth')

@section('content')

<div class="content shadow-sm position-relative">
    <header class="header">
        <!-- Fixed navbar -->
        <nav class="container-fluid">
            <div class="row">
                <div class="col align-self-center">
                    <a href="" class="logo text-white">
                        <img src="{{asset('logo.png')}}" style="height:40px;" alt="" class="logo-icon">
                        <div class="logo-text">
                            <h5 class="fs22 mb-0">{{config('app.name')}}</h5>
                        </div>
                    </a>
                </div>
                <div class="col text-right align-self-center">
                    <a href="{{route('login')}}" class="btn btn-sm btn-primary text-uppercase">{{__('app.login')}}</a>
                </div>
            </div>
        </nav>
    </header>
    <div class="background opac blur">
        <img src="../assets/img/team.jpg" alt="">
    </div>

    <!-- Main container starts -->
    <div class="container main-container" id="main-container">
        <div class="row login-row-height">
            <div class="col-12 col-md-6 col-lg-5 col-xl-4 mx-auto align-self-center">
                <div class="card border-0 shadow-lg blur">
                    <div class="card-body py-5">
                        @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                        @endif
                        <form method="POST" action="{{ route('password.email') }}">
                            @csrf
                            <h5 class="font-weight-light mb-1 text-mute-high">{{__('app.reset_password')}}</h5>
                            <h3 class="font-weight-normal mb-4">{{__('app.enter_username')}}</h3>
                            <div class="card  mb-2 overflow-hidden">
                                <div class="card-body p-0">
                                    <input type="text" id="inputEmail" name="username"
                                        class="form-control rounded-0 border-0" placeholder="{{__('app.username')}}"
                                        required="" autofocus="">
                                </div>
                            </div>
                            <div class="mb-4">
                                <button type="submit" class=" btn btn-primary btn-block">
                                    {{ __('Send Password Reset Link') }} <i
                                        class="material-icons md-18">arrow_forward</i></button>
                            </div>
                            <div class="">
                                <p class="text-mute">
                                    <a class="text-white" href="{{route('login')}}"
                                        class="template-primary">{{__('app.login')}}</a>
                                </p>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Main container ends -->
</div>
@endsection