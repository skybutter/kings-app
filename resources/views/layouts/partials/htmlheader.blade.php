<head>
    <meta charset="utf-8" />
    <title> @yield('htmlheader_title', '') </title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta name="apple-mobile-web-app-title" content="133 Empire">
    <meta name="application-name" content="133 Empire">
    
    <!-- Apple Icon -->
    <link rel="apple-touch-icon" sizes="57x57" href="../assets/img/icon/apple-icon-57x57.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="../assets/img/icon/apple-icon-72x72.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="../assets/img/icon/apple-icon-114x114.png" />
    <link rel="apple-touch-icon" sizes="192x192" href="../assets/img/icon/apple-icon-192x192.png" />


    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Fonts and Styles -->
    @yield('css_before')

    <link rel="shortcut icon" href="{{asset('favicon.ico')}}">

    <!-- g fonts style -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500&display=swap" rel="stylesheet">
    <!-- g fonts style ends -->

    <!-- Vendor or 3rd party style -->

    <!-- material icons -->
    <link href="{{asset('assets/vendor/material-icons/material-icons.css')}}" rel="stylesheet">
    <!-- flags icons -->
    <link href="{{asset('assets/vendor/flags/css/flag-icon.min.css')}}" rel="stylesheet">
    <!-- daterange picker -->
    <link href="{{asset('assets/vendor/daterangepicker-master/daterangepicker.css')}}" rel="stylesheet">

    <!-- Vendor or 3rd party style ends -->

    <!-- Customized template style mandatory -->
    <link href="{{asset('assets/css/style-purple-dark.css')}}" rel="stylesheet" id="stylelink">
    <link href="{{asset('assets/css/custom.css?v=1.1')}}" rel="stylesheet" id="stylelink">
    <!-- Customized template style ends -->
    @yield('css_after')

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode(['csrfToken' => csrf_token(),]) !!};
    </script>
</head>