<!-- Sidebar starts -->
<div class="sidebar">
    <!-- Logo sidebar -->
    <a href="" class="logo">
        <img src="{{asset('logo_trans.png')}}" style="width:50px;" alt="" class="logo-icon">
    </a>
    <!-- Logo sidebar ends -->

    <!-- Navigation menu sidebar-->
    <ul class="nav flex-column">
        <li class="nav-item ">
            <a class="nav-link " href="{{route('admin.wallets')}}"><i
                    class="material-icons icon">exposure</i><span>{{__('app.transfer_credit')}}</span></a>
        </li>
        <li class="nav-item ">
            <a class="nav-link " href="{{route('admin.members')}}"><i
                    class="material-icons icon">people</i><span>{{__('app.members')}}</span></a>
        </li>
        <li class="nav-item ">
            <a class="nav-link " href="{{route('admin.settings')}}"><i
                    class="material-icons icon">pages</i><span>{{__('app.settings')}}</span></a>
        </li>
        <li class="nav-item ">
            <a class="nav-link " href="{{route('admin.admin_statement')}}"><i
                    class="material-icons icon">library_add</i><span>{{__('app.bet_history')}}</span></a>
        </li>
        <li class="nav-item ">
            <a class="nav-link " href="{{route('admin.settings.commissions')}}"><i
                    class="material-icons icon">library_add</i><span>{{__('app.commissions')}}</span></a>
        </li>
        <li class="nav-item ">
            <a class="nav-link " href="{{route('admin.settings.pool')}}"><i
                    class="material-icons icon">grade</i><span>{{__('app.weekly_pool')}}</span></a>
        </li>
        <li class="nav-item ">
            <a class="nav-link " href="{{route('admin.settings.game')}}"><i
                    class="material-icons icon">library_books</i><span>{{__('app.games')}}</span></a>
        </li>
       <li class="nav-item ">
            <a class="nav-link dropdown-toggle" href="javascript:void(0)"><i
                    class="material-icons icon">people</i><span>{{__('app.referral')}}</span> <i
                    class="material-icons arrow">expand_more</i></a>
            <div class="nav flex-column">

                <div class="nav-item"><a class="nav-link"
                        href="{{ route("member.referral") }}"><span>{{__('app.referrallink')}}</span></a>
                </div>
                <div class="nav-item"><a class="nav-link"
                        href="{{ route("member.referral.tree") }}"><span>{{__('app.referral_tree')}}</span></a>
                </div>
            </div>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="javascript:void(0)"><i
                    class="material-icons icon">storage</i><span>{{__('app.approvals')}}</span> <i
                    class="material-icons arrow">expand_more</i></a>
            <div class="nav flex-column">

                <div class="nav-item"><a class="nav-link"
                        href="{{ route("admin.approvals.topup") }}"><span>{{__('app.top_up')}}</span></a></div>
                <div class="nav-item"><a class="nav-link"
                        href="{{ route("admin.approvals.withdrawal") }}"><span>{{__('app.withdrawal')}}</span></a></div>
            </div>
        </li>
        <li class="nav-item ">
            <a class="nav-link " href="{{route('admin.mailbox.inbox')}}"><i
                    class="material-icons icon">help</i><span>{{__('app.mailbox')}}</span></a>
        </li>
        <!--<li class="nav-item ">
            <a class="nav-link " href="/member/dashboard"><i
                    class="material-icons icon">dashboard</i><span>{{__('app.dashboard')}}</span></a>
        </li>-->
    </ul>
</div>
<!-- Sidebar ends -->