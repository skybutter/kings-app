<!-- Global js mandatory -->
<script src="{{ asset('assets/js/jquery-3.3.1.min.js')}}"></script>
<script src="{{asset('assets/js/popper.min.js')}}"></script>
<script src="{{asset('assets/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/vendor/cookie/jquery.cookie.js')}}"></script>
<!-- Global js ends -->

<!-- Vendor or 3rd party js -->

<!-- date range picker -->
<script src="{{asset('assets/vendor/daterangepicker-master/moment.min.js')}}"></script>
<script src="{{asset('assets/vendor/daterangepicker-master/daterangepicker.js')}}"></script>
<!-- Vendor or 3rd party js ends -->

<!-- Customized template js mandatory -->
<script src="{{asset('assets/js/main.js')}}"></script>
<!-- Customized template js ends -->

<!-- theme picker -->
<script src="{{asset('assets/js/style-picker.js')}}"></script>
<!-- theme picker ends -->