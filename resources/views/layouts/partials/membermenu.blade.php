<!-- Sidebar starts -->
<div class="sidebar">
    <!-- Logo sidebar -->
    <a href="" class="logo">
        <img src="{{asset('logo_trans.png')}}" style="width:50px;" alt="" class="logo-icon">
    </a>
    <!-- Logo sidebar ends -->

    <!-- Navigation menu sidebar-->
    <ul class="nav flex-column">
        <li class="nav-item ">
            <a class="nav-link " href="{{route('member.dashboard')}}"><i
                    class="material-icons icon">dashboard</i><span>{{__('app.dashboard')}}</span></a>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="javascript:void(0)"><i
                    class="material-icons icon">games</i><span>{{__('app.games')}}</span> <i
                    class="material-icons arrow">expand_more</i></a>
            <div class="nav flex-column">

                <div class="nav-item"><a class="nav-link"
                        href="{{ route("member.qianzi") }}"><span>{{__('game.GAME_1')}}</span></a></div>
                <div class="nav-item"><a class="nav-link"
                        href="{{ route("member.zodiac") }}"><span>{{__('game.GAME_2')}}</span></a>
                </div>
                <div class="nav-item"><a class="nav-link"
                        href="{{ route("member.zihua") }}"><span>{{__('game.GAME_3')}}</span></a>
                </div>
                <div class="nav-item"><a class="nav-link"
                        href="{{ route("member.horoscope") }}"><span>{{__('game.GAME_4')}}</span></a>
                </div>
            </div>
        </li>
        <li class="nav-item ">
            <a class="nav-link " href="{{route('member.wallet')}}"><i
                    class="material-icons icon">library_books</i><span>{{__('app.wallet')}}</span></a>
        </li>
        <li class="nav-item ">
            <a class="nav-link " href="{{route('member.statement')}}"><i
                    class="material-icons icon">style</i><span>{{__('app.statement')}}</span></a>
        </li>
        <li class="nav-item ">
            <a class="nav-link " href="{{route('member.pool')}}"><i
                    class="material-icons icon">grade</i><span>{{__('app.weekly_pool')}}</span></a>
        </li>
        <li class="nav-item ">
            <a class="nav-link " href="{{route('member.prizestructure')}}"><i
                    class="material-icons icon">explore</i><span>{{__('app.prize_struc')}}</span></a>
        </li>
        <li class="nav-item ">
            <a class="nav-link dropdown-toggle" href="javascript:void(0)"><i
                    class="material-icons icon">people</i><span>{{__('app.referral')}}</span> <i
                    class="material-icons arrow">expand_more</i></a>
            <div class="nav flex-column">

                <div class="nav-item"><a class="nav-link"
                        href="{{ route("member.referral") }}"><span>{{__('app.referrallink')}}</span></a>
                </div>
                <div class="nav-item"><a class="nav-link"
                        href="{{ route("member.referral.tree") }}"><span>{{__('app.referral_tree')}}</span></a>
                </div>
            </div>
        </li>
      
        <li class="nav-item ">
            <a class="nav-link " href="{{route('member.settings.commissions')}}"><i
                    class="material-icons icon">pages</i><span>{{__('app.settings')}}</span></a>
        </li>
        <li class="nav-item ">
            <a class="nav-link " href="{{route('member.mailbox.inbox')}}"><i
                    class="material-icons icon">help</i><span>{{__('app.mailbox')}}</span></a>
        </li>
        <!--<li class="nav-item ">
            <a class="nav-link " href="/admin/dashboard"><i
                    class="material-icons icon">people</i><span>{{__('app.tx_ADMIN')}}</span></a>
        </li>-->
    </ul>
</div>
<!-- Sidebar ends -->