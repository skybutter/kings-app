<!DOCTYPE html>
<html lang="{{ config('app.locale') }}" class="no-focus">

@section('htmlheader')
@include('layouts.partials.htmlheader')
@show

<body class="h-100 no-sidemenu">

    <div class="wrapper">
        @if (count($errors) > 0)
        <div class="row justify-content-center px-5">
            <div class="alert alert-danger">
                <strong>{{__('app.whoops')}}!</strong> {{ __('app.someproblems') }}<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
        @endif
        @yield("content")
        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 col-md text-center text-md-left align-self-center">
                        {{__('app.copyright')}} &copy; 2020
                    </div>

                </div>
            </div>
        </footer>

    </div>
    <!-- wrapper ends -->

    @section('scripts')
    @include('layouts.partials.scripts')
    @show

    @yield('js_after')
</body>

<!-- Body ends -->

</html>