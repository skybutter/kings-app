@extends('layouts.master')
@section('head')
@include('layouts.partials.htmlheader')
@endsection


@section('content_wrapper')
@include('layouts.partials.membermenu')
<!-- wrapper starts -->
<div class="wrapper">
    <div class="content shadow-sm">
        <div class="container-fluid header-container">
            <div class="row header">
                <div class="container-fluid " id="header-container">
                    <div class="row">
                        <!-- Header starts -->
                        <nav class="navbar col-12 navbar-expand ">
                            <button class="menu-btn btn btn-link btn-sm" type="button">
                                <i class="material-icons">menu</i>
                            </button>
                            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                <!-- icons dropwdowns starts -->
                                <ul class="navbar-nav ml-auto">
                                    <!-- flag dropdown-->
                                    <a href="/member/mailbox" style="color:white"><i
                                            class="material-icons">help</i></a>&nbsp;&nbsp;&nbsp;
                                    <!--<a href="https://7starsnet.com/starsnet-01.apk" style="color:white"><i
                                            class="material-icons">cloud_download</i></a>-->
                                    <li class="nav-item dropdown select-flag">
                                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown0" role="button"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <span
                                                class="flag-icon flag-icon-{{\Session::get('locale', 'en') == 'en' ? 'gb' : \Session::get('locale', 'zh-CN') == 'zh-CN' ? 'gb' : 'cn'}}"></span>
                                        </a>
                                        <div class="dropdown-menu" aria-labelledby="navbarDropdown0">
                                            <a class="dropdown-item" href="{{url('/lang/en')}}"><span
                                                    class="flag-icon flag-icon-gb"></span> <span
                                                    class="ml-2">English</span></a>
                                            <a class="dropdown-item" href="{{url('/lang/zh-CN')}}"><span
                                                    class="flag-icon flag-icon-cn"></span> <span
                                                    class="ml-2">中文</span></a>
                                            <a class="dropdown-item" href="{{url('/lang/th')}}"><span
                                                    class="flag-icon flag-icon-th"></span> <span class="ml-2">Thai</span></a>
                                            <a class="dropdown-item" href="{{url('/lang/vn')}}"><span
                                                    class="flag-icon flag-icon-vn"></span> <span class="ml-2">Vietnam</span></a>
                                        </div>
                                    </li>

                                    <!-- message dropdown-->
                                    {{-- <li class="nav-item dropdown">
                                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown5" role="button"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="material-icons">email</i>
                                            <span class="counter bg-danger">1</span>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-sm-left dropdown-menu-center no-defaults pt-0 overflow-hidden"
                                            aria-labelledby="navbarDropdown5">
                                            <div class="position-relative text-center rounded">
                                                <div class="background">
                                                    <img src="/assets/img/background-part.png" alt="">
                                                </div>
                                                <div class="py-3 text-white">
                                                    <h5 class="font-weight-normal">Messages</h5>
                                                    <p>Updates and Status</p>
                                                </div>

                                            </div>
                                            <div class="scroll-y h-320 d-block">
                                                <a class="dropdown-item border-top new" href="#">
                                                    <div class="row">
                                                        <div class="col-auto align-self-center">
                                                            <i
                                                                class="material-icons text-template-primary">local_mall</i>
                                                        </div>
                                                        <div class="col pl-0">
                                                            <div class="row mb-1">
                                                                <div class="col">
                                                                    <p class="mb-0">Admin</p>
                                                                </div>
                                                                <div class="col-auto pl-0">
                                                                    <p class="small text-mute text-trucated mt-1">
                                                                        20/05/2020</p>
                                                                </div>
                                                            </div>
                                                            <p class="small text-mute">Hello</p>
                                                        </div>

                                                    </div>
                                                </a>

                                            </div>
                                        </div>
                                    </li> --}}

                                    <!-- profile dropdown-->
                                    <li class="nav-item dropdown ml-0 ml-sm-4">
                                        <a class="nav-link dropdown-toggle profile-link" href="#" id="navbarDropdown6"
                                            role="button" data-toggle="dropdown" aria-haspopup="true"
                                            aria-expanded="false">
                                            <figure class="rounded avatar avatar-30">
                                                <img src="https://avatars.dicebear.com/v2/jdenticon/{{Auth::user()->username}}.svg"
                                                    alt="">
                                            </figure>
                                            <div class="username-text ml-2 mr-2 d-none d-lg-inline-block">
                                                <h6 class="username"><span>Welcome,</span>{{Auth::user()->username}}</h6>
                                            </div>
                                            <figure class="rounded avatar avatar-30 d-none d-md-inline-block">
                                                <i class="material-icons">expand_more</i>
                                            </figure>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right w-300 pt-0 overflow-hidden"
                                            aria-labelledby="navbarDropdown6">
                                            <div class="position-relative text-center rounded py-5">
                                                <div class="background">
                                                    <img src="{{asset('assets/img/background-part.png')}}" alt="">
                                                </div>
                                            </div>
                                            <div class="text-center mb-3 top-60 z-2">
                                                <figure class="avatar avatar-120 mx-auto shadow"><img
                                                        src="https://avatars.dicebear.com/v2/jdenticon/{{Auth::user()->username}}.svg"
                                                        alt=""></figure>
                                            </div>
                                            <h5 class="text-center mb-0">{{Auth::user()->username}}</h5>
                                            <p class="text-center">{{__('level.LEVEL_'.Auth::user()->level)}}</p>
                                            <a class="dropdown-item border-top" href="{{route('member.profile')}}">
                                                <div class="row">
                                                    <div class="col-auto align-self-center">
                                                        <i class="material-icons text-success">account_box</i>
                                                    </div>
                                                    <div class="col pl-0">
                                                        <p class="mb-0">{{__('app.my_profile')}}</p>
                                                    </div>
                                                    <div class="col-auto align-self-center text-right pl-0">
                                                        <i class="material-icons text-mute small">chevron_right</i>
                                                    </div>
                                                </div>
                                            </a>
                                            <a class="dropdown-item border-top" href="{{route('member.wallet')}}">
                                                <div class="row">
                                                    <div class="col-auto align-self-center">
                                                        <i class="material-icons text-info">account_balance_wallet</i>
                                                    </div>
                                                    <div class="col pl-0">
                                                        <p class="mb-0">{{__('app.my_wallet')}}</p>
                                                    </div>
                                                    <div class="col-auto align-self-center text-right pl-0">
                                                        <i class="material-icons text-mute small">chevron_right</i>
                                                    </div>
                                                </div>
                                            </a>
                                            <a class="dropdown-item border-top" href="{{ url('/logout') }}" onClick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                                                <div class="row">
                                                    <div class="col-auto align-self-center">
                                                        <i class="material-icons text-danger">exit_to_app</i>
                                                    </div>
                                                    <div class="col pl-0">
                                                        <p class="mb-0 text-danger">{{__('app.logout')}}</p>
                                                    </div>
                                                    <div class="col-auto align-self-center text-right pl-0">
                                                        <i
                                                            class="material-icons text-mute small text-danger">chevron_right</i>
                                                    </div>
                                                </div>
                                            </a>
                                            <form id="logout-form" action="{{ url('/logout') }}" method="POST"
                                                style="display: none;">
                                                {{ csrf_field() }}
                                                <input type="submit" value="logout" style="display: none;">
                                            </form>
                                        </div>
                                    </li>
                                </ul>
                                <!-- icons dropwdowns starts -->
                            </div>
                        </nav>
                        <!-- Header ends -->
                    </div>
                </div>
            </div>
        </div>

        <!-- Main container starts -->
        <div class="container main-container" id="main-container">
            @include('layouts.partials.flash-message')
            @yield('content')
        </div>
        <!-- Main container ends -->
    </div>

    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-md text-center text-md-left align-self-center">
                    {{__('app.copyright')}} &copy; 2020
                </div>

            </div>
        </div>
    </footer>

</div>
<!-- wrapper ends -->
@endsection