{{$name}} 您好!

<p>您的账号收到更换密码请求。</p>

<p>
    如果是您本人发出的请求， 请点此链接更换：<br>
    <a href="{{$link}}">{{$link}}</a>
</p>

<p>如果有任何询问欢迎电邮至 <a href="mailto:support@133empire.ga">support@133empire.ga</a>。</p>
<p>&nbsp;</p>
<p>贏勝皇朝</p>


Hello {{$name}}!<br>
<p>There was recently a request to change the password for your
    account.</p>

<p>If you requested this password change, please click on the
    following link to reset your password: <a href="{{$link}}">Click to Reset</a></p>

<p>If clicking the link doesn't work, please copy and paste the URL into your browser</p>

<p>LINK: {{$link}}</p>

<p>If you do not perform the request, please contact <a
        href="mailto:support@133empire.ga">support@133empire.ga</a>
    <p>&nbsp;</p>
    <p>Regards,<br>133 EMPIRE</p>