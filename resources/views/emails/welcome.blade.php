{{$user->name}} 您好!

<p>热烈欢迎您加入贏勝皇朝</p>

<p>您的账号已创建于 {{Carbon\Carbon::now()->format('Y-m-d')}}， 以下是您的账号资料:</p>

<p>用户名: {{$user->username}}<br>
    密码 : {{$rawpassword}}<br>
    安全密码: {{$user->withdrawkey}}<br>
    网站: <a href="133empire.ga">133empire.ga</a><br>
</p>
<p>为了您账号的安全，希望您登录后尽快更换密码。</p>
<p>如果有任何询问欢迎电邮至 <a href="mailto:support@133empire.ga">support@133empire.ga</a>。</p>
<p>&nbsp;</p>
<p>贏勝皇朝</p>



Hi {{$user->name}}!

<p>Congratulations and welcome to 133 Empire</p>

<p>You have been successfully registered with 133 Empire on {{Carbon\Carbon::now()->format('Y-m-d')}}. Please view your
    account
    details below:</p>

<p>User ID: {{$user->username}}<br>
    Password : {{$rawpassword}}<br>
    Security Pin: {{$user->withdrawkey}}<br>
    Login Website: <a href="133empire.ga/">133empire.ga</a><br>
</p>
<p>For security purpose , you are strongly advised to change your login password and security password immediately after
    your first login.</p>
<p>If you need help at any time or if you have any questions , do feel free to contact us by replying to this message or
    email us at <a href="mailto:support@133empire.ga">support@133empire.ga</a> directly.</p>
<p>&nbsp;</p>
<p>Regards,<br>133 Empire</p>