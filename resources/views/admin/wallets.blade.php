@extends('layouts.admin')


@section('content')
<div class="mb-2">
    <div class="row">
        <div class="col-12 col-md-6 col-lg-6">
            <div class="card border-0 shadow-sm overflow-hidden mb-4">
                <div class="card-body py-0">
                    <div class="row">
                        <div class="col py-2">
                            <h3>{{ __('app.search_wallet') }}</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col py-2">
                            <form method="GET" action="">
                                @csrf
                                <div class="row ">
                                    <div class="col-9">
                                        <div class="form-group ">
                                            <input type="text" class="form-control"
                                                placeholder="{{ __('app.username') }}" name="username"
                                                value="{{ $username }}" />
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <button type="submit"
                                            class="btn btn-primary">{{ __('app.submit') }}</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card border-0 shadow-sm overflow-hidden mb-4">
                <div class="card-body py-0">
                    <div class="row">
                        <div class="col py-2 text-left">
                            <h3>{{ __('app.member') }}{{ __('app.wallet') }}</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col py-2">

                            <table class="table datatable display responsive w-100">
                                <thead>
                                    <tr>
                                        <th>{{ __('app.username') }}</th>
                                        <th>{{ __('app.type') }}</th>
                                        <th>{{ __('app.amount') }}</th>
                                        <th>{{ __('app.adjust') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($wallets as $row)
                                        <tr>
                                            <td>{{ $row->user->username }}</td>
                                            <td>{{ __('wallet.'.$row->type) }}</td>
                                            <td>{{ number_format($row->balance,2) }}</td>
                                            <td width="250px">
                                                <div class="d-flex ">
                                                    <div class="form-group d-flex">
                                                        <input type="number" step="1.00" class="form-control mr-2"
                                                            placeholder="{{ __('app.amount') }}"
                                                            id="amount-{{ $row->id }}" />
                                                        <button type="button"
                                                            data-username="{{ $row->user->username }}"
                                                            data-id="{{ $row->id }}"
                                                            class="btn btn-primary mb-2 btn-transfer">
                                                            {{ __('app.submit') }}
                                                        </button>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade " id="transferModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered " role="document">
        <div class="modal-content box-shadow">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{ __('app.transfer_confirmation') }}
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="form-adjust" method="POST" action="{{ route('admin.wallets.adjust') }}">
                    @csrf
                    <div class="form-group">
                        <label style="color:#000"
                            class="col-md-9 control-label">{{ __('app.username') }}</label>
                        <div class="col-md-9">
                            <input id="input-wallet" name="wallet" type="hidden" />
                            <input type="text" class="form-control" disabled id="input-username" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label style="color:#000"
                            class="col-md-9 control-label">{{ __('app.amount') }}</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" readonly="readonly" id="input-amount" name="amount" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label style="color:#000"
                            class="col-md-9 control-label">{{ __('app.transfer_password') }}</label>
                        <div class="col-md-9">
                            <input type="password" class="form-control" id="input-password" type="password" name="password" />
                        </div>
                    </div>
            </div>
            <div class="box-footer mb-4">
                <div class="col-8">
                    <button type="submit" class="btn btn-primary pull-right">Submit</button>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
</div>
@endsection

@section('js_after')
<script type="text/javascript">
    $(document).ready(function () {
        $('.btn-transfer').click(function () {
            var walletId = $(this).data('id')
            $('#input-wallet').val(walletId)
            $('#input-amount').val($('#amount-' + walletId).val());
            $('#input-username').val($(this).data('username'))
            $('#transferModal').modal();
        })
    })

</script>
@endsection
