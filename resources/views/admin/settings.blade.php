@extends('layouts.admin')


@section('content')
<div class="mb-2">
    <div class="row">
        <div class="col-12">
            <div class="card border-0 shadow-sm overflow-hidden mb-4">
                <div class="card-body py-0">
                    <div class="row">
                        <div class="col py-2 text-left">
                            <h3>{{__('app.settings')}}</h3>
                        </div>
                    </div>
                    <form class="form-horizontal" method="POST" action="{{route('admin.settings.update')}}">
                        @csrf
                        @method('PATCH')
                        <div class="form-group">
                            <label class="col-md-9 control-label">{{__('app.payment_method')}}</label>
                            <div class="col-md-9">
                                <textarea rows="10" class="form-control" name="paymentMethods"
                                   >{{$paymentMethodsInput}}
                                   </textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-9 control-label">{{__('app.transfer_password')}}</label>
                            <div class="col-md-9">
                                <input type="password"  class="form-control" name="transfer_pwd" />
                            </div>
                        </div>
                        
                        <div class="box-footer">
                            <div class="col-8">
                            <button type="submit" class="btn btn-primary pull-right">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
