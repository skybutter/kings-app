@extends('layouts.admin')


@section('content')
<div class="mb-2">
    <div class="row">
        <div class="col-12">
            <div class="card border-0 shadow-sm overflow-hidden mb-4">
                <div class="card-body py-0">
                    <div class="row">
                        <div class="col py-2 text-left">
                            <h3>{{__('app.weekly_pool')}}</h3>
                        </div>
                    </div>
                    <form class="form-horizontal" method="POST" action="{{route('admin.settings.updatePool')}}">
                        @csrf
                        @method('PATCH')
                        <div class="box-body">
                            <div class="form-group my-2">
                                <label class="col-8 control-label">{{__('app.pool_rate')}}</label>
                                <div class="col-8">
                                    <div class="input-group">
                                        <input type="number" step="0.01" class="form-control" name="pool"
                                            value="{{$pool}}" autocomplete="off">
                                        <div class="input-group-append">
                                            <button type="button" class="btn btn-secondary">%</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group my-2">
                                <label class="col-8 control-label">{{__('app.pool_total')}}</label>
                                <div class="col-8">
                                    <div class="input-group">
                                        <input type="number" step="0.01" class="form-control" name="poolTotal"
                                            value="{{$poolTotal}}" autocomplete="off">
                                    </div>
                                </div>
                            </div>

                            @foreach($poolVals as $k => $v)
                            <div class="form-group my-2">
                                <label class="col-8 control-label">{{__('app.day_'.($k+1))}}</label>
                                <div class="col-8">
                                    <div class="input-group">
                                        <input type="number" step="0.01" class="form-control" name="poolVals[{{$k}}]"
                                            value="{{$v}}" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                            @endforeach

                        </div>
                        <div class="box-footer mt-4">
                            <div class="col-8">
                            <button type="submit" class="btn btn-primary pull-right">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    
</div>
@endsection