@extends('layouts.admin')


@section('content')
<div class="mb-2">
    <div class="row">
        <div class="col-12">
            <div class="card border-0 shadow-sm overflow-hidden mb-4">
                <div class="card-body py-0">
                    <div class="row">
                        <div class="col py-2 text-center">
                            <h3>{{__('app.approvals')}} - {{__('app.withdrawal')}}</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col py-2">
                            <input id="input-amount" name="amount" type="hidden" />
                            <input id="input-wallet" name="wallet" type="hidden" />

                            <table class="table datatable display responsive w-100">
                                <thead>
                                    <tr>
                                        <th>{{__('app.date')}}</th>
                                        <th>{{__('app.status')}}</th>
                                        <th>{{__('app.member')}}</th>
                                        <th>{{__('app.amount')}}</th>
                                        <th>{{__('app.payment_method')}}</th>
                                        <th>{{__('app.proof_payment')}}</th>
                                        <th>{{__('app.action')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($approvals as $approval)
                                    <tr>
                                        <td>{{Carbon\Carbon::parse($approval['created_at'])->format('m/d H:i')}}
                                        </td>
                                        <td>@include('partials.approvalstatus', ['status' => $approval['status']])</td>
                                        <td>{{$approval['user']['name']}}<br>{{$approval['user']['email']}}</td>
                                        <td>{{number_format($approval['content']['amount'],2)}}</td>
                                        <td>{{$approval['content']['details']}}
                                        <td>
                                            @if($approval['content'])
                                            @if(isset($approval['content']['PAYMENT_TXID']))
                                            TX: <b>{{$approval['content']['PAYMENT_TXID']}}</b>
                                            @endif
                                            @if(isset($approval['content']['PAYMENT_FILE']))
                                            <br>
                                            <a href="{{ asset('storage/'.$approval['content']['PAYMENT_FILE'])}}"
                                                target="_blank"><img
                                                    src="{{ asset('storage/'.$approval['content']['PAYMENT_FILE'])}}"
                                                    class="attachment-img" /></a>
                                            @endif
                                            @endif
                                        </td>
                                        <td width="250px">
                                            <div class="d-flex ">
                                                @if($approval['status'] == 0)
                                                <div class="form-group d-flex">
                                                    <a href="{{ route('admin.approvals.withdrawalpayment', $approval['id'])}}"
                                                        class="btn btn-block btn-success mr-2">{{__('app.approve')}}</a>

                                                    <form
                                                        action="{{ route('admin.approvals.withdrawal_update', $approval['id'])}}"
                                                        method="post">
                                                        @csrf
                                                        @method("PATCH")
                                                        <input name="status" type="hidden" value="2" />
                                                        <button class="btn btn-danger"
                                                            type="submit">{{__('app.reject')}}</button>
                                                    </form>
                                                </div>
                                                @endif
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js_after')
<link href="{{asset('assets/vendor/DataTables-1.10.18/css/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
<link href="{{asset('assets/vendor/DataTables-1.10.18/css/responsive.dataTables.min.css')}}" rel="stylesheet">
<script src="{{asset('assets/vendor/DataTables-1.10.18/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/vendor/DataTables-1.10.18/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('assets/vendor/DataTables-1.10.18/js/dataTables.bootstrap4.min.js')}}"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('.datatable').DataTable({
            "bLengthChange": true,
            'responsive': true,
                'searching': true,
                "bInfo" : true,
                "order": [[ 0, "desc" ]]
        });
    })
</script>
@endsection