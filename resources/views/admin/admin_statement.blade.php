@extends('layouts.admin')


@section('content')
<div class="mb-2">

    <div class="row">
        <div class="col-12">
            <div class="card border-0 shadow-sm overflow-hidden mb-4">
                <div class="col-8">
                    <h4>{{__('app.bet_history')}}</h4>
                    <!--<h4>{{$topplayer}}</h4>-->
                </div>
            </div>
            
        <div class="row">
            <div class="col-12 col-md-3">
                <div class="card border-1 shadow mb-4 ">
                    <div class="card-body py-0 text-center p-4">
                    {{__('app.total_bet')}} : {{$totalbetamount}}</br>
                    <span class="small">Start from: 29-05-2020</span>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-3">
                <div class="card border-1 shadow mb-4 ">
                    <div class="card-body py-0 text-center p-4">
                    {{__('game.GAME_1')}} : {{$game1betamount}}</br>
                    <span class="small">Start from: 29-05-2020</span>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-3">
                <div class="card border-1 shadow mb-4 ">
                    <div class="card-body py-0 text-center p-4">
                    {{__('game.GAME_2')}} : {{$game2betamount}}</br>
                    <span class="small">Start from: 29-05-2020</span>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-3">
                <div class="card border-1 shadow mb-4 ">
                    <div class="card-body py-0 text-center p-4">
                    {{__('game.GAME_3')}} : {{$game3betamount}}</br>
                    <span class="small">Start from: 29-05-2020</span>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-3">
                <div class="card border-1 shadow mb-4 ">
                    <div class="card-body py-0 text-center p-4">
                    {{__('game.GAME_4')}} : {{$game4betamount}}</br>
                    <span class="small">Start from: 29-05-2020</span>
                    </div>
                </div>
            </div>
        </div>    
                
                
                    <table class="table datatable display responsive w-100">
                        <thead>
                            <tr>
                                <th>{{__('app.date')}}</th>
                                <th>{{__('app.username')}}</th>
                                <th>{{__('app.detail')}}</th>
                                <th>{{__('app.results')}}</th>
                                <th>{{__('app.total_bet')}}</th>
                                <th>{{__('app.profit')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                         @foreach($bets as $row)    
                            <tr>
                                <td>{{Carbon\Carbon::parse($row->created_at->setTimezone('Asia/Kuala_Lumpur'))->format('m/d H:i')}}</td>
                                <td>{{$row->user->username}}</td>
                                <td>
                                    <div class="d-flex flex-row">
                                        <div class="btn-rounded-circle btn btn-info sgame mr-2">
                                            {{__('game.SGAME_'.$row->game)}}
                                        </div>
                                        <div class="d-flex justify-content-center align-items-center text-left">
                                            {!!$row->details!!}

                                            <div data-container="body" data-trigger="hover" data-toggle="popover"
                                                class="bet-info" data-placement="top" 
                                                data-html="true" data-content="{{$row->info}}">
                                                <i class="material-icons md-18 ml-2">info</i>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td>{{$row->resultStr}}</td>
                                <td>{{number_format($row->amount,2)}}</td>
                                <td>{{number_format($row->winnings,2)}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js_after')
<link href="{{asset('assets/vendor/DataTables-1.10.18/css/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
<link href="{{asset('assets/vendor/DataTables-1.10.18/css/responsive.dataTables.min.css')}}" rel="stylesheet">
<script src="{{asset('assets/vendor/DataTables-1.10.18/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/vendor/DataTables-1.10.18/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('assets/vendor/DataTables-1.10.18/js/dataTables.bootstrap4.min.js')}}"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('.datatable').DataTable({
            "bLengthChange": false,
            'responsive': true,
                'searching': false,
                "bInfo" : false,
                "order": [[ 0, "desc" ]]
        });
      $('[data-toggle="popover"]').popover();
    })
</script>
@endsection