<a href="{{route('admin.mailbox.compose')}}" class="btn btn-primary btn-block m-1">{{__('app.compose')}}</a>
<!--<a  href="{{route('admin.mailbox.compose', ['recipient' => '*'])}}" class="btn btn-primary btn-block m-1">Make Announcement</a>-->
</br>
<div class="">

    <div class="card border-0 shadow-sm">
        <ul class="nav flex-column">
            <li class="{{ request()->is('admin/mailbox') ? 'active' : '' }}">
                <a class="btn btn-info btn-full p-2 m-1" href="{{route('admin.mailbox.inbox')}}">{{__('app.inbox')}}
                    <span class="">-{{$unreadCount}}</span></a>
            </li>
            
            <li class="{{ request()->is('admin/mailbox/outbox') ? 'active' : '' }}">
                <a class="btn btn-info btn-full m-1" href="{{route('admin.mailbox.outbox')}}">{{__('app.sent')}}</a>
            </li>
            
            <!--<li class="{{ request()->is('admin/mailbox/announcements') ? 'active' : '' }}">
                <a class="btn btn-info btn-full m-1" href="{{route('admin.mailbox.announcements')}}">Announcements</a
            ></li>-->
        </ul>
    </div>
</div>