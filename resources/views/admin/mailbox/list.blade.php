@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="card border-1 shadow-sm mb-4 p-4">
        @include('admin.mailbox.sidebar', ['unreadCount' => $unreadCount])
    </div>
    <div class="col-12 col-md-9 text-center">
        <div class="card border-1 shadow-sm overflow-hidden mb-4 p-4">
            <div class="">
                <h3>{{$title}}</h3>
            </div>

            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <table id="table-threads" class="table datatable display responsive w-100">
                        <thead>
                            @if(!$isAnnouncement)
                            <th></th>
                            @endif
                            <th></th>
                            <th></th>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{{__('app.username')}}</td>
                                <td>{{__('app.subject')}}</td>    
                                <td>{{__('app.message')}}</td>
                            </tr>
                            @foreach($threads as $thread)
                            <tr class="{{ ($thread['pivot']['seen_at'] || $isAnnouncement) ? '' : 'mail-unread'}}">
                                @if(!$isAnnouncement)
                                <td class="mailbox-name"><a href="{{route('admin.mailbox.show', $thread['id'])}}">{{$thread['user']['username']}}</a></td>
                                @endif
                                <td class="mailbox-subject">
                                    <span><a href="{{route('admin.mailbox.show', $thread['id'])}}">{{$thread['subject']}}</a></span>
                                </td>
                                <td class="mailbox-date">
                                    {{Carbon\Carbon::parse($thread['created_at'])->timezone('Asia/Kuala_Lumpur')->format('Y-m-d H:i:s')}}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footerscripts')
<script>
    $('#table-threads').DataTable({
        bSort:false
    });
    $('#table-threads tbody').show();
</script>
@endsection