@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="card border-1 shadow-sm mb-4 p-4">
        @include('admin.mailbox.sidebar', ['unreadCount' => $unreadCount])
    </div>
    <div class="col-12 col-md-9">
            <div class="card border-1 shadow-sm mb-4 p-4">
              <div class="box-header with-border">
                    <a class="btn btn-primary" href="{{URL::previous()}}">{{__('app.back')}}</a>
                    @if(!$isAnnouncement)
                    <a href="{{route('admin.mailbox.compose', ['subject' => 'Re: '.$thread['subject'], 'recipient' => $target['username']])}}" class="btn btn-primary">{{__('app.reply')}}</a>
                    @endif
                    <a href="{{route('admin.mailbox.delete', $thread['id'])}}" class="btn btn-primary">{{__('app.delete')}}</a>
              </div>
              <!-- /.box-header -->
              <div class="col-lg-12 col-md-12 p-4">
                <div class="">
                    <h4>
                        @if(!$isAnnouncement)
                        {{$target['id'] == $message['user_id'] ? 'From' : 'To'}}: {{$target['username']}}
                    </h4>
                        @endif
                    <h5>
                        <span class="">
                            {{Carbon\Carbon::parse($message['created_at'])->format('Y-m-d H:i:s')}}
                        </span>
                    </h5>
                    </br>
                    <h3>{{$thread['subject']}}</h3>
                  
                </div>
                <div class="mailbox-read-message">
                  {!! $message['body'] !!}
                </div>
              </div>

              <!-- /.box-footer -->
              
              <!-- /.box-footer -->
            </div>
            <!-- /. box -->
          </div>
        </div>
</div>
@endsection