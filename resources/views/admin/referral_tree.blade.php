@extends('layouts.admin')


@section('content')
<link rel="stylesheet" href="{{ asset('assets/vendor/jstree/themes/default/style.min.css')}}">
<div class="row">
    <div class="col-12">
        <div class="card border-1 shadow-sm mb-4">
            <div class="background-half text-template-primary-opac-10"></div>
            <div class="card-body text-left">

                <h4 class="font-weight-light mb-0 btn btn-info">{{__('app.referral_tree')}}</h4>


                <div class="row mt-5">
                    <div class="col-12">
                        <div id="chart-container"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js_after')
<script src="{{ asset('assets/vendor/jstree/jstree.min.js')}}"></script>
<script>
    rootTree = {!! $rootTree !!};
        validNames = {!! $validNames !!};
        $('#chart-container').jstree({
            'core' : {
                'data' : rootTree
            }
        }).on('loaded.jstree',function(e, data){
            data.instance.open_node($('.jstree-node:first', data.instance.element));
        }).on('refresh.jstree',function(e, data){
            data.instance.open_node($('.jstree-node:first', data.instance.element));
        });
    
    function searchTree(){
        var username = $('#input-search').val();
        if(username.length == 0){
            username = '{{$username}}';
        }
        if(validNames.indexOf(username) === -1){
            alert("Member not found");
        }
        else{
            var d = getData(username, [rootTree]);
            $('#chart-container').jstree(true).settings.core.data = d;
            $('#chart-container').jstree(true).refresh();
        }
        return false;
    }
    
    function getData(name, org){
        for(var i = 0; i < org.length; i++){
            var node = org[i];
            if(node.text == name){
                return node;
                break;
            }
            else
            {
                if(node.children){
                    var n = getData(name, node.children);
                    if(n){
                        return n;
                    }
                }
            }
        }
    }
        
</script>

@endsection