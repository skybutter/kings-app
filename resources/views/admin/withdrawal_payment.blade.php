@extends('layouts.admin')


@section('content')
<div class="mb-2">
    <div class="row">
        <div class="col-12">
            <div class="card border-0 shadow-sm overflow-hidden mb-4">
                <div class="card-body py-0">
                    <div class="row">
                        <div class="col py-2 text-center">
                            <h3>{{__('app.approvals')}} - {{__('app.withdrawal')}}</h3>
                        </div>
                    </div>
                    <form class="form-horizontal" method="POST" enctype="multipart/form-data"
                        action="{{route('admin.approvals.withdrawal_update', $id)}}">
                        @csrf
                        @method('PATCH')
                        <input type="hidden" name="status" value="1" />
                        <div class="box-body attachments">
                            <div class="form-group">
                                <label for="input-txid" class="col-md-3 control-label">{{__('app.txid')}}</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="txid" id="txid" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input-txfile"
                                    class="col-md-3 control-label">{{__('app.payment_image')}}</label>
                                <div class="col-md-9">
                                    <input type="file" class="form-control" name="txfile" id="txfile">
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary pull-right">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection