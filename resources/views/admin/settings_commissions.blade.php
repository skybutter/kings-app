@extends('layouts.admin')


@section('content')
<div class="mb-2">
    <div class="row">
        <div class="col-12">
            <div class="card border-0 shadow-sm overflow-hidden mb-4">
                <div class="card-body py-0">
                    <div class="row">
                        <div class="col py-2 ">
                            <div class="col-8">
                                <h4>{{__('app.commissions')}}</h4>
                            </div>
                            
                        </div>
                    </div>
                    <form class="form-horizontal" method="POST" action="{{route('admin.settings.updateCommissions')}}">
                        @csrf
                        @method('PATCH')
                        <div class="box-body ">
                            <div class="form-group my-2">
                                <label class="col-8 control-label">{{__('level.LEVEL_0')}}</label>
                                <div class="col-8">

                                    <div class="input-group">
                                        <input type="number" step="0.01" class="form-control" name="commissions[comm_0]"
                                            value="{{$commissions['comm_0'] ?? ''}}" autocomplete="off">
                                        <div class="input-group-append">
                                            <button type="button" class="btn btn-secondary">%</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group my-2">
                                <label class="col-8 control-label">{{__('level.LEVEL_1')}}</label>
                                <div class="col-8">
                                    <div class="input-group">
                                        <input type="number" step="0.01" class="form-control" name="commissions[comm_1]"
                                            value="{{$commissions['comm_1'] ?? ''}}" autocomplete="off">
                                        <div class="input-group-append">
                                            <button type="button" class="btn btn-secondary">%</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group my-2 mb-4">
                                <label class="col-8 control-label">{{__('level.LEVEL_2')}}</label>
                                <div class="col-8">
                                    <div class="input-group">
                                        <input type="number" step="0.01" class="form-control" name="commissions[comm_2]"
                                            value="{{$commissions['comm_2'] ?? ''}}" autocomplete="off">
                                        <div class="input-group-append">
                                            <button type="button" class="btn btn-secondary">%</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group my-2 mb-4">
                                <label class="col-8 control-label">{{__('app.comm_max')}}</label>
                                <div class="col-8">
                                    <div class="input-group">
                                        <input type="number" step="0.01" class="form-control"
                                            name="commissions[comm_max]" value="{{$commissions['comm_max'] ?? ''}}"
                                            autocomplete="off">
                                        <div class="input-group-append">
                                            <button type="button" class="btn btn-secondary">%</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group my-2 mb-4">
                                <label class="col-8 control-label">{{__('app.comm_extra')}}</label>
                                <div class="col-8">
                                    <div class="input-group">
                                        <input type="number" step="0.01" class="form-control"
                                            name="commissions[comm_extra]" value="{{$commissions['comm_extra'] ?? ''}}"
                                            autocomplete="off">
                                        <div class="input-group-append">
                                            <button type="button" class="btn btn-secondary">%</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <div class="col-8">
                                <button type="submit" class="btn btn-primary pull-right">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection