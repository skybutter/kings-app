@extends('layouts.admin')


@section('content')
<div class="mb-2">
    <div class="row">
        <div class="col-12">
            <div class="card border-0 shadow-sm overflow-hidden mb-4">
                <div class="card-body py-0">
                    <div class="row">
                        <div class="col py-2 text-left">
                            <h3>{{__('app.games')}}</h3>
                        </div>
                    </div>
                    <form class="form-horizontal" method="POST" action="{{route('admin.settings.updateGame')}}">
                        @csrf
                        @method('PATCH')
                        <div class="box-body">
                            <div class="mt-4">
                                <div class="col-8">
                                <h4>{{__('game.GAME_1')}}<h4>
                                </div>
                            </div>
                            <div class="form-group my-2">
                                <label class="col-8 control-label">{{__('app.max_bet')}}</label>
                                <div class="col-8">
                                    <input type="number" step="0.01" class="form-control" name="maxbet[1]"
                                        value="{{$maxbet['1']}}" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group my-2">
                                <label class="col-8 control-label">{{__('app.qianzi_blacklist')}}</label>
                                <div class="col-8">
                                    <textarea name="qianzi_blacklist" class="form-control">{{$blacklist}}</textarea>
                                </div>
                            </div>

                            <div class="mt-4">
                                <div class="col-8">
                                <h4>{{__('game.GAME_2')}}<h4>
                                </div>
                            </div>
                            <div class="form-group my-2">
                                <label class="col-8 control-label">{{__('app.max_bet')}}</label>
                                <div class="col-8">
                                    <input type="number" step="0.01" class="form-control" name="maxbet[2]"
                                        value="{{$maxbet['2']}}" autocomplete="off">
                                </div>
                            </div>

                            <div class="mt-4">
                                <div class="col-8">
                                <h4>{{__('game.GAME_3')}}<h4>
                                </div>
                            </div>
                            <div class="form-group my-2">
                                <label class="col-8 control-label">{{__('app.max_bet')}}</label>
                                <div class="col-8">
                                    <input type="number" step="0.01" class="form-control" name="maxbet[3]"
                                        value="{{$maxbet['3']}}" autocomplete="off">
                                </div>
                            </div>

                            <div class="mt-4">
                                <div class="col-8">
                                <h4>{{__('game.GAME_4')}}<h4>
                                </div>
                            </div>
                            <div class="form-group my-2">
                                <label class="col-8 control-label">{{__('app.max_bet')}}</label>
                                <div class="col-8">
                                    <input type="number" step="0.01" class="form-control" name="maxbet[4]"
                                        value="{{$maxbet['4']}}" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group my-2">
                                <label class="col-8 control-label">{{__('app.winrate')}}</label>
                                <div class="col-8">
                                    <input type="number" step="1" class="form-control" name="horoscope_winrate"
                                        value="{{$horoscopeWinrate}}" autocomplete="off">
                                </div>
                            </div>

                        </div>
                        
                        <div class="box-body">
                            <div class="mt-4">
                                <div class="col-8">
                                <h4>{{__('game.GAME_5')}}<h4>
                                </div>
                            </div>
                        <div class="form-group my-2">
                            <label class="col-md-9 control-label">{{__('app.winrate')}}</label>
                            <div class="col-md-9">
                                <div class="row p-1">
                                    <div class="col-4">
                                        1000
                                    </div>
                                    <div class="col-8">
                                    <input type="text"  class="form-control" name="spins[0]" value="{{$spins[0]}}" />
                                    </div>
                                </div>

                                <div class="row p-1">
                                    <div class="col-4">
                                        300
                                    </div>
                                    <div class="col-8">
                                    <input type="text"  class="form-control" name="spins[1]" value="{{$spins[1]}}" />
                                    </div>
                                </div>

                                <div class="row p-1">
                                    <div class="col-4">
                                        50
                                    </div>
                                    <div class="col-8">
                                    <input type="text"  class="form-control" name="spins[2]" value="{{$spins[2]}}" />
                                    </div>
                                </div>

                                <div class="row p-1">
                                    <div class="col-4">
                                        500
                                    </div>
                                    <div class="col-8">
                                    <input type="text"  class="form-control" name="spins[3]" value="{{$spins[3]}}" />
                                    </div>
                                </div>

                                <div class="row p-1">
                                    <div class="col-4">
                                        100
                                    </div>
                                    <div class="col-8">
                                    <input type="text"  class="form-control" name="spins[4]" value="{{$spins[4]}}" />
                                    </div>
                                </div>

                                <div class="row p-1">
                                    <div class="col-4">
                                        10
                                    </div>
                                    <div class="col-8">
                                    <input type="text"  class="form-control" name="spins[5]" value="{{$spins[5]}}" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                        <div class="box-footer mt-4">
                            <div class="col-8">
                            <button type="submit" class="btn btn-primary pull-right">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection