@extends('layouts.member')


@section('content')
<div class="mb-2">
    <div class="row mb-2">
        <div class="col-12">
            <a href="{{route('member.statement')}}" class="btn btn-primary">{{__('app.bet_history')}}</a>
            <button class="btn btn-info">{{__('app.commission')}}</button>
            <a href="{{route('member.wallet')}}" class="btn btn-primary">{{__('app.transactions')}}</a>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card border-0 shadow-sm overflow-hidden mb-4">
                <div class="card-body py-0 text-center p-4">
                    <table class="table datatable display responsive w-100">
                        <thead>
                            <tr>
                                <th>{{__('app.detail')}}</th>
                                <th>{{__('app.date')}}</th>
                                <th>{{__('app.time')}}</th>
                                <th>{{__('app.commission')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $row)
                            <tr>
                                <td>{{$row->details}}</td>
                                <td>{{Carbon\Carbon::parse($row->created_at->setTimezone('Asia/Kuala_Lumpur'))->format('d-m-Y')}}
                                </td>
                                <td>{{Carbon\Carbon::parse($row->created_at->setTimezone('Asia/Kuala_Lumpur'))->format('H:i')}}
                                </td>
                                <td>
                                    {{$row->amount}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js_after')
<link href="{{asset('assets/vendor/DataTables-1.10.18/css/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
<link href="{{asset('assets/vendor/DataTables-1.10.18/css/responsive.dataTables.min.css')}}" rel="stylesheet">
<script src="{{asset('assets/vendor/DataTables-1.10.18/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/vendor/DataTables-1.10.18/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('assets/vendor/DataTables-1.10.18/js/dataTables.bootstrap4.min.js')}}"></script>

<script src="{{asset('assets/vendor/dropzone-master/dropzone.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.datatable').DataTable({
            "bLengthChange": false,
            'responsive': true,
                'searching': false,
                "bInfo" : false,
                "order": [[ 1, "desc" ]]
        });
      
    })
</script>
@endsection