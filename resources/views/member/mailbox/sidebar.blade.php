<a href="{{route('member.mailbox.compose')}}" class="btn btn-primary btn-block m-1">{{__('app.compose')}}</a>

</br>
<div class="">

    <div class="card border-0 shadow-sm">
        <ul class="nav flex-column">
            <li class="{{ request()->is('member/mailbox') ? 'active' : '' }}">
                <a class="btn btn-info btn-full p-2 m-1" href="{{route('member.mailbox.inbox')}}">{{__('app.inbox')}}
                    <span class="text-template-primary-light small">-{{$unreadCount}}</span></a>
            </li>
            
            <li class="{{ request()->is('member/mailbox/outbox') ? 'active' : '' }}">
                <a class="btn btn-info btn-full m-1" href="{{route('member.mailbox.outbox')}}">{{__('app.sent')}}</a>
            </li>
            
            <!--<li class="{{ request()->is('member/mailbox/announcements') ? 'active' : '' }}">
                <a class="btn btn-info btn-full m-1" href="{{route('member.mailbox.announcements')}}">Announcements</a
            ></li>-->
        </ul>
    </div>
</div>