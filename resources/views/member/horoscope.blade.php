@extends('layouts.member')


@section('content')
<div class="row justify-content-center">
   <div class="col-12 my-4 col-md-8 col-lg-6 text-center mb-4">
      <div class="card border-1 shadow-sm h-100">
         <div class="card-body p-5">

         @if(!is_null(session('winres')))
            @if(session('winres'))
            <div id="bet-result" class="alert alert-success alert-block">
            @else
            <div id="bet-result" class="alert alert-warning alert-block">
            @endif
               <button type="button" class="close" data-dismiss="alert">×</button>
               {{__('game.horoscope_result', ['result' => __('game.HOROSCOPE_'.session('winnum'))])}}
            </div>
         @endif

            <form method="POST" action="{{route('member.horoscope_submit')}}">
               <input name="valid" type="hidden" id="input-valid" value="1" />
               <input type="hidden" name="_token" value="{{ csrf_token() }}">

               <h3 class="">
                    <img style="width:80px;height:80px;" src="../assets/img/xinzuo-1.png"></img>
                    </i>
                </h3>

               <h5>{{__('game.GAME_4')}}</h5>
               <p>{{__('game.horoscopedetail')}}</p>
               <h3>{{__('game.pay')}}: <span id="total">0.00</span></h3>
               <div class="my-2 row mb-4 gameboard">
                  @for($i = 1; $i <= 12; $i++) <div class="col-4 col-md-3 p-1">
                     <div class="">{{__('game.HOROSCOPE_'.$i)}}</br>
                        
                     </div>
                     
                     <input type="number" class="form-control rounded-0 border-1 val" step="1.00"
                        name="val[{{$i-1}}]" />
               </div>
               @endfor
         </div>

         <button class="btn btn-primary mt-4" id="btn-submit"
            style="width:80%; height:50px;">{{__('app.submit')}}</button>
         <div style="display:none" id="bet-error" class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong id="bet-error-message"></strong>
         </div>
         </form>
      </div>
   </div>
</div>
</div>
@endsection

@section('js_after')

<script type="text/javascript">
   @if($maxbet)
   var maxBet = {{$maxbet}};
   @else
   var maxBet = 9999999;
   @endif
   function inputValid(){
      $('#bet-error').hide();
      $('#btn-submit').show();
      $('#input-valid').val("1")
   }

   function inputInvalid(){
      $('#bet-error').show();
      $('#bet-error-message').text("{{__('app.bet_exceed_limit')}}");
      $('#btn-submit').hide();
      $('#input-valid').val("0");
   }

   function checkInput(){
   var total = 0;

   $('.val').each(function(idx, ele){
      if($(ele).val()){
         total += parseFloat($(ele).val());
      }

   })
   $('#total').text(total.toFixed(2))
   if(parseFloat(total) > maxBet){
      inputInvalid();
   }
   else{
      inputValid();
   }
   }
   $(document).ready(function(){
      $('.val').on('change',function(val){
      checkInput();
      })
   })
</script>
@endsection
