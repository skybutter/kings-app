@extends('layouts.member')


@section('content')
<div class="row">
    <div class="col-12">
        <div class="card border-1 shadow-sm mb-4">
            <div class="background-half text-template-primary-opac-10"></div>
            <div class="card-body text-left">

                <h4 class="font-weight-light mb-0 btn btn-info">{{__('app.affiliate_program')}}</h4>
                <!--<p class="mt-4 text-template-primary-light  mb-2">{{__('app.affiliate_program_desc')}}-->
                </p>
                <h4><a href="#" onclick="copyToClipboard('#share-link');return false;"><span
                            id="share-link">{{$reflink}}</span><i id="share-icon"
                            class="material-icons icon ">content_copy</i></a>
                </h4>

                <div class="row mt-5">
                    <div class="col-12">
                        <table class="table datatable display responsive">
                            <thead>
                                <tr>
                                    <th>{{__('app.username')}}</th>
                                    <th>{{__('app.register_date')}}</th>
                                    <th>{{__('app.status')}}</th>
                                    <th>{{__('app.total_bet')}}</th>
                                    <th>{{__('app.commission')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($downlines as $user)
                                <tr>
                                    <td>{{$user->username}}</td>
                                    <td>{{Carbon\Carbon::parse($user->created_at->setTimezone('Asia/Kuala_Lumpur'))->format('d-m-Y')}}
                                    </td>
                                    <td>{{__('level.LEVEL_'.$user->level)}}</td>
                                    <td>{{number_format($user->total_bet ?? 0,2)}}</td>
                                    <td>{{number_format($user->commission ?? 0,2)}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js_after')
<link href="{{asset('assets/vendor/DataTables-1.10.18/css/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
<link href="{{asset('assets/vendor/DataTables-1.10.18/css/responsive.dataTables.min.css')}}" rel="stylesheet">
<script src="{{asset('assets/vendor/DataTables-1.10.18/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/vendor/DataTables-1.10.18/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('assets/vendor/DataTables-1.10.18/js/dataTables.bootstrap4.min.js')}}"></script>
<script>
    $(document).ready(function(){
        $('.datatable').DataTable({
        "bLengthChange": false,
        'responsive': true,
        'searching': true,
        "bInfo" : false,
        "order": [[ 0, "desc" ]]
        });
    })
    function copyToClipboard(element) {
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val($(element).text()).select();
        document.execCommand("copy");
        $temp.remove();
        alert("{{__('app.reflink_copied')}}");
    }
</script>

@endsection