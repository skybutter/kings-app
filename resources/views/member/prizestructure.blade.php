@extends('layouts.member')


@section('content')
<div class="row">
    <div class="col-12 col-md-6 col-lg-6 col-xl-4">
        <div class="card border-1 shadow-sm mb-4">
            <div class="background-half text-template-primary-opac-10 h-200"></div>
            <div class="card-body text-center">
                <p class="text-template-primary-light small mb-2">{{__('app.prize_struc')}}</p>
                <h3 class="">
                    <img style="width:80px;height:80px;" src="../assets/img/qianzi-5.png">
                </h3>
                <h4 class="font-weight-light mb-0">{{__('game.GAME_1')}}</h4>
                <div class="row mt-3">
                    <div class="col-12 col-md-11 mx-auto mw-300">

                        <table class="table datatable display responsive w-100">
                            <tbody>
                                {{__('game.GAME_struc')}}
                                <tr>
                                    <td>{{__('game.Category')}}</td>
                                    <td>{{__('game.Prize')}}</td>
                                </tr>
                                <tr>
                                    <td>3D</td>
                                    <td>500</td>
                                </tr>
                                <tr>
                                    <td>2D</td>
                                    <td>50</td>
                                </tr>
                                <tr>
                                    <td>1D</td>
                                    <td>5</td>
                                </tr>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-6 col-lg-6 col-xl-4">
        <div class="card border-1 shadow-sm mb-4">
            <div class="background-half text-template-primary-opac-10 h-200"></div>
            <div class="card-body text-center">
                <p class="text-template-primary-light small mb-2">{{__('app.prize_struc')}}</p>
                <h3 class="">
                    <img style="width:80px;height:80px;" src="../assets/img/zodiac-5.png">
                </h3>
                <h4 class="font-weight-light mb-0">{{__('game.GAME_2')}}</h4>
                <div class="row mt-3">
                    <div class="col-12 col-md-11 mx-auto mw-300">

                        <table class="table datatable display responsive w-100">
                            <tbody>
                                {{__('game.GAME_struc')}}
                                <tr>
                                    <td>{{__('game.Category')}}</td>
                                    <td>{{__('game.Prize')}}</td>
                                </tr>
                                <tr>
                                    <td>{{__('game.GAME_2')}}</td>
                                    <td>6.00</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-6 col-lg-6 col-xl-4">
        <div class="card border-1 shadow-sm mb-4">
            <div class="background-half text-template-primary-opac-10 h-200"></div>
            <div class="card-body text-center">
                <p class="text-template-primary-light small mb-2">{{__('app.prize_struc')}}</p>
                <h3 class="">
                    <img style="width:80px;height:80px;" src="../assets/img/zihua-5.png">
                </h3>
                <h4 class="font-weight-light mb-0">{{__('game.GAME_3')}}</h4>
                <div class="row mt-3">
                    <div class="col-12 col-md-11 mx-auto mw-300">

                        <table class="table datatable display responsive w-100">
                            <tbody>
                                {{__('game.GAME_struc')}}
                                <tr>
                                    <td>{{__('game.lucky_number')}}</td>
                                    <td>{{__('game.Prize')}}</td>
                                </tr>
                                <tr>
                                    <td>1-36</td>
                                    <td>30.00</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-6 col-lg-6 col-xl-4">
        <div class="card border-1 shadow-sm mb-4">
            <div class="background-half text-template-primary-opac-10 h-200"></div>
            <div class="card-body text-center">
                <p class="text-template-primary-light small mb-2">{{__('app.prize_struc')}}</p>
                <h3 class="">
                    <img style="width:80px;height:80px;" src="../assets/img/xinzuo-1.png">
                </h3>
                <h4 class="font-weight-light mb-0">{{__('game.GAME_4')}}</h4>
                <div class="row mt-3">
                    <div class="col-12 col-md-11 mx-auto mw-300">

                        <table class="table datatable display responsive w-100">
                            <tbody>
                                {{__('game.GAME_struc')}}
                                <tr>
                                    <td>{{__('game.Category')}}</td>
                                    <td>{{__('game.Prize')}}</td>
                                </tr>
                                <tr>
                                    <td>{{__('game.GAME_4')}}</td>
                                    <td>6.00</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</div>

@endsection