@extends('layouts.member')


@section('content')
<div class="mb-2">
    <div class="row">
        <div class="col-12">
            <div class="card border-0 shadow-sm overflow-hidden mb-4">
                <div class="card-body py-0">
                    <div class="row">
                        <div class="col py-2 text-center">
                            <h3>{{__('app.members')}}</h3>
                            <span>Total Members: {{count($members)}}</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col py-2">
                            <input id="input-amount" name="amount" type="hidden" />
                            <input id="input-wallet" name="wallet" type="hidden" />
                            <table class="table datatable display responsive w-100">
                                <thead>
                                    <tr>
                                        <th>{{__('app.username')}}</th>
                                        <th>{{__('app.email')}}</th>
                                        <th>{{__('app.fullname')}}</th>
                                        <th>{{__('app.status')}}</th>
                                        <th>{{__('app.commission')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($members as $member)
                                    <tr>
                                        <td>{{$member->username}}</td>
                                        <td>{{$member->email}}</td>
                                        <td>{{$member->name}}</td>
                                        <td>{{__('level.LEVEL_'.$member->level)}}</td>
                                        <td>
                                            <div class="d-flex ">

                                                <div class="form-group d-flex">
                                                    <form action="{{ route('member.settings.updateCommission', ['id' => $member->id])}}"
                                                        method="post">
                                                        @csrf
                                                        @method("PATCH")
                                                        <div class="input-group">
                                                            <input type="number" step="0.01" class="form-control" name="commission"
                                                                value="{{$member->commission ?? ''}}" autocomplete="off">
                                                            <div class="input-group-append">
                                                                <button type="button" class="btn btn-secondary">%</button>
                                                            </div>
                                                        </div>
                                                        <br />
                                                        <button class="btn btn-success mr-2"
                                                            type="submit">{{__('app.submit')}}</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js_after')
<link href="{{asset('assets/vendor/DataTables-1.10.18/css/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
<link href="{{asset('assets/vendor/DataTables-1.10.18/css/responsive.dataTables.min.css')}}" rel="stylesheet">
<script src="{{asset('assets/vendor/DataTables-1.10.18/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/vendor/DataTables-1.10.18/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('assets/vendor/DataTables-1.10.18/js/dataTables.bootstrap4.min.js')}}"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('.datatable').DataTable({
            "bLengthChange": true,
            'responsive': true,
                'searching': true,
                "bInfo" : true,
                "order": [[ 0, "desc" ]]
        });
    })
</script>
@endsection