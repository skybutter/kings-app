@extends('layouts.member')


@section('content')
<div class="row justify-content-center">
   <div class="col-12 my-4 col-md-8 col-lg-6 text-center mb-4">
      <div class="card border-1 shadow-sm h-100">
         <div class="card-body p-5">
            <form method="POST" action="{{route('member.zodiac_submit')}}">
               <input name="valid" type="hidden" id="input-valid" value="1" />
               <input type="hidden" name="_token" value="{{ csrf_token() }}">
               
               <h3 class="">
                    <img style="width:80px;height:80px;" src="../assets/img/zodiac-5.png"></img>
                    </i>
                </h3>
               
               <h5>{{__('game.GAME_2')}}</h5>
               <p>{{__('game.zodiacdetail')}}</p>
               <h3>{{__('game.pay')}}: <span id="total">0.00</span></h3>
               <div class="my-2 row mb-4 gameboard">
                  @for($i = 1; $i <= 12; $i++) <div class="col-4 col-md-3 p-1">
                     <div class="">{{__('game.ZODIAC_'.$i)}}</br>
                        <p><img src="{{__('game.IMG_'.$i)}}" />
                     </div>
                     </p>
                     <input type="number" class="form-control rounded-0 border-1 val" step="1.00"
                        name="val[{{$i-1}}]" />
               </div>
               @endfor
         </div>
         <h5>{{__('app.next_result_in')}}</h5>
         <h3 id="countdown"></h3>
         <button class="btn btn-primary mt-4" id="btn-submit"
            style="width:80%; height:50px;">{{__('app.submit')}}</button>
         <div style="display:none" id="bet-error" class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong id="bet-error-message"></strong>
         </div>
         </form>
      </div>
   </div>
</div>
<div class="col-12 col-md-4 col-results">
   <p class="fs15">{{__('app.results')}}</p>
   <ul class="list-results">
      @foreach($gameResults as $res)
      <li class="row my-1">
         <div class="col-8">
            <div class="text-muted">
               {{ Carbon\Carbon::parse($res->created_at->setTimezone('Asia/Kuala_Lumpur'))->format('H:i') }}</div>
            <p><a href="https://etherscan.io/block/{{$res->hash}}" target="_blank">{{$res->hash}}</a></p>
         </div>
         <div class="col-4 col-num">
            {{__('game.ZODIAC_'.$res->zodiac)}}<p><img src="{{__('game.IMG_'.$res->zodiac)}}" /></p>
         </div>
      </li>
      @endforeach
   </ul>
</div>
</div>
@endsection

@section('js_after')
<script src="{{asset('assets/js/jquery.countdown.min.js')}}"></script>
<script type="text/javascript">
   @if($maxbet)
   var maxBet = {{$maxbet}};
   @else
   var maxBet = 9999999;
   @endif
   function inputValid(){
   $('#bet-error').hide();
   $('#btn-submit').show();
   $('#input-valid').val("1")
   }
   
   function inputInvalid(){
   $('#bet-error').show();
   $('#bet-error-message').text("{{__('app.bet_exceed_limit')}}");
   $('#btn-submit').hide();
   $('#input-valid').val("0");
   }
   
   function checkInput(){
   var total = 0;
   
   $('.val').each(function(idx, ele){
   if($(ele).val()){
   total += parseFloat($(ele).val());
   }
   
   })
   $('#total').text(total.toFixed(2))
   if(parseFloat(total) > maxBet){
   inputInvalid();
   }
   else{
   inputValid();
   }
   }

   function setupCountdown(dt){
      $('#countdown').countdown(dt, function(event) {
      $(this).html(event.strftime('%M:%S'));
      }).on('finish.countdown', function(){
         var nextdate = new Date();

         nextdate.setMilliseconds(0);
         nextdate.setSeconds(0);
         nextdate.setMinutes(Math.ceil((nextdate.getMinutes() + 1) / 10) * 10);

         setupCountdown(nextdate);
      });
   }
   $(document).ready(function(){
      $('.val').on('change',function(val){
      checkInput();
      })
      setupCountdown(nextOpen());
   })
</script>
@endsection