@extends('layouts.member')


@section('content')
<div class="row">
    <div class="col-12">
        <div class="card shadow-sm border-0 mb-4">
            <form class="mb-4" action="{{ route('member.profile.update')}}" method="post">
                @csrf
                <div class="card-header p-0 overflow-hidden">
                    <div class="row no-gutters align-items-center position-relative gradient-pink">
                        <figure class="background opac">
                            <img src="../assets/img/background-part.png" alt="" class="">
                        </figure>
                        <div class="container p-4">
                            <div class="row align-items-center ">
                                <div class="col-12 col-sm-auto text-center">
                                    <a href="profile.html">
                                        <figure class="avatar avatar-150 rounded-circle mx-auto my-3">
                                            <img src="https://avatars.dicebear.com/v2/jdenticon/{{$user->username}}.svg"
                                                alt="">
                                        </figure>
                                    </a>
                                </div>
                                <div class="col-12 col-sm text-center text-sm-left text-white">
                                    <h3 class="mb-0">{{$user->username}}</h3>
                                    <p><i class="material-icons vm mr-2 fs15">call</i> {{$user->name}}<span
                                            class="mx-2">|</span>
                                        <i class="material-icons vm mr-2 fs15">mail_outline</i> {{$user->email}}</p>
                                    <div>{{__('app.referral_link')}}</div>
                                    <h5><a href="#" onclick="copyToClipboard('#share-link');return false;"><span
                                                id="share-link">{{$reflink}}</span><i id="share-icon"
                                                class="material-icons icon ">content_copy</i></a>
                                    </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row justify-content-center">

                        @method("PATCH")
                        <div class="col-md-10 col-lg-8">
                            <div class="form-group row">
                                <div class="col-6">
                                    <label>{{__('app.username')}}</label>
                                    <input disabled type="text" value={{$user->username}} class="form-control"
                                        placeholder="">
                                </div>
                                <div class="col-6">
                                    <label>{{__('app.status')}}</label>
                                    <input disabled type="text" value={{__('level.LEVEL_'.$user->level)}}
                                        class="form-control" placeholder="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-6 col-md-6">
                                    <label>{{__('app.email')}}</label>
                                    <input type="email" name="email" class="form-control" value="{{$user->email}}"
                                        placeholder="">
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <label>{{__('app.fullname')}}</label>
                                    <input type="text" name="name" class="form-control" value="{{$user->name}}"
                                        placeholder="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-6 col-md-6">
                                    <label>{{__('app.bank_acc')}}</label>
                                    <input type="text" name="bank_acc" class="form-control" value="{{$user->bank_acc}}"
                                        placeholder="">
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <label>{{__('app.usdt_acc')}}</label>
                                    <input type="text" name="usdt_acc" class="form-control" value="{{$user->usdt_acc}}"
                                        placeholder="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-6 col-md-6">
                                    <label>{{__('app.new_password')}}</label>
                                    <input type="password" name="new_password" class="form-control" value=""
                                        placeholder="">
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <label>{{__('app.password_confirmation')}}</label>
                                    <input type="password" name="new_password_confirmation" class="form-control"
                                        value="" placeholder="">
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary float-right">{{__('app.save')}}</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('js_after')
<script>
    function copyToClipboard(element) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($(element).text()).select();
    document.execCommand("copy");
    $temp.remove();
    alert("{{__('app.reflink_copied')}}");
    }
</script>
@endsection