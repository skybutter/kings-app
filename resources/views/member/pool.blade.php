@extends('layouts.member')


@section('content')
<div class="row">
    <div class="col-12 col-md-6 col-lg-6 col-xl-4">
        <div class="card border-1 shadow-sm mb-4">
            <div class="background-half text-template-primary-opac-10 h-200"></div>
            <div class="card-body text-center">
                <p class="text-template-primary-light small mb-2">{{__('app.weekly_pool')}}</p>
                <h4 class="font-weight-light mb-0">{{$total}}</h4>
                <div class="row mt-3">
                    <div class="col-12 col-md-11 mx-auto mw-300">

                        <table class="table datatable display responsive w-100">
                            <tbody>
                                @foreach($buckets as $k => $row)
                                <tr>
                                    <td>{{__('app.day_'.$k)}}</td>
                                    <td>{{($row)}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection