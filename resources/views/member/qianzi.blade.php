@extends('layouts.member')


@section('content')
<div class="row justify-content-center qianzi">
   <div class="col-12 my-4 col-md-8 col-lg-6 text-center mb-4">
      <div class="card border-1 shadow-sm h-100">
         <div class="card-body p-5">
            <form method="POST" action="{{route('member.qianzi_submit')}}">
               <input type="hidden" name="_token" value="{{ csrf_token() }}">
               
               <h3 class="">
                    <img style="width:80px;height:80px;" src="../assets/img/qianzi-5.png"></img>
                    </i>
                </h3>
                
               <h5>{{__('game.GAME_1')}}</h5>
               <p>{{__('game.qianzidetail')}}</p>
               <h3>{{__('game.pay')}}: <span id="total">0.00</span></h3>
               <input name="valid" type="hidden" id="input-valid" value="1" />

               <div class="my-2 row mb-4 gameboard">
                  <div class="row">
                     <label>{{__('game.lucky_number')}}</label>
                     <input max="999" type="number" id="input-num" class="form-control rounded-0 border-1"
                        onkeyup="checkInput();" name="num" />
                  </div>
                  <div class="row my-2">
                     <div class="col-2">
                        <label for="d-3d">{{__('game.3D')}}</label>
                        <input id="d-3d" type="checkbox" name="d[2]" class="form-control rounded-0 border-0 val" />
                     </div>
                     <div class="col-2">
                        <label for="d-2d">{{__('game.2D')}}</label>
                        <input id="d-2d" type="checkbox" name="d[1]" class="form-control rounded-0 border-0 val" />
                     </div>
                     <div class="col-2">
                        <label for="d-1d">{{__('game.1D')}}</label>
                        <input id="d-1d" type="checkbox" name="d[0]" class="form-control rounded-0 border-0 val" />
                     </div>
                     <div class="col-6 text-left">
                        <label for="bet">{{__('game.bet')}}</label>
                        <input id="bet" onkeyup="checkInput();" type="number" step="1.00"
                           class="form-control rounded-0 border-1" name="bet" />
                     </div>
                  </div>
               </div>
               <h5>{{__('app.next_result_in')}}</h5>
               <h3 id="countdown"></h3>
               <button class="btn btn-primary mt-4" id="btn-submit"
                  style="width:80%; height:50px;">{{__('app.submit')}}</button>
               <div style="display:none" id="bet-error" class="alert alert-danger alert-block">
                  <button type="button" class="close" data-dismiss="alert">×</button>
                  <strong id="bet-error-message"></strong>
               </div>
            </form>
         </div>
      </div>
   </div>
   <div class="col-12 col-md-4 col-results">
      <p class="fs15">{{__('app.results')}}</p>
      <ul class="list-results">
         @foreach($gameResults as $res)
         <li class="row my-1">
            <div class="col-9">
               <div class="text-muted">
                  {{ Carbon\Carbon::parse($res->created_at->setTimezone('Asia/Kuala_Lumpur'))->format('H:i') }}</div>
               <p><a href="https://etherscan.io/block/{{$res->hash}}" target="_blank">{{$res->hash}}</a></p>
            </div>
            <div class="col-3 col-num">
               <h4>{{$res->qianzi}}</h4>
            </div>
         </li>
         @endforeach
      </ul>
   </div>
</div>
@endsection

@section('js_after')
<script src="{{asset('assets/js/jquery.countdown.min.js')}}"></script>
<script type="text/javascript">
   var qianzi_blacklist = "{{$qianzi_blacklist}}".split(';');
@if($maxbet)
var maxBet = {{$maxbet}};
@else
var maxBet = 9999999;
@endif

function inputValid(){
   $('#bet-error').hide();
   $('#btn-submit').show();
   $('#input-valid').val("1")
}

function inputInvalid(){
   $('#bet-error').show();
   $('#bet-error-message').text("{{__('app.bet_exceed_limit')}}");
   $('#btn-submit').hide();
   $('#input-valid').val("0");
}

function checkInput(){
      var total = 0;
      var bet = parseFloat($('#bet').val())
      if(bet){
      var checked = $('.val:checked').length;
         total = checked * bet;
      }
      
      $('#total').text(total.toFixed(2));

      if(qianzi_blacklist.length > 0){
         if(qianzi_blacklist.indexOf($('#input-num').val().trim()) > -1){
            inputInvalid();
            return;
         }
         else{
            inputValid();
         }
      }

      if(parseFloat(total) > maxBet){
         inputInvalid();
      }
      else{
         inputValid();
      }
   }

   function setupCountdown(dt){
      $('#countdown').countdown(dt, function(event) {
      $(this).html(event.strftime('%M:%S'));
      }).on('finish.countdown', function(){
         var nextdate = new Date();

         nextdate.setMilliseconds(0);
         nextdate.setSeconds(0);
         nextdate.setMinutes(Math.ceil((nextdate.getMinutes() + 1) / 10) * 10);

         setupCountdown(nextdate)
      });
   }
   $(document).ready(function(){
      $('.val').on('change',function(val){
         checkInput();
      })
     setupCountdown(nextOpen());
   })
</script>
@endsection