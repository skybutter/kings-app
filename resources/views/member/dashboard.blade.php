@extends('layouts.member')

@section('content')
<link rel="stylesheet" href="{{ asset('assets/vendor/spin/css/style.css?v=1') }}">

<!--<div class="row">
  <div class="col-12 ">
    <div class="card border-1 shadow-sm mb-4">
      <div class="card-header border-0 bg-none">
        <div class="row">
          <div class="col">
            <div class="col text-center">
              <h3>{{ __('app.spin_title') }}</h3>
            </div>
          </div>
        </div>
      </div>

      <div class="card-body p-15">
        <div class="row">
          <div class="col-10 offset-1 text-center">

            <div id="spinContainer">
              @if($canPlaySpin && $spinBalance > 0)
                <button class="spinBtn btn btn-primary">{{__('app.click_to_spin')}}</button>
              @else
                <input type="hidden" class="spinBtn" />
              @endif
                <br />

              <div class="wheelContainer">
                <svg class="wheelSVG" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" text-rendering="optimizeSpeed" preserveAspectRatio="xMidYMin meet">
                  <defs>
                    <filter id="shadow" x="-100%" y="-100%" width="550%" height="550%">
                      <feOffset in="SourceAlpha" dx="0" dy="0" result="offsetOut"></feOffset>
                      <feGaussianBlur stdDeviation="9" in="offsetOut" result="drop" />
                      <feColorMatrix in="drop" result="color-out" type="matrix" values="0 0 0 0   0 0 0 0 0   0 0 0 0 0   0 0 0 0 .3 0" />
                      <feBlend in="SourceGraphic" in2="color-out" mode="normal" />
                    </filter>
                  </defs>
                    <g class="mainContainer">
                      <g class="wheel"></g>
                    </g>
                    <g class="centerCircle" />
                    <g class="wheelOutline" />
                    <g class="pegContainer" opacity="1">
                      <path class="peg" fill="#EEEEEE" d="M22.139,0C5.623,0-1.523,15.572,0.269,27.037c3.392,21.707,21.87,42.232,21.87,42.232 s18.478-20.525,21.87-42.232C45.801,15.572,38.623,0,22.139,0z" />
                    </g>
                    <g class="valueContainer" />
                    <g class="centerCircleImageContainer" />
                </svg>
                <div class="toast"><p></p></div>
              </div>

            </div>

            <div class="row">
              <div class="col">
                <div class="card border-1 shadow-sm mb-4 walletop">
                  <div class="card-body">
                    <div class="row">
                      <div class="col text-center">
                        <h3><i class="material-icons"></i>{{$intro}}</h3>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col">
                        <form method="POST" action="{{route('member.wallet.convert')}}">
                          @csrf
                          <div class="row">
                            <div class="col-6">
                              <div class="form-group ">
                                <label >Balance</label>
                                <select class="form-control" id="input-convert_wallet" name="wallet">
                                  <option value="{{$wallets['CP']['id']}}">Spin Point: {{ $wallets['SP']['balance'] }}</option>
                                </select>
                              </div>
                            </div>

                            <div class="col-6">
                              <div class="form-group ">
                                <label >Amount</label>
                                <input type="number" step="1.00" class="form-control" name="amount" />
                              </div>
                            </div>
                          </div>

                          <div class="row ">
                            <div class="col">
                              <div class="form-group ">
                                <button type="submit" class="btn btn-primary btn-w100">Convert to CP</button>
                              </div>
                            </div>
                          </div>

                          <div class="row ">
                            <div class="col">
                              <span>{{ __('game.spin_desc') }}</span>
                              <h4>
                                <br />
                                <span id="share-link">{{$reflink}}</span>
                                <a href="#" onclick="copyToClipboard('#share-link');return false;">
                                  <i id="share-icon" class="material-icons icon">content_copy</i>
                                </a>
                              </h4>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>

    </div>
  </div>
</div>-->

<div class="row">

  <div class="col-12 col-md-6 col-lg-6 col-xl-6">
    <div class="card border-1 shadow-sm mb-4">
      <div class="background-half text-template-primary-opac-10 h-200"></div>
      <div class="card-header border-2 bg-none">
        <div class="row">
          <div class="col">
            <p class="fs15">{{ __('app.dashboard') }}</p>
          </div>
        </div>
      </div>
      <div class="card-body text-center">
        <div class="row mt-3">
            <div class="col-12 col-md-11 mx-auto mw-300">
                <div class="row">
                    <div class="col-6">
                        <div class="card border-2 shadow mb-4 ">
                            <div class="card-body text-center">
                                <i class="material-icons">attach_money</i>
                                <p class="text-template-primary-light small mb-1">
                                    {{ __('wallet.CP') }} </p>
                                <h6 class="mb-0">
                                    {{ $wallets['CP']['balance'] }}
                                </h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="card border-2 shadow mb-4 ">
                            <div class="card-body text-center">
                                <i class="material-icons">all_inclusive</i>
                                <p class="text-template-primary-light small mb-1">
                                    {{ __('wallet.RP') }}</p>
                                <h6 class="mb-0">
                                    {{ $wallets['RP']['balance'] }}
                                </h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="card border-2 shadow mb-3 ">
                            <div class="card-body text-center">
                                <i class="material-icons">casino</i>
                                <p class="text-template-primary-light small mb-1">
                                    {{ __('wallet.BP') }}</p>
                                <h6 class="mb-0">
                                    {{ $wallets['BP']['balance'] }}
                                </h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="card border-2 shadow mb-3 ">
                            <div class="card-body text-center">
                                <i class="material-icons">star</i>
                                <p class="text-template-primary-light small mb-1">
                                    {{ __('wallet.PT') }}</p>
                                <h6 class="mb-0">{{ number_format($totalPoints, 2) }}</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>

  <div class="col-12 col-md-6 col-lg-6 col-xl-6">
    <div class="card border-1 shadow-sm mb-4">
      <div class="card-body p-4">
        <div class="text-center" style="padding:86px 0">
          <h5>
            {{ __('app.nextdraw') }}
          </br></br>
            <script>
              document.write(new Date().toLocaleDateString("zh-Hans-CN"));
            </script>
            <p>
              <?php
                date_default_timezone_set("Asia/Kuala_Lumpur");
                $signintime = date("H:i");
                define('SIGN_IN', $signintime);
                function minutes_round($hour = SIGN_IN, $minutes = 10, $format = "H:i")
                {
                  $seconds = strtotime($hour);
                  $rounded = ceil($seconds / ($minutes * 60)) * ($minutes * 60);
                  return date($format, $rounded);
                }
                echo minutes_round();
                date_default_timezone_set("UTC");
              ?>
            </p>
          </h5>
          <h1 id="countdown"></h1>
        </div>
      </div>
    </div>
  </div>

  <div class="col-12 col-xl-6 gamesgroup">
    <a href="{{ route('member.qianzi') }}" style="color: white; text-decoration: none;">
      <div class="card border-2 shadow-sm mb-4 text-center">
        <div class="card-body p-0">
          <div class="row gamerow">
            <div class="col-7 gamedesc">
              <h5>Qian Zi</h5>
              <h5>{{ __('game.GAME_1T') }}</h5>
              <span style="color: grey">{{ __('app.play_now') }}</span>
            </div>
            <div class="col-5">
              <p class="btn btn-img btn-full">
                <img style="width:80px;height:80px;" src="../assets/img/qianzi-5.png"></img>
              </p>
            </div>
          </div>
        </div>
      </div>
    </a>
  </div>

  <div class="col-12 col-xl-6 gamesgroup">
    <a href="{{ route('member.zodiac') }}" style="color: white; text-decoration: none;">
      <div class="card border-2 shadow-sm mb-4 text-center">
        <div class="card-body p-0">
          <div class="row gamerow">
            <div class="col-7 gamedesc">
              <h5>Zodiac </h5>
              <h5>{{ __('game.GAME_2T') }}</h5>
              <span style="color: grey">{{ __('app.play_now') }}</span>
            </div>
            <div class="col-5">
              <p class="btn btn-img btn-full">
                <img style="width:80px;height:80px;" src="../assets/img/zodiac-5.png"></img>
              </p>
            </div>
          </div>
        </div>
      </div>
    </a>
  </div>

  <div class="col-12 col-xl-6 gamesgroup">
    <a href="{{ route('member.zihua') }}" style="color: white; text-decoration: none;">
      <div class="card border-2 shadow-sm mb-4 text-center">
        <div class="card-body p-0">
          <div class="row gamerow">
            <div class="col-7 gamedesc">
              <h5>Zi Hua</h5>
              <h5>{{ __('game.GAME_3T') }}</h5>
              <span style="color: grey">{{ __('app.play_now') }}</span>
            </div>
            <div class="col-5">
              <p class="btn btn-img btn-full">
                <img style="width:80px;height:80px;" src="../assets/img/zihua-5.png"></img>
              </p>
            </div>
          </div>
        </div>
      </div>
    </a>
  </div>

  <div class="col-12 col-xl-6 gamesgroup">
    <a href="{{ route('member.horoscope') }}" style="color: white; text-decoration: none;">
      <div class="card border-2 shadow-sm mb-4 text-center">
        <div class="card-body p-0">
          <div class="row gamerow">
            <div class="col-7 gamedesc">
              <h5>Horoscope</h5>
              <h5>{{ __('game.GAME_4T') }}</h5>
              <span style="color: grey">{{ __('app.play_now') }}</span>
            </div>
            <div class="col-5">
              <p class="btn btn-img btn-full">
                <img style="width:80px;height:80px;" src="../assets/img/xinzuo-1.png"></img>
              </p>
            </div>
          </div>
        </div>
      </div>
    </a>
  </div>

</div>

<div class="row">
  <div class="col-12 ">
    <div class="card border-1 shadow-sm mb-4">
      <div class="card-header border-0 bg-none">
        <div class="row">
          <div class="col">
            <p class="fs15">{{ __('app.results') }}</p>
          </div>
        </div>
      </div>
      <div class="card-body p-15">
        <table class="table datatable display responsive w-100">
          <thead>
            <tr>
              <th>{{ __('game.GAME_1') }}</th>
              <th>{{ __('game.GAME_2') }}</th>
              <th>{{ __('game.GAME_3') }}</th>
              <th>{{ __('app.hash') }}</th>
              <th>{{ __('app.time') }} </th>
            </tr>
          </thead>
          <tbody>
          @foreach($gameResults as $res)
            <tr>
              <td>{{ $res->qianzi }}</td>
              <td><img src="{{ __('game.IMG_'.$res->zodiac) }}" style="padding-right:14px"/>{{ __('game.ZODIAC_'.$res->zodiac) }}</td>
              <td>{{ str_pad($res->zihua,2,'0',STR_PAD_LEFT) }}</td>
              <td><a href="https://etherscan.io/block/{{ $res->hash }}" target="_blank">{{ $res->hash }}</a></td>
              <td>{{ Carbon\Carbon::parse($res->created_at->setTimezone('Asia/Kuala_Lumpur'))->format('Y-m-d h:i a') }}</td>
            </tr>
          @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

</div>

@endsection
@section('js_after')
<link href="{{ asset('assets/vendor/DataTables-1.10.18/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
<link href="{{ asset('assets/vendor/DataTables-1.10.18/css/responsive.dataTables.min.css') }}" rel="stylesheet">
<script src="{{ asset('assets/vendor/DataTables-1.10.18/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/DataTables-1.10.18/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('assets/vendor/DataTables-1.10.18/js/dataTables.bootstrap4.min.js') }}"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.2/TweenMax.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.2/utils/Draggable.min.js'></script>
<script src="{{ asset('assets/vendor/spin/js/ThrowPropsPlugin.min.js') }}"></script>
<script src="{{ asset('assets/vendor/spin/js/Spin2WinWheel.min.js') }}"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/gsap/latest/plugins/TextPlugin.min.js'></script>
<script type="text/javascript">
  $(document).ready(function () {
    $('.datatable').DataTable({
      "bLengthChange": false,
      'responsive': true,
      'searching': false,
      "bInfo": false,
      "bSort": false
    });
  })
</script>
<script src="{{ asset('assets/js/jquery.countdown.min.js') }}"></script>

<script type="text/javascript">
function setupCountdown(dt) {
  $('#countdown').countdown(dt, function (event) {
    $(this).html(event.strftime('%M:%S'));
  }).on('finish.countdown', function () {
    var nextdate = new Date();
    nextdate.setMilliseconds(0);
    nextdate.setSeconds(0);
    nextdate.setMinutes(Math.ceil((nextdate.getMinutes() + 1) / 10) * 10);
    setupCountdown(nextdate)
  });
}

$(document).ready(function () {
  $('.val,#bet').on('change', function (val) {
    var total = 0;
    var bet = parseFloat($('#bet').val())
    if (bet) {
      var checked = $('.val:checked').length;
      total = checked * bet;
    }
    $('#total').text(total.toFixed(2))
  })
  setupCountdown(nextOpen());
})

function myResult(e) {
  if(e.userData){
    $.post('{{route("member.spinresult")}}', {
      gameId: e.gameId,
      result: e.userData.val,
      "_token": "{{ csrf_token() }}"
    });
  }
}

function myError(e) {

}

function myGameEnd(e) {

}

function initSpin(wheelData) {
  var jsonData = JSON.parse(wheelData);
  var mySpinBtn = document.querySelector('.spinBtn');
    var myWheel = new Spin2WinWheel();
  myWheel.init({data:jsonData, onResult:myResult, onGameEnd:myGameEnd, onError:myError, spinTrigger: mySpinBtn});
}
initSpin('{!! $wheelData !!}');
</script>

<script>
function copyToClipboard(element) {
  var $temp = $("<input>");
  $("body").append($temp);
  $temp.val($(element).text()).select();
  document.execCommand("copy");
  $temp.remove();
  alert("{{__('app.reflink_copied')}}");
}
</script>

<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="{{ asset('js/share.js') }}"></script>
@endsection
