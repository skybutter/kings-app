@extends('layouts.member')


@section('content')
<div class="mb-2">
    <div class="row">
        <div class="col-12 col-md-3">
            <div class="card border-1 shadow mb-4">
                <div class="card-body py-0 text-center">
                    <div class="row">
                        <div class="col py-2">
                            CNY
                            <h4>{{__('wallet.CP')}}</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col py-2">
                            <h4 class="">{{number_format($wallets['CP']['balance'],2)}}</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-3">
            <div class="card border-1 shadow mb-4">
                <div class="card-body py-0 text-center">
                    <div class="row">
                        <div class="col py-2">
                            <i class="material-icons">all_inclusive</i>
                            <h4>{{__('wallet.RP')}}</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col py-2">
                            <h4 class="">{{number_format($wallets['RP']['balance'], 2)}}</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-3">
            <div class="card border-1 shadow mb-4">
                <div class="card-body py-0 text-center">
                    <div class="row">
                        <div class="col py-2">
                            <i class="material-icons">casino</i>
                            <h4>{{__('wallet.BP')}}</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col py-2">
                            <h4 class="">{{number_format($wallets['BP']['balance'],2)}}</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-3">
            <div class="card border-1 shadow mb-4">
                <div class="card-body py-0 text-center">
                    <div class="row">
                        <div class="col py-2">
                            <i class="material-icons">star</i>
                            <h4>{{__('wallet.PT')}}</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col py-2">
                            <h4 class="">{{number_format($wallets['PT']['balance'],2)}}</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
<div class="row">  
    <div class="col-6 col-md-3">
            <div class="card border-1 shadow mb-4">
                <div class="card-body py-0 text-center">
                    <a href="{{ route('wallet.deposit', $users->id) }}">
                    <div class="row">
                        <div class="col py-2">
                            </br>
                            <h4 class="">Deposit</h4>
                            </br>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
    </div>
    
        <div class="col-6 col-md-3">
            <div class="card border-1 shadow mb-4">
                <div class="card-body py-0 text-center">
                    <a href="{{ route('wallet.withdraw', $users->id) }}">
                    <div class="row">
                        <div class="col py-2">
                            </br>
                            <h4 class="">Withdrawal</h4>
                            </br>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
    </div>
    
        <div class="col-6 col-md-3">
            <div class="card border-1 shadow mb-4">
                <div class="card-body py-0 text-center">
                    </br>
                    <form method="POST" action="{{ route('wallet.buy') }}">
                        @csrf
                        <input type="number" name="lcurrency_amount" value="{{ old('lcurrency_amount') }}" id="lcurrency_amount" class="form-control border-input" step="any" required="required" placeholder="Value" oninput="check(this)">
                        Please keyin CNY value to buy
                        <button type="submit" class="btn btn-primary mt-4">
                            Buy
                        </button>
                        
                    </form>
                    </br>
                    
                </div>
            </div>
    </div>
    
        <div class="col-6 col-md-3">
            <div class="card border-1 shadow mb-4">
                <div class="card-body py-0 text-center">
                    <a href="{{ route('wallet.deposit', $users->id) }}">
                    <div class="row">
                        <div class="col py-2">
                            </br>
                            <h4 class="">Sell</h4>
                            </br>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
    </div>

</row>

    
    <!--<div class="row">

        <div class="col-12 col-md-4">
            <div class="card border-1 shadow-sm overflow-hidden mb-4 p-4">
                <div class="row">
                    <div class="col py-2 text-center">
                        <h3><i class="material-icons">grade</i>{{__('app.top_up')}}</h3>
                    </div>
                </div>
                <form method="POST" action="{{route('member.wallet.topup')}}">
                    @csrf
                    <div class="row ">
                        <div class="col-lg-12 col-md-12">
                            <div class="form-group ">
                                <label >{{__('app.amount')}}</label>
                                <div class="input-group">
                                <input type="number" id="input-amount" step="1.00" class="form-control" name="amount" onchange="updateConversion();" />
                                        <div class="input-group-append">
                                            <button type="button" class="btn btn-secondary">RMB</button>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                    <div class="row ">
                        <div class="col-lg-12 col-md-12">
                            <div class="form-group" >
                                <label >{{__('app.payment_method')}}</label>
                                <select class="form-control" id="input-payment_method" name="payment_method"
                                    onchange="updatePaymentDetails();updateConversion();">
                                    @foreach($paymentSettingsJSON as $p)
                                    <option value="{{$p->value}}">{{$p->value}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row ">
                        <div class="col-lg-12 col-md-12">
                            <div class="form-group" >
                                <label >{{__('app.conversion')}}</label>
                                <div class="form-control">
                                    <span></span>&nbsp;&nbsp;<span id="conversion"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row ">
                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label >{{__('app.payment_detail')}}</label>
                                <textarea class="form-control" id="input-payment_detail" readonly="readonly"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row ">
                        <div class="col-lg-12 col-md-12">
                            <div class="form-group ">
                                <label >{{__('app.attachment')}}</label>
                                <input type="hidden" id="input-attachment" name="attachment" />
                                <br>
                                <div class="custom-dropzone text-center align-items-center" id="att-dropzone">
                                    <div class="dz-default dz-message" data-dz-message="">
                                        <h3 class="mt-3"><i class="material-icons">cloud_download</i></h3>
                                        <p>{{__('app.upload_input_notice')}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row ">
                        <div class="col-lg-12 col-md-12">
                            <div class="form-group ">
                                <button type="submit" class="btn btn-primary btn-w100">{{__('app.submit')}}</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>-->
        <!-- Col 1 End -->
        <!-- Col 2 -->
        <!--<div class="col-12 col-md-4">
             Convert Credit 
            <div class="row">
                <div class="col">
                    <div class="card border-1 shadow-sm mb-4 walletop">
                        <div class="card-body">
                            <div class="row">
                                <div class="col text-center">
                                    <h3><i class="material-icons">swap_calls</i>{{__('app.convert_credit')}}</h3>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <form method="POST" action="{{route('member.wallet.convert')}}">
                                        @csrf
                                        <div class="row">
                                            <div class="col-7">
                                                <div class="form-group ">
                                                    <label >{{__('app.wallet_type')}}</label>
                                                    <select class="form-control" id="input-convert_wallet"
                                                        name="wallet">
                                                        <option value="{{$wallets['RP']['id']}}">{{__('wallet.RP')}}
                                                        </option>
                                                        <option value="{{$wallets['BP']['id']}}">{{__('wallet.BP')}}
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-5">
                                                <div class="form-group ">
                                                    <label >{{__('app.amount')}}</label>
                                                    <input type="number" step="1.00" class="form-control"
                                                        name="amount" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row ">
                                            <div class="col">
                                                <div class="form-group ">
                                                    <button type="submit"
                                                        class="btn btn-primary btn-w100">{{__('app.submit')}}</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>-->
            <!--<div class="row">
                <div class="col">
                    <div class="card border-1 shadow-sm mb-4 walletop">
                        <div class="card-body">
                            <div class="row">
                                <div class="col text-center">
                                    <h3><i class="material-icons">swap_calls</i>{{__('app.convert_reward')}}</h3>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <form method="POST" action="{{route('member.wallet.convertBP')}}">
                                        @csrf
                                        <div class="row">
                                            <div class="col-7">
                                                <div class="form-group ">
                                                    <label >{{__('app.wallet_type')}}</label>
                                                    <select class="form-control" id="input-convert_wallet"
                                                        name="wallet">
                                                        <option value="{{$wallets['BP']['id']}}">{{__('wallet.BP')}}
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-5">
                                                <div class="form-group ">
                                                    <label >{{__('app.amount')}}</label>
                                                    <input type="number" step="1.00" class="form-control"
                                                        name="amount" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row ">
                                            <div class="col">
                                                <div class="form-group ">
                                                    <button type="submit"
                                                        class="btn btn-primary btn-w100">{{__('app.submit')}}</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>-->
        </div>
        <!--<div class="col-12 col-md-4">
            <div class="row">
                <div class="col">
                    <div class="card border-1 shadow-sm mb-4 walletop">
                        <div class="card-body">
                            <div class="row">
                                <div class="col text-center">
                                    <h3><i class="material-icons">transfer_within_a_station</i>{{__('app.transfer_credit')}}</h3>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <form method="POST" action="{{route('member.wallet.transfer')}}">
                                        @csrf
                                        <div class="row">
                                            <div class="col-7">
                                                <div class="form-group ">
                                                    <label>{{__('app.username')}}</label>
                                                    <input type="text" class="form-control" name="username" />
                                                </div>
                                            </div>
                                            <div class="col-5">
                                                <div class="form-group ">
                                                    <label>{{__('app.amount')}}</label>
                                                    <input type="number" step="1.00" class="form-control"
                                                        name="amount" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row ">
                                            <div class="col">
                                                <div class="form-group ">
                                                    <button type="submit"
                                                        class="btn btn-primary btn-w100">{{__('app.submit')}}</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>-->
            <!--<div class="col-12 col-md-4">
            <div class="row">
                <div class="col">
                    <div class="card border-1 shadow-sm mb-4 walletop">
                        <div class="card-body">
                            <div class="row">
                                <div class="col text-center">
                                    <h3><i class="material-icons">credit_card</i>{{__('app.withdrawal')}}</h3>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <form method="POST" action="{{route('member.wallet.withdraw')}}">
                                        @csrf
                                        <div class="row">
                                            <div class="col-7">
                                                <div class="form-group ">
                                                    <label>{{__('app.withdrawal_method')}}</label>
                                                    @if(count($withdrawalMethods) > 0)
                                                    <select name="withdrawal_method" class="form-control">
                                                        @foreach($withdrawalMethods as $m)
                                                        <option value="{{$m}}">{{$m}}</option>
                                                        @endforeach
                                                    </select>
                                                    @else
                                                    <input type="text" class="form-control" name="withdrawal_method" />
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-5">
                                                <div class="form-group ">
                                                    <label>{{__('app.amount')}}</label>
                                                    <input type="number" step="1.00" class="form-control"
                                                        name="amount" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row ">
                                            <div class="col">
                                                <div class="form-group ">
                                                    <button type="submit"
                                                        class="btn btn-primary btn-w100">{{__('app.submit')}}</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>-->
            
        <!--<div class="col-12 col-md-4">

            <div class="row">
                <div class="col">
                    <div class="card border-1 shadow-sm mb-4 walletop">
                        <div class="card-body">
                            <div class="row">
                                <div class="col text-center">
                                    <h3><i class="material-icons">assignment</i>{{__('app.transactions')}}</h3>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <table class="table datatable display responsive w-100">
                                        <thead>
                                            <tr>
                                                <th>{{__('app.date')}}</th>
                                                <th>{{__('app.wallet')}}</th>
                                                <th>{{__('app.type')}}</th>
                                                <th>{{__('app.amount')}}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($transactions as $tx)
                                            <tr>
                                                <td>{{Carbon\Carbon::parse($tx->created_at->setTimezone('Asia/Kuala_Lumpur'))->format('m/d')}}
                                                </td>
                                                <td>{{__('wallet.'.$tx->wallet->type)}}</td>
                                                <td>{{strpos($tx->type, 'GAME_') === 0 ? __('game.'.$tx->type) : __('app.tx_'.$tx->type)}}
                                                </td>
                                                <td>{{number_format($tx->amount,2)}}
                                                    @if($tx->withdrawalProof)
                                                    @if(isset($tx->withdrawalProof->PAYMENT_TXID))
                                                    <br>TX: <b>{{$tx->withdrawalProof->PAYMENT_TXID}}</b>
                                                    @endif
                                                    @if(isset($tx->withdrawalProof->PAYMENT_FILE))
                                                    <br>
                                                    <a href="{{ asset('storage/'.$tx->withdrawalProof->PAYMENT_FILE)}}"
                                                        target="_blank"><img
                                                            src="{{ asset('storage/'.$tx->withdrawalProof->PAYMENT_FILE)}}"
                                                            class="attachment-img" /></a>
                                                    @endif
                                                    @endif
                                                </td>

                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>-->

</div>
@endsection

@section('js_after')
<link href="{{asset('assets/vendor/DataTables-1.10.18/css/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
<link href="{{asset('assets/vendor/DataTables-1.10.18/css/responsive.dataTables.min.css')}}" rel="stylesheet">
<script src="{{asset('assets/vendor/DataTables-1.10.18/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/vendor/DataTables-1.10.18/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('assets/vendor/DataTables-1.10.18/js/dataTables.bootstrap4.min.js')}}"></script>

<script src="{{asset('assets/vendor/dropzone-master/dropzone.js')}}"></script>
<script type="text/javascript">
    var paymentSettings = JSON.parse(`{!! $paymentSettings !!}`);
var selectedCurrency = "";
function updatePaymentDetails(){
    var method = $('#input-payment_method').val();
    for(var i = 0; i < paymentSettings.length; i++){
        var info = paymentSettings[i];
        if(info.value === method){
            selectedCurrency = info.currency;
            $('#input-payment_detail').val(info.details);
        }
    }
}

var rates = {};
function updateConversion(){
    var amt = parseFloat($("#input-amount").val())
    var r = 0;
    if(selectedCurrency === "usdt"){
        r = amt / rates.cny;
    }
    else{
        r = amt / rates.cny * rates[selectedCurrency];
    }
    $('#conversion').text(amt + " : " + r.toFixed(2));
}

    $(document).ready(function(){
        $.get('https://api.coingecko.com/api/v3/simple/price?ids=tether&vs_currencies=thb%2Cmyr%2Ccny%2Cvnd%2Cidr%2Cthb', function(data){
            rates = data.tether;
        });
        $('.datatable').DataTable({
            "bLengthChange": false,
            'responsive': true,
                'searching': false,
                "bInfo" : false,
                "order": [[ 0, "desc" ]]
        });
        $("#att-dropzone").dropzone({
        url: "{{route('member.upload')}}",
        addRemoveLinks: false,
        success:function(e,res){
            if(res){
                $('#input-attachment').val(res)
            }
        }
        });
        updatePaymentDetails();
    })
</script>
<script>
 function check(input) {
   if (input.value == 0) {
     input.setCustomValidity('The number must not be zero.');
   } else {
     // input is fine -- reset the error message
     input.setCustomValidity('');
   }
 }
</script>
@endsection