@extends('layouts.member')


@section('content')
<div class="mb-2">
    <div class="row mb-2">
        <div class="col-12">
            <button class="btn btn-info">{{__('app.bet_history')}}</button>
            <a href="{{route('member.statement.commission')}}" class="btn btn-primary">{{__('app.commission')}}</a>
            <a href="{{route('member.wallet')}}" class="btn btn-primary">{{__('app.transactions')}}</a>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card border-0 shadow-sm overflow-hidden mb-4">
                <div class="card-body py-0 text-center p-4">
                    <table class="table datatable display responsive w-100">
                        <thead>
                            <tr>
                                <th>{{__('app.date')}}</th>
                                <th>{{__('app.detail')}}</th>
                                <th>{{__('app.results')}}</th>
                                <th>{{__('app.total_bet')}}</th>
                                <th>{{__('app.profit')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($bets as $row)
                            <tr>
                                <td>{{Carbon\Carbon::parse($row->created_at->setTimezone('Asia/Kuala_Lumpur'))->format('m/d H:i')}}
                                </td>
                                <td>
                                    <div class="d-flex flex-row">
                                        <div class="btn-rounded-circle btn btn-info sgame mr-2">
                                            {{__('game.SGAME_'.$row->game)}}
                                        </div>
                                        <div class="d-flex justify-content-center align-items-center text-left">{!!
                                            $row->details
                                            !!}

                                            <div data-container="body" data-trigger="hover" data-toggle="popover"
                                                class="bet-info" data-placement="top" title="{{__('app.my_bet')}}"
                                                data-html="true" data-content="{{$row->info}}" data-original-title=""
                                                title="">
                                                <i class="material-icons md-18 ml-2">info</i>
                                            </div>
                                        </div>
                                    </div>

                                </td>

                                <td>{{$row->resultStr}}</td>
                                <td>{{number_format($row->amount,2)}}</td>
                                <td>{{number_format($row->winnings,2)}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js_after')
<link href="{{asset('assets/vendor/DataTables-1.10.18/css/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
<link href="{{asset('assets/vendor/DataTables-1.10.18/css/responsive.dataTables.min.css')}}" rel="stylesheet">
<script src="{{asset('assets/vendor/DataTables-1.10.18/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/vendor/DataTables-1.10.18/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('assets/vendor/DataTables-1.10.18/js/dataTables.bootstrap4.min.js')}}"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('.datatable').DataTable({
            "bLengthChange": false,
            'responsive': true,
                'searching': false,
                "bInfo" : false,
                "order": [[ 0, "desc" ]]
        });
      $('[data-toggle="popover"]').popover();
    })
</script>
@endsection