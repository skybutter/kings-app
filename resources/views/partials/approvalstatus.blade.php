@switch($status)
@case(1)
<span class="badge badge-success">{{__('app.approved')}}</span>
@break
@case(2)
<span class="badge badge-secondary">{{__('app.rejected')}}</span>
@break
@default
<span class="badge badge-warning">{{__('app.pending')}}</span>
@endswitch