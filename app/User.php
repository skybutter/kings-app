<?php

namespace App;

use App\GameEntry;
//use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notifiable;
use Liliom\Inbox\Traits\HasInbox;

class User extends Authenticatable
{
    use Notifiable, HasInbox;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $guarded = ['id'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function wallets()
    {
        return $this->hasMany('App\Wallet');
    }

    public function sponsor()
    {
        return $this->belongsTo('App\User', 'sponsor_id', 'id');
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new CustomPasswordNotification($token, $this->name));
    }

    public function scopeWithTotalBet($query)
    {
        $raw = '(select sum(g.amount) from game_entries g where g.user_id=`users`.id) AS total_bet';
        return $query->addSelect(\DB::raw($raw));
    }

    public function getDownlines()
    {
        return User::select('*')->where('parents', 'like', "%," . $this->id . ",%")->withTotalBet()->get();
    }

    public function getLevelFor($sponsorID)
    {
        $sponsorID = strval($sponsorID);
        $parents = explode(',', $this->parents);
        $parents = array_filter($parents);
        $level = 1;
        $sponsorIdx = 0;
        foreach ($parents as $p) {
            if ($p == $sponsorID) {
                break;
            } else {
                $sponsorIdx++;
            }
        }
        $level = count($parents) - $sponsorIdx;
        return $level;
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($user) {
            $refkey = strtoupper(str_random(6));

            while (User::where('referralkey', '$refkey')->count() > 0) {
                $refkey = strtoupper(str_random(6));
            }

            if (!$user->referralkey) {
                $user->referralkey = $refkey;
            }

        });
    }
}

class CustomPasswordNotification extends ResetPassword
{
    public $name;

    public function __construct($token, $name)
    {
        parent::__construct($token);
        $this->name = $name;
    }

    public function toMail($notifiable)
    {
        if (static::$toMailCallback) {
            return call_user_func(static::$toMailCallback, $notifiable, $this->token);
        }

        $resetLink = url(config('app.url') . route('password.reset', ['token' => $this->token, 'email' => $notifiable->getEmailForPasswordReset()], false));

        return (new MailMessage)
            ->view('emails.resetpassword', ['name' => $this->name, 'link' => $resetLink])
            ->subject('Reset Password Notification');
    }
}