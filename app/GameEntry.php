<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GameEntry extends Model
{
    protected $casts = [
        'extra' => 'array',
        'winres' => 'array',
    ];

    public function result()
    {
        return $this->belongsTo('App\GameResult', 'winresult_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

}
