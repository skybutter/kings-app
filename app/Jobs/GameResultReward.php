<?php

namespace App\Jobs;

use App\GameEntry;
use App\GameResult;
use App\Setting;
use App\Transaction;
use App\Wallet;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class GameResultReward implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public static $DECIMALS = 2;

    protected $hash = null;
    protected $ts = null;
    protected $qianziOdds = [];
    protected $zodiacOdds = 0;
    protected $zihuaOdds = 0;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($hash = null, $ts = null)
    {
        $this->hash = $hash;
        $this->ts = $ts;
        $this->readQianziOdds();
        $this->readZihuaOdds();
        $this->readZodiacOdds();

    }

    protected function readQianziOdds()
    {
        $res = Setting::where('option', 'qianzi_odds')->first();
        $this->qianziOdds = json_decode($res->val);
    }

    protected function readZihuaOdds()
    {
        $res = Setting::where('option', 'zihua_odds')->first();
        $this->zihuaOdds = json_decode($res->val);
    }

    protected function readZodiacOdds()
    {
        $res = Setting::where('option', 'zodiac_odds')->first();
        $this->zodiacOdds = doubleval($res->val);
    }

    protected function getResForGame($val, $mod)
    {
        $res = $val % $mod;
        $res = $res == 0 ? $mod : $res;
        return $res;
    }

    public function processHash()
    {
        $numbers = filter_var($this->hash, FILTER_SANITIZE_NUMBER_INT);

        $qianzi = substr($numbers, -3);
        $numVal = (double) ($qianzi);

        $zodiac = $this->getResForGame($numVal, 12);
        $zihua = $this->getResForGame($numVal, 36);

        $gameResult = new GameResult();
        $gameResult->hash = $this->hash;
        $gameResult->ts = \Carbon\Carbon::createFromTimestamp($this->ts);
        $gameResult->qianzi = $qianzi;
        $gameResult->zodiac = $zodiac;
        $gameResult->zihua = $zihua;
        $gameResult->save();

        Log::info("Game result #" . $gameResult->id . " Qianzi: " . $qianzi . " Zodiac: " . $zodiac . " Zihua: " . $zihua);

        $this->processEntries($gameResult);
    }

    protected function processZodiac($hash, $entry, $gameResultId)
    {
        $entry->status = 1;
        $winnings = 0;
        $winres = [];
        if (count($entry->extra) > 0) {
            foreach ($entry->extra as $bet) {
                $num = $bet[0];
                $betAmt = $bet[1];
                $win = 0;
                if ($hash == $num) {
                    $win = doubleval(round($betAmt * $this->zodiacOdds, self::$DECIMALS));
                    $winres[] = [$num, $win];
                }
                $winnings += $win;
            }
        }

        $entry->winresult_id = $gameResultId;

        if ($winnings > 0) {
            $entry->status = 2;

            $wallet = Wallet::where('user_id', $entry->user_id)->where('type', 'RP')->first();
            if ($wallet) {
                $tx = new Transaction();
                $tx->type = "GAME_2";
                $tx->wallet_id = $wallet->id;
                $tx->amount = $winnings;
                $tx->ref_type = 'App\GameEntry';
                $tx->ref_id = $entry->id;
                $tx->save();
            }
        }
        $entry->winres = $winres;
        $entry->winnings = $winnings;
        $entry->save();
    }

    protected function processZihua($hash, $entry, $gameResultId)
    {
        $entry->status = 1;
        $winnings = 0;
        $winres = [];
        $entry->winresult_id = $gameResultId;

        if (count($entry->extra) > 0) {
            foreach ($entry->extra as $bet) {
                $num = $bet[0];
                $betAmt = $bet[1];
                $win = 0;
                if ($hash == $num) {
                    $win = doubleval(round($betAmt * $this->zihuaOdds, self::$DECIMALS));
                    $winres[] = [$num, $win];
                }
                $winnings += $win;
            }
        }
        if ($winnings > 0) {
            $entry->status = 2;
            $entry->winresult_id = $gameResultId;

            $wallet = Wallet::where('user_id', $entry->user_id)->where('type', 'RP')->first();
            if ($wallet) {
                $tx = new Transaction();
                $tx->type = "GAME_3";
                $tx->wallet_id = $wallet->id;
                $tx->amount = $winnings;
                $tx->ref_type = 'App\GameEntry';
                $tx->ref_id = $entry->id;
                $tx->save();
            }

        }
        $entry->winres = $winres;
        $entry->winnings = $winnings;
        $entry->save();
    }

    protected function processQianzi($d3, $entry, $gameResultId)
    {
        $d1 = substr($d3, -1);
        $d2 = substr($d3, -2);
        $results = [$d1, $d2, $d3];
        $entry->status = 1;
        $winnings = 0;
        $winres = [];
        $entry->winresult_id = $gameResultId;

        if (count($entry->extra) > 1) {
            $betNum = $entry->extra[0];
            $count = count($entry->extra);

            for ($i = 1; $i < $count; $i++) {
                $bet = $entry->extra[$i];
                $betType = $bet[0];
                $typeNum = substr($betNum, $betType * -1);
                $betAmt = $bet[1];
                $win = 0;
                if ($typeNum == $results[$betType - 1]) {
                    $win = doubleval(number_format($betAmt * $this->qianziOdds[$betType - 1], self::$DECIMALS));
                    $winres[] = [$betType . 'D', $win];
                }
                $winnings += $win;
            }
        }
        if ($winnings > 0) {
            $entry->status = 2;
            $entry->winresult_id = $gameResultId;
            $wallet = Wallet::where('user_id', $entry->user_id)->where('type', 'RP')->first();
            if ($wallet) {
                $tx = new Transaction();
                $tx->type = "GAME_1";
                $tx->wallet_id = $wallet->id;
                $tx->amount = $winnings;
                $tx->ref_type = 'App\GameEntry';
                $tx->ref_id = $entry->id;
                $tx->save();
            }
        }
        $entry->winres = $winres;
        $entry->winnings = $winnings;
        $entry->save();
    }

    protected function processEntries($gameResult)
    {
        $entries = GameEntry::where('status', 0)->where('created_at', '<', $gameResult->ts)->get();
        foreach ($entries as $entry) {
            // process qianzi
            switch ($entry->game) {
                case 1:
                    $this->processQianzi($gameResult->qianzi, $entry, $gameResult->id);
                    break;
                case 2:
                    $this->processZodiac($gameResult->zodiac, $entry, $gameResult->id);
                    break;
                case 3:
                    $this->processZihua($gameResult->zihua, $entry, $gameResult->id);
                    break;
            }
        }
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (!$this->hash || !$this->ts) {
            return;
        }
        $this->processHash();
    }
}
