<?php

namespace App\Jobs;

use App\Setting;
use App\Transaction;
use App\User;
use App\Wallet;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class Rebate implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public static $DECIMALS = 2;

    public $player = null;
    public $entry = null;
    public $total = null;
    public $commissions = null;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user, $entry, $total)
    {
        $this->player = $user;
        $this->entry = $entry;
        $this->total = $total;

        $this->commissions = Setting::where('option', 'like', 'comm_%')->get()->toArray();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $maxComm = 0;
        $extraComm = 0;
        $userComm = 0;
        $commissions = [];
        foreach ($this->commissions as $comm) {
            switch ($comm['option']) {
                case 'comm_max':
                    $maxComm = $comm['val'];
                    break;
                case 'comm_extra':
                    $extraComm = $comm['val'];
                    break;
                default:
                    // if ($comm['option'] == 'comm_' . $this->player->level) {
                    //     $userComm = $comm['val'];
                    // }
                    $commissions[$comm['option']] = $comm['val'];
                    break;
            }
        }
        $userComm = $this->player->commission;
        // Player rebate
        $wallet = Wallet::where('user_id', $this->player->id)->where('type', 'BP')->first();

        $tx = new Transaction();
        $tx->type = "COMMISSION";
        $tx->wallet_id = $wallet->id;
        //$tx->amount = round($this->total * $userComm / 100, self::$DECIMALS);
        $tx->amount = 0;
        $tx->ref_type = 'App\GameEntry';
        $tx->ref_id = $this->entry->id;
        $tx->extra = json_encode(['total' => $this->total]);
        $tx->save();

        $balanceComm = $maxComm - $userComm;
        $prevComm = $userComm;
        \Log::info("Pay commission for bet from " . $this->player->id . ", amount: ".$this->total.", player comm: " . $userComm . "%");
        // Get uplines
        $parentsIds = array_filter(explode(',', $this->player->parents), function ($item) {
            return $item != null;
        });
        // Get upline details
        $parents = User::whereIn('id', $parentsIds)->get()->toArray();
        $parentsIds = array_reverse($parentsIds); // pay from bottom up
        $payableParents = [];
        for ($i = 0; $i < count($parentsIds); $i++) {
            $pid = $parentsIds[$i];
            $parent = null;
            foreach ($parents as $pr) {
                if ($pr['id'] == $pid) {
                    $parent = $pr;
                    break;
                }
            }
            
            if ($balanceComm > 0) {
                $comm = min($balanceComm, $parent['commission'] - $prevComm);
                if($comm > 0){
                    $payableParents[] = [$parent['id'], $comm];
                    $balanceComm = max($balanceComm - $comm, 0);
                    \Log::info("Parent: " . $parent['id'] . ", comm: ".$comm."% (" . $parent['commission'] . "%), balance: ".$balanceComm."%");
                    $prevComm = $parent['commission'];
                }
            }
        }
        foreach ($payableParents as $p) {
            $wallet = Wallet::where('user_id', $p[0])->where('type', 'BP')->first();
            $tx = new Transaction();
            $tx->type = "COMMISSION";
            $tx->wallet_id = $wallet->id;
            $tx->amount = round($p[1] / 100 * $this->total, self::$DECIMALS);
            $tx->ref_type = 'App\GameEntry';
            $tx->ref_id = $this->entry->id;
            $tx->extra = json_encode(['total' => $this->total, 'player' => $this->player->username]);
            $tx->save();
        }
    }
}
