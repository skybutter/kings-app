<?php

namespace App\Jobs;

use App\Jobs\GameResultReward;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class ScanETH implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $apiKey = '';
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
        $this->apiKey = config('app.scanToken');
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $client = new \GuzzleHttp\Client();
        //Get block
        $ts = \Carbon\Carbon::now('UTC')->timestamp;
        Log::info('Scanning ETH with ts ' . $ts);
        $response = $client->get('https://api.etherscan.io/api?module=block&action=getblocknobytime&timestamp=' . $ts . '&closest=before&apikey=' . $this->apiKey);
        if ($response->getStatusCode() == 200) {
            $body = $response->getBody();
            $bodyJSON = json_decode($body);
            $blockNumber = $bodyJSON->result;
            Log::info('Block: ' . $blockNumber);

            $blockHex = dechex($blockNumber);
            $hashRes = $client->get('https://api.etherscan.io/api?module=proxy&action=eth_getBlockByNumber&tag=' . $blockHex . '&boolean=true&apikey=' . $this->apiKey);
            if ($hashRes->getStatusCode() == 200) {
                $hashJSON = json_decode($hashRes->getBody());
                $ts = hexdec($hashJSON->result->timestamp);
                Log::info("Hash: " . $hashJSON->result->hash . " TS: " . $ts);
                GameResultReward::dispatch($hashJSON->result->hash, $ts);
            } else {
                Log::error("Scan hash failed with status " . $hashRes->getStatusCode());
            }
        } else {
            Log::error("Scan failed with status " . $response->getStatusCode());
        }
    }
}
