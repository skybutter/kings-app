<?php

namespace App\Jobs;

use App\GameEntry;
use App\Setting;
use App\Transaction;
use App\Wallet;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class Pool implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public static $DECIMALS = 2;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $poolComm = Setting::where('option', 'pool')->first();
        $now = Carbon::now('Asia/Kuala_Lumpur');
        $day = $now->format('N');
        $start = $now;
        if ($day < 7) {
            $start = Carbon::now()->subtract($day, 'days');
        }

        $startStr = $start->toDateString();
        $startDt = new Carbon($startStr, 'Asia/Kuala_Lumpur');
        $startDt->timestamp . PHP_EOL;

        $totalPool = 0;
        $entries = GameEntry::where('created_at', '>=', $startDt)
            ->where('created_at', '<=', $now)->get();

        $users = [];
        foreach ($entries as $ge) {
            $totalPool += $ge->amount;
            if (!isset($users[$ge->user_id])) {
                $users[$ge->user_id] = 0;
            }
            $users[$ge->user_id] += $ge->amount;
        }

        $totalPoolComm = $totalPool * $poolComm['val'] / 100;
        $totalPoints = floor($totalPool / 100);
        foreach ($users as $k => $v) {
            $amount = $totalPoolComm / $totalPoints * floor($v / 100);
            if ($amount > 0) {
                $wallet = Wallet::where('user_id', $k)->where('type', 'BP')->first();

                $tx = new Transaction();
                $tx->type = "POOL";
                $tx->wallet_id = $wallet->id;
                $tx->amount = $amount;
                $tx->ref_type = 'App\User';
                $tx->ref_id = $k;
                $tx->save();
            }
        }
    }
}
