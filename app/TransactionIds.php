<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TransactionIds extends Model
{
    protected $table = "validate_transaction_ids";

    protected $fillable = [
        'user_id', 'transaction_id', 'status',
    ];

    public static function checkTransactionId($user_id,$transaction_id)
    {
    	$val_trans = DB::table('validate_transaction_ids')->orderBy('id','desc')->where(array('user_id'=>$user_id,'transaction_id'=>$transaction_id,'status'=>0))->first();

    	return $val_trans;
    }
}
