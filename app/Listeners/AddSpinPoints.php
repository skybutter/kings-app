<?php

namespace App\Listeners;

use App\Events\UserCreated;
use App\Wallet;
use App\Transaction;

class AddSpinPoints
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserCreated  $event
     * @return void
     */
    public function handle(UserCreated $event)
    {
        $spinWallet = Wallet::where('user_id', $event->user->sponsor_id)->where('type', 'SPIN')->first();
        if ($spinWallet) {
            $tx = new Transaction();
            $tx->type = "REFERRAL";
            $tx->wallet_id = $spinWallet->id;
            $tx->amount = 3;
            $tx->extra = '';
            $tx->ref()->associate($event->user);
            $tx->save();
        }
    }
}
