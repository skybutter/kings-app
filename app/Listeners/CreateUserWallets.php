<?php

namespace App\Listeners;

use App\Events\UserCreated;
use App\Wallet;

class CreateUserWallets
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserCreated  $event
     * @return void
     */
    public function handle(UserCreated $event)
    {
        Wallet::Create([
            'user_id' => $event->user->id,
            'type' => 'CP',
        ]);
        Wallet::Create([
            'user_id' => $event->user->id,
            'type' => 'RP',
        ]);
        Wallet::Create([
            'user_id' => $event->user->id,
            'type' => 'BP',
        ]);
        Wallet::Create([
            'user_id' => $event->user->id,
            'type' => 'PT',
        ]);
        Wallet::Create([
            'user_id' => $event->user->id,
            'type' => 'SPIN',
            'balance' => 7
        ]);
        Wallet::Create([
            'user_id' => $event->user->id,
            'type' => 'SP',
        ]);
    }
}
