<?php

namespace App\Listeners;

use App\Events\UserCreated;

class UserActivations
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserCreated  $event
     * @return void
     */
    public function handle(UserCreated $event)
    {
        //Mail::to($event->user->email)->send((new WelcomeMail($event->user, $event->rawpassword))->subject('Welcome to STC'));
    }
}
