<?php

namespace App\Http\Controllers\Auth;

use App\Events\UserCreated;
use App\Http\Controllers\Controller;
use App\Rules\UserEmailCount;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
     */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'username' => 'required|unique:users,username|alpha_num',
            'email' => ['required',
                'email',
                'regex:/(.*)@(.*)\.(.*)/i',
                new UserEmailCount],
            'name' => 'required',
        ], [
            'username.required' => __('validation.required', ['attribute' => __('app.username')]),
            'name.required' => __('validation.required', ['attribute' => __('app.fullname')]),
        ]);

    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $sponsorname = $data['refcode'];
        $sponsornames = ['admin'];
        if ($sponsorname) {
            $sponsornames[] = $sponsorname;
        }
        $sponsor = User::whereIn('referralkey', $sponsornames)->orderBy('id', 'desc')->first();
        $parents = rtrim($sponsor->parents, ',') . ',' . $sponsor->id . ',';

        $member = User::create([
            'username' => $data['username'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'name' => $data['name'],
            'role' => 'member',
            'parents' => $parents,
            'sponsor_id' => $sponsor->id,
            'commission' => 0
        ]);
        event(new UserCreated($member, $data['password']));

        return $member;

    }

    public function showRegistrationForm(Request $request)
    {

        $ref = $request->get('refcode', '');
        return view('auth.register', ['refcode' => $request->get('refcode', '')]);
    }
}
