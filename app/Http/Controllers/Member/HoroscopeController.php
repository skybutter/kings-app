<?php

namespace App\Http\Controllers\Member;

use App\GameEntry;
use App\GameResult;
use App\Http\Controllers\Controller;
use App\Jobs\Rebate;
use App\Setting;use App\Transaction;
use App\Wallet;
use \Illuminate\Http\Request;

class HoroscopeController extends Controller
{
    public static $DECIMALS = 2;

    public function index()
    {
        $maxbet = Setting::where('option', 'maxbet_4')->first()->val;
        return view('member.horoscope', compact('maxbet'));
    }

    public function submit(Request $request)
    {
        if (!$request->input('valid')) {
            return back()->withError(trans('error.invalid_bet'));
        }

        $user = \Auth::user();
        $userID = $user->id;
        $gameResult = ['winres' => false, 'winnum' => ''];

        $extra = [];
        $total = 0;
        $message = null;
        foreach ($request->get('val') as $k => $val) {
            $amt = doubleval($val);
            if ($amt > 0) {
                $extra[] = [($k + 1) . '', $amt];
                $total += $amt;
            }
        }

        $wallet = Wallet::where('user_id', $userID)->where('type', 'CP')->first();

        if (!$wallet || $wallet->balance < $total) {
            throw \Illuminate\Validation\ValidationException::withMessages([
                'amount' => [__('validation.insufficient_funds')],
            ]);
        }

        if (count($extra) > 0) {
            $entry = new GameEntry;
            $entry->user_id = $userID;
            $entry->extra = $extra;
            $entry->status = 1;
            $entry->game = 4;
            $entry->amount = $total;
            $entry->winres = '';
            $entry->save();

            $tx = new Transaction();
            $tx->type = "GAME_4";
            $tx->wallet_id = $wallet->id;
            $tx->amount = -$total;
            $tx->ref_type = 'App\GameEntry';
            $tx->ref_id = $entry->id;
            $tx->save();

            Rebate::dispatch($user, $entry, $total);

            $message = trans('app.entry_submitted');
            $gameResult = $this->processGameEntry($entry);

        } else {
            return back()->withError(trans('error.empty_bet'));
        }
        return redirect(route('member.horoscope'))->with('success', $message)->with('winres', $gameResult['winres'])->with('winnum', $gameResult['winnum']);
    }

    protected function processGameEntry($entry){
        $gameResult = ['winres' => false, 'winnum' => ''];
        $winrate = intval(Setting::where('option', 'horoscope_winrate')->first()->val);
        $rand = mt_rand(0, 99);
        $gameResult['winres'] = $rand <= $winrate;
        $betNums = [];

        $odds = Setting::where('option', 'horoscope_odds')->first()->val;
        
        foreach($entry->extra as $bet){
            $betNums[] = $bet[0];
        }
        $loseNums = array_diff(array_map('strval', range(1,12)), $betNums);
        
        $randArray = $gameResult['winres'] ? $betNums : $loseNums;
        $gameResult['winnum'] = $randArray[array_rand($randArray, 1)];

        $winres = [];
        $winnings = 0;
        if($gameResult['winres']){
            // Won, pay reward
            foreach ($entry->extra as $bet) {
                $num = $bet[0];
                $betAmt = $bet[1];
                if ($gameResult['winnum'] == $num) {
                    $win = doubleval(round($betAmt * $odds, self::$DECIMALS));
                    $winres[] = [$num, $win];
                    $winnings += $win;
                    break;
                }
            }
            $entry->status = 2;
            $wallet = Wallet::where('user_id', $entry->user_id)->where('type', 'RP')->first();
            if ($wallet) {
                $tx = new Transaction();
                $tx->type = "GAME_4";
                $tx->wallet_id = $wallet->id;
                $tx->amount = $winnings;
                $tx->ref_type = 'App\GameEntry';
                $tx->ref_id = $entry->id;
                $tx->save();
            }
        }
        else{
            $winres[] = [$gameResult['winnum'], 0];
        }
        $entry->winres = $winres;
        $entry->winnings = $winnings;
        $entry->save();
        return $gameResult;
    }
}
