<?php

namespace App\Http\Controllers\Member;

use App\GameEntry;
use App\Http\Controllers\Controller;
use App\Setting;
use Carbon\Carbon;

class PoolController extends Controller
{
    public function index()
    {
        $now = Carbon::now('Asia/Kuala_Lumpur');
        $day = $now->format('N');
        $start = $now;
        if ($day < 7) {
            $start = Carbon::now('Asia/Kuala_Lumpur')->subDays($day, 'days');
        }

        $startStr = $start->toDateString();
        $startDt = new Carbon($startStr, 'Asia/Kuala_Lumpur');

        $total = 0;
        $entries = GameEntry::where('created_at', '>=', $startDt)
            ->where('created_at', '<=', $now)->get();

        $poolRate = floatval(Setting::where('option', 'pool')->first()->val);

        $buckets = [];
        for ($i = 1; $i <= 7; $i++) {
            $buckets[$i . ''] = 0;
        }
        foreach ($entries as $ge) {
            $d = Carbon::parse($ge->created_at->setTimezone('Asia/Kuala_Lumpur'))->format('N');
            $buckets[$d] = $buckets[$d] + $ge->amount;
            $total += $ge->amount;
        }

        $total = $total * $poolRate / 100;

        $poolTotal = Setting::where('option', 'poolTotal')->first()->val;

        $total = strlen($poolTotal) > 0 ? $poolTotal : $total;
        $poolVals = json_decode(Setting::where('option', 'poolVals')->first()->val);

        for ($i = 1; $i <= 7; $i++) {
            $buckets[$i . ''] = strlen($poolVals[$i - 1]) > 0 ? floatval($poolVals[$i - 1]) : $buckets[$i . ''] * $poolRate / 100;
        }

        return view('member.pool', compact('total', 'buckets'));
        return view('admin.settings_pool', compact('total', 'buckets'));
    }
}
