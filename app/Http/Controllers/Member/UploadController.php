<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UploadController extends Controller
{
    public function index(Request $request)
    {
        $path = str_replace('public/', '', $request->file('file')->store('public/attachments'));

        return $path;
    }
}
