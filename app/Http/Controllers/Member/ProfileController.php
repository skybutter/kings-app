<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    public function index()
    {
        $user = \Auth::user();
        $reflink = route('register', ['refcode' => $user->referralkey]);

        return view('member.profile', compact('user', 'reflink'));
    }

    public function update(Request $request)
    {
        $request->validate(['email' => 'required|email']);

        $user = \Auth::user();
        if ($request->get('new_password')) {
            $request->validate(['new_password' => 'confirmed']);

            $user->password = Hash::make($request->get('new_password'));
        }
        $user->email = $request->get('email');
        $user->name = $request->get('name');
        $user->bank_acc = $request->get('bank_acc');
        $user->usdt_acc = $request->get('usdt_acc');
        $user->save();

        return redirect(route('member.profile'))->with('success', trans('app.request_successful'));

    }
}
