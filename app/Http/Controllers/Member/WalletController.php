<?php

namespace App\Http\Controllers\Member;

use App\Approval;
use App\Http\Controllers\Controller;
use App\Setting;
use App\Transaction;
use App\User;
use App\Wallet;
use Illuminate\Http\Request;
use Mail;
use DB;
use App\TransactionIds;

class WalletController extends Controller
{
    public function index()
    {
        $users = \Auth::user();
        $userID = \Auth::id();
        $user = User::select('*')->where('id', $userID)->with('wallets')->first();

        $wallets = [];
        $walletIDs = [];
        foreach ($user->wallets as $w) {
            if($w->type == 'SPIN'){
                continue;
            }
            $walletIDs[] = $w->id;
            $wallets[$w->type] = ['id' => $w->id, 'balance' => $w->balance];
        }

        $paymentSettings = Setting::where('option', 'payment_methods')->first()->val;
        $paymentSettingsJSON = json_decode($paymentSettings);

        $transactions = Transaction::with('wallet')->with('ref')->whereIn('wallet_id', $walletIDs)->latest()->get();

        $withdrawalMethods = [];
        if (strlen($user->bank_acc) > 0) {
            $withdrawalMethods[] = __('app.bank') . ' ' . $user->bank_acc;
        }
        if (strlen($user->usdt_acc) > 0) {
            $withdrawalMethods[] = __('app.usdt') . ' ' . $user->usdt_acc;
        }

        foreach ($transactions as $tx) {
            if ($tx->type == 'WITHDRAWAL' && $tx->ref_type == 'App\Approval' && $tx->ref->status == 1) {
                $content = json_decode($tx->ref->content);
                $tx->withdrawalProof = $content;
            }
        }

        return view('member.wallet', compact('wallets', 'paymentSettings', 'paymentSettingsJSON', 'transactions', 'withdrawalMethods', 'users'));
    }
    

    public function topup(Request $request)
    {
        $request->validate(['amount' => 'numeric|min:20']);

        $amount = floatval($request->get('amount', 0));

        $user = \Auth::user();
        $wallet = Wallet::where('user_id', $user->id)->where('type', 'CP')->first();

        $approval = new Approval();
        $approval->type = 'TOPUP';
        $approval->user()->associate($user);
        $approval->ref()->associate($wallet);

        $approval->content = json_encode(['amount' => $amount,
            'payment_method' => $request->get('payment_method'),
            'attachment' => $request->get('attachment')]);

        $approval->save();

        Mail::raw('User: '.$user->username."\nAmount: ".$amount."\nPayment method: ".$request->get('payment_method'), function ($message) use($user) {
            $message->to(config('app.support'))
              ->subject('New Top up from '.$user->username);
          });

        
        return redirect(route('member.wallet'))->with('success', trans('app.request_submitted_approval'));
    }

    public function convert(Request $request)
    {
        $user = \Auth::user();

        $request->validate([
            'amount' => 'numeric|min:1',
            'wallet' => 'required']);

        $amount = floatval($request->get('amount', 0));
        $walletFrom = Wallet::where('user_id', $user->id)->where('id', $request->get('wallet'))->first();
        $walletTo = Wallet::where('user_id', $user->id)->where('type', 'CP')->first();

        if (!$walletFrom || !$walletTo) {
            throw \Illuminate\Validation\ValidationException::withMessages([
                'wallet' => [__('validation.invalid')],
            ]);
        }

        if ($amount > $walletFrom->balance) {
            throw \Illuminate\Validation\ValidationException::withMessages([
                'amount' => [__('validation.insufficient_funds')],
            ]);

        }

        $user = \Auth::user();
        $approval = new Approval();
        $approval->type = 'CONVERT';
        $approval->user()->associate($user);
        $approval->ref()->associate($walletFrom);
        $approval->content = json_encode(['amount' => $amount, 'to' => $walletTo->id]);
        $approval->status = 1;
        $approval->save();

        // Do transaction
        $tx = new Transaction();
        $tx->type = 'CONVERT_OUT';
        $tx->wallet()->associate($walletFrom);
        $tx->ref()->associate($walletTo);
        $tx->amount = -$amount;
        $tx->save();

        $tx = new Transaction();
        $tx->type = 'CONVERT_IN';
        $tx->wallet()->associate($walletTo);
        $tx->ref()->associate($walletFrom);
        $tx->amount = $amount;
        $tx->save();

        return redirect(route('member.wallet'))->with('success', trans('app.request_successful'));
    }
    
    public function convertBP(Request $request)
    {
        $user = \Auth::user();

        $request->validate([
            'amount' => 'numeric|min:1',
            'wallet' => 'required']);

        $amount = floatval($request->get('amount', 0));
        $walletFrom = Wallet::where('user_id', $user->id)->where('id', $request->get('wallet'))->first();
        $walletTo = Wallet::where('user_id', $user->id)->where('type', 'RP')->first();

        if (!$walletFrom || !$walletTo) {
            throw \Illuminate\Validation\ValidationException::withMessages([
                'wallet' => [__('validation.invalid')],
            ]);
        }

        if ($amount > $walletFrom->balance) {
            throw \Illuminate\Validation\ValidationException::withMessages([
                'amount' => [__('validation.insufficient_funds')],
            ]);

        }

        $user = \Auth::user();
        $approval = new Approval();
        $approval->type = 'CONVERT';
        $approval->user()->associate($user);
        $approval->ref()->associate($walletFrom);
        $approval->content = json_encode(['amount' => $amount, 'to' => $walletTo->id]);
        $approval->status = 1;
        $approval->save();

        // Do transaction
        $tx = new Transaction();
        $tx->type = 'CONVERT_OUT';
        $tx->wallet()->associate($walletFrom);
        $tx->ref()->associate($walletTo);
        $tx->amount = -$amount;
        $tx->save();

        $tx = new Transaction();
        $tx->type = 'CONVERT_IN';
        $tx->wallet()->associate($walletTo);
        $tx->ref()->associate($walletFrom);
        $tx->amount = $amount;
        $tx->save();

        return redirect(route('member.wallet'))->with('success', trans('app.request_successful'));
    }
    
    
    
    

    public function transfer(Request $request)
    {
        $user = \Auth::user();

        $request->validate([
            'username' => 'required',
            'amount' => 'numeric|min:1']);

        $amount = floatval($request->get('amount', 0));
        $walletFrom = Wallet::where('user_id', $user->id)->where('type', 'CP')->first();
        $recipient = User::where('username', $request->get('username'))->first();
        if (!$recipient) {
            throw \Illuminate\Validation\ValidationException::withMessages([
                'username' => [__('validation.invalid')],
            ]);
        }

        // Check up/downline
        $isDirectionOK = false;
        if (strlen($user->parents) > strlen($recipient->parents)) {
            // Upline
            if (strpos($user->parents, ',' . $recipient->id . ',') > -1) {
                $isDirectionOK = true;
            }
        } else {
            if (strpos($recipient->parents, ',' . $user->id . ',') > -1) {
                $isDirectionOK = true;
            }
        }

        $walletTo = Wallet::where('user_id', $recipient->id)->where('type', 'CP')->first();
        if (!$walletTo) {
            throw \Illuminate\Validation\ValidationException::withMessages([
                'email' => [__('validation.invalid')],
            ]);
        }

        if ($amount > $walletFrom->balance) {
            throw \Illuminate\Validation\ValidationException::withMessages([
                'amount' => [__('validation.insufficient_funds')],
            ]);
        }
        if (!$isDirectionOK) {
            $error = \Illuminate\Validation\ValidationException::withMessages([
                'recipient' => [__('validation.invalid', ['attribute' => __('app.username')])],
            ]);
            throw $error;
        }
        $user = \Auth::user();
        $approval = new Approval();
        $approval->type = 'TRANSFER';
        $approval->user()->associate($user);
        $approval->ref()->associate($walletFrom);
        $approval->content = json_encode(['amount' => $amount, 'to' => $walletTo->id]);
        $approval->status = 1;
        $approval->save();

        // Do transaction
        $tx = new Transaction();
        $tx->type = 'TX_OUT';
        $tx->wallet()->associate($walletFrom);
        $tx->ref()->associate($walletTo);
        $tx->amount = -$amount;
        $tx->save();

        $tx = new Transaction();
        $tx->type = 'TX_IN';
        $tx->wallet()->associate($walletTo);
        $tx->ref()->associate($walletFrom);
        $tx->amount = $amount;
        $tx->save();

        return redirect(route('member.wallet'))->with('success', trans('app.request_successful'));
    }

    public function withdraw(Request $request)
    {
        $request->validate([
            'amount' => 'numeric|min:1',
            'withdrawal_method' => 'required']);

        $amount = floatval($request->get('amount', 0));

        $user = \Auth::user();

        $walletFrom = Wallet::where('user_id', $user->id)->where('type', 'RP')->first();
        if ($amount > $walletFrom->balance) {
            throw \Illuminate\Validation\ValidationException::withMessages([
                'amount' => [__('validation.insufficient_funds')],
            ]);
        }

        $approval = new Approval();
        $approval->type = 'WITHDRAWAL';
        $approval->user()->associate($user);
        $approval->ref()->associate($walletFrom);
        $approval->content = json_encode(['amount' => $amount, 'details' => $request->get('withdrawal_method')]);

        $approval->save();

        $tx = new Transaction();
        $tx->type = 'WITHDRAWAL';
        $tx->wallet()->associate($walletFrom);
        $tx->ref()->associate($approval);
        $tx->amount = -$amount;
        $tx->save();

        Mail::raw('User: '.$user->username."\nAmount: ".$amount."\nPayment method: ".$request->get('withdrawal_method'), function ($message) use($user) {
            $message->to(config('app.support'))
              ->subject('New Withdrawal from '.$user->username);
          });

        return redirect(route('member.wallet'))->with('success', trans('app.request_submitted_approval'));

    }
    
    public function wallet_deposit($id)
    {
        $Users = DB::table('users')->where(array('id'=>$id))->first();
        
        $data = array(
            'username'=>$Users->username,
            'email'=>$Users->email,
            'name'=>$Users->name,
            'merchants_user_id'=>$id,
            'whitelisted_wallet_addresses'=>$Users->usdt_acc,
            'req' => array('action'=>'deposit'),
        );

        $url = config('app.stoneUrl') .'/api/user';
        $data_string = json_encode($data);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('apikey:'.config('app.stoneAPI'),'Content-Type:application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $result = json_decode($result);
        // print_r($result);
        curl_close($ch);

        $access_token = $result->access_token;

        return redirect()->away(config('app.stoneUrl').'/gateway/'.$access_token); 
        
    }
    
    public function wallet_withdraw($id)
    {
        $Users = DB::table('users')->where(array('id'=>$id))->first();
        
        $transaction_id = rand(10,1000000000);
        $arr = array(
            'user_id'=>$Users->id,
            'transaction_id'=>$transaction_id,
        );

        TransactionIds::create($arr);
        
        $data = array(
            'username'=>$Users->username,
            'email'=>$Users->email,
            'name'=>$Users->name,
            'merchants_user_id'=>$id,
            'whitelisted_wallet_addresses'=>$Users->usdt_acc,
            'req' => array('action'=>'withdrawal', 'transaction_id'=>$transaction_id),
        );

        $url = config('app.stoneUrl').'/api/user';
        $data_string = json_encode($data);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array('apikey:'.config('app.stoneAPI'),'Content-Type:application/json'));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($ch);
        $result = json_decode($result);
        // print_r($result);
        curl_close($ch);
        $access_token = $result->access_token;
        return redirect()->away(config('app.stoneUrl').'/gateway/'.$access_token);
    }
    
    
    public function wallet_buy(Request $request)
    {
        $user = \Auth::user();
        $transaction_id = rand(10,1000000000);
        $lcurrencyAmount = $request->get('lcurrency_amount');
        $data = array(
            'username'=>$user->username,
            'email'=>$user->email,
            'name'=>$user->name,
            'merchants_user_id'=>$user->id,
            'whitelisted_wallet_addresses'=>$user->usdt_acc,
            'req' => array('action'=>'buy', 'lcurrency_amount'=>$lcurrencyAmount,'transaction_id'=>$transaction_id),
        );

        $url = config('app.stoneUrl').'/api/user';
        $data_string = json_encode($data);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array('apikey:'.config('app.stoneAPI'),'Content-Type:application/json'));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($ch);
        $result = json_decode($result);
        // print_r($result);
        curl_close($ch);

        $access_token = $result->access_token;
        
        return redirect()->away(config('app.stoneUrl').'/gateway/'.$access_token);
        
    }
    public function wallet_sell($id)
    {
        $Users = DB::table('users')->where(array('id'=>$id))->first();
        $data = array(
            'username'=>$Users->username,
            'email'=>$Users->email,
            'name'=>$Users->name,
            'contact_number'=>$Users->contact_number,
            'credit_point'=>$Users->credit_point,
            'lcurrency_amount'=>$Users->lcurrency_amount,
            'merchants_user_id'=>$id,
            'whitelisted_wallet_addresses'=>$Users->whitelist_wallet_address
        );

        $url = config('app.stoneUrl').'/api/user';
        $data_string = json_encode($data);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array('apikey:'.config('app.stoneAPI'),'Content-Type:application/json'));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($ch);
        $result = json_decode($result);
        // print_r($result);
        curl_close($ch);

        $access_token = $result->access_token;
        return redirect()->away(config('app.stoneUrl').'/home/'.$access_token.'/sell');
    }
}