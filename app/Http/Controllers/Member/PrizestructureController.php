<?php

namespace App\Http\Controllers\Member;

use App\GameEntry;
use App\Http\Controllers\Controller;
use App\Setting;
use Carbon\Carbon;

class PrizestructureController extends Controller
{
    public function index()
    {
        return view('member.prizestructure');
    }
}