<?php

namespace App\Http\Controllers\Member;

use App\GameEntry;
use App\GameResult;
use App\Http\Controllers\Controller;
use App\Setting;
use App\Transaction;
use App\User;
use App\Wallet;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $gameResults = GameResult::latest()->take(50)->get();

        $userID = \Auth::id();
        $user = User::select('*')->where('id', $userID)->with('wallets')->first();

        $wallets = [];
        $walletIDs = [];
        $spinWallet = 0;
        foreach ($user->wallets as $w) {
            $walletIDs[] = $w->id;
            if ($w->type == 'SPIN') {
                $spinWallet = $w->id;
            }
            $wallets[$w->type] = ['id' => $w->id, 'balance' => $w->balance];
        }

        // Did user play the spin in the past 24hr?
        $canPlaySpin = !Transaction::where('wallet_id', $spinWallet)->where('type', 'SPIN')->whereDate('created_at', Carbon::today())->exists();

        $now = Carbon::now('Asia/Kuala_Lumpur');
        $day = $now->format('N');
        $start = $now;
        if ($day < 7) {
            $start = Carbon::now('Asia/Kuala_Lumpur')->subDays($day);
        }

        $startStr = $start->toDateString();
        $startDt = new Carbon($startStr, 'Asia/Kuala_Lumpur');

        $totalPoints = GameEntry::where('created_at', '>=', $startDt)
            ->where('created_at', '<=', $now)->where('user_id', $userID)->sum('amount') / 100;

        $spinBalance = intval($wallets['SPIN']['balance']);
        $intro = $canPlaySpin ? ($spinBalance > 0 ? __('app.spin_balance', ['balance' => $spinBalance]) : __('app.spin_none')) : __('app.spin_comeback');
        $wheelData = $this->getSpinWheelData($userID, $intro);
        $reflink = route('register', ['refcode' => $user->referralkey]);
        $refc = $user->referralkey;
        

        return view('member.dashboard', compact('gameResults', 'wallets', 'totalPoints', 'canPlaySpin', 'wheelData', 'spinBalance', 'intro', 'reflink', 'refc'));
    }

    public function getRandomWeightedElement(array $weightedValues)
    {
        $rand = mt_rand(1, (int) array_sum($weightedValues));

        foreach ($weightedValues as $key => $value) {
            $rand -= $value;
            if ($rand <= 0) {
                return $key;
            }
        }
    }

    public function spinResult(Request $request)
    {
        $input = $request->all();
        $gameId = $input['gameId'];
        $result = $input['result'];
        $decryptedData = explode("#", \Crypt::decrypt($result));
        if ($decryptedData[0] == $gameId) {
            $reward = 0;
            switch ($decryptedData[1]) {
                case '1':
                    $reward = 1000;
                    break;
                case '2':
                    $reward = 300;
                    break;
                case '3':
                    $reward = 50;
                    break;
                case '4':
                    $reward = 500;
                    break;
                case '5':
                    $reward = 100;
                    break;
                case '6':
                    $reward = 10;
                    break;
            }
            if ($reward > 0) {
                $userID = \Auth::id();

                $cacheKey = 'spin_' . $userID;
                \Cache::forget($cacheKey);
                
                $spinWallet = Wallet::where('type', 'SPIN')->where('user_id', $userID)->first();
                $spWallet = Wallet::where('type', 'SP')->where('user_id', $userID)->first();
                $tx = new Transaction();
                $tx->type = "SPIN";
                $tx->wallet_id = $spinWallet->id;
                $tx->amount = -1;
                $tx->extra = $reward;
                $tx->ref()->associate($spinWallet);
                $tx->save();

                $tx2 = new Transaction();
                $tx2->type = "SPINREWARD";
                $tx2->wallet_id = $spWallet->id;
                $tx2->amount = $reward;
                $tx2->extra = '';
                $tx2->ref()->associate($tx);
                $tx2->save();
            }
        }
    }

    public function getSpinWheelData($userID, $intro)
    {
        $cacheKey = 'spin_' . $userID;
        if (!\Cache::has($cacheKey)) {
            $spinSettings = json_decode(Setting::where('option', 'spins')->first()->val);
            $weighs = [];
            for ($i = 0; $i < count($spinSettings); $i++) {
                $weighs[($i + 1) . ''] = $spinSettings[$i];
            }
            $destination = $this->getRandomWeightedElement($weighs);
            \Cache::put($cacheKey, $destination, now()->addHours(24));
        }
        $destination = \Cache::get($cacheKey);

        $gameId = \uniqid();
        $encVal = \Crypt::encrypt($gameId . '#' . $destination);
        $data = array(
            "colorArray" => array("#ff0000", "#b05600", "#b07600", "#b09600", "#b03600", "#b01600", "#b04600", "#b03600", "#b02600", "#b01600", "#b00600", "#b00000", "#b05600", "#b05600", "#1ABC9C", "#2ECC71", "#E87AC2", "#3498DB", "#9B59B6", "#7F8C8D"),

            "segmentValuesArray" => array(
                array(
                    "type" => "string",
                    "value" => "1000",
                    "resultText" => "10",
                    "userData" => array("val" => $encVal),
                ),
                array(
                    "type" => "string",
                    "value" => "300",
                    "resultText" => "3",
                    "userData" => array("val" => $encVal),
                ),
                array(
                    "type" => "string",
                    "value" => "50",
                    "resultText" => "0.5",
                    "userData" => array("val" => $encVal),
                ),
                array(
                    "type" => "string",
                    "value" => "500",
                    "resultText" => "5",
                    "userData" => array("val" => $encVal),
                ),
                array(
                    "type" => "string",
                    "value" => "100",
                    "resultText" => "1",
                    "userData" => array("val" => $encVal),
                ),
                array(
                    "type" => "string",
                    "value" => "10",
                    "resultText" => "0.1",
                    "userData" => array("val" => $encVal),
                ),
            ),
            "svgWidth" => 1024,
            "svgHeight" => 768,
            "wheelStrokeColor" => "#ffffff",
            "wheelStrokeWidth" => 10,
            "wheelSize" => 700,
            "wheelTextOffsetY" => 80,
            "wheelTextColor" => "#EDEDED",
            "wheelTextSize" => "2.3em",
            "wheelImageOffsetY" => 40,
            "wheelImageSize" => 50,
            "centerCircleSize" => 360,
            "centerCircleStrokeColor" => "#F1DC15",
            "centerCircleStrokeWidth" => 12,
            "centerCircleFillColor" => "#EDEDED",
            "centerCircleImageUrl" => asset('assets/vendor/spin') . "/media/logo.png",
            "centerCircleImageWidth" => 400,
            "centerCircleImageHeight" => 400,
            "segmentStrokeColor" => "#E2E2E2",
            "segmentStrokeWidth" => 4,
            "centerX" => 512,
            "centerY" => 384,
            "hasShadows" => false,
            "numSpins" => 1,
            "spinDestinationArray" => array(intval($destination)),
            "minSpinDuration" => 6,
            "gameOverText" => __('app.spin_comeback'),
            "invalidSpinText" => "INVALID SPIN. PLEASE SPIN AGAIN.",
            "introText" => $intro,
            "hasSound" => true,
            "gameId" => $gameId,
            "clickToSpin" => true,
            "spinDirection" => "ccw",

        );

        return str_replace("'", "\'", json_encode($data));
    }

    
}