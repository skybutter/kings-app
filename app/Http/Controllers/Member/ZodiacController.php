<?php

namespace App\Http\Controllers\Member;

use App\GameEntry;
use App\GameResult;
use App\Http\Controllers\Controller;
use App\Jobs\Rebate;
use App\Setting;use App\Transaction;
use App\Wallet;
use \Illuminate\Http\Request;

class ZodiacController extends Controller
{
    public function index()
    {
        $gameResults = GameResult::latest()->take(50)->get();
        $maxbet = Setting::where('option', 'maxbet_2')->first()->val;

        return view('member.zodiac', compact('gameResults', 'maxbet'));
    }

    public function submit(Request $request)
    {
        if (!$request->input('valid')) {
            return back()->withError(trans('error.invalid_bet'));
        }

        $user = \Auth::user();
        $userID = $user->id;

        $extra = [];
        $total = 0;
        $message = null;
        foreach ($request->get('val') as $k => $val) {
            $amt = doubleval($val);
            if ($amt > 0) {
                $extra[] = [($k + 1) . '', $amt];
                $total += $amt;
            }
        }
        $wallet = Wallet::where('user_id', $userID)->where('type', 'CP')->first();

        if (!$wallet || $wallet->balance < $total) {
            throw \Illuminate\Validation\ValidationException::withMessages([
                'amount' => [__('validation.insufficient_funds')],
            ]);
        }

        if (count($extra) > 0) {
            $entry = new GameEntry;
            $entry->user_id = $userID;
            $entry->extra = $extra;
            $entry->status = 0;
            $entry->game = 2;
            $entry->amount = $total;
            $entry->winres = '';
            $entry->save();

            $tx = new Transaction();
            $tx->type = "GAME_2";
            $tx->wallet_id = $wallet->id;
            $tx->amount = -$total;
            $tx->ref_type = 'App\GameEntry';
            $tx->ref_id = $entry->id;
            $tx->save();

            Rebate::dispatch($user, $entry, $total);

            $message = trans('app.entry_submitted');
        } else {
            return back()->withError(trans('error.empty_bet'));
        }
        return redirect(route('member.zodiac'))->with('success', $message);
    }
}
