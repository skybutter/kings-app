<?php

namespace App\Http\Controllers\Member;

use App\GameEntry;
use App\Http\Controllers\Controller;
use App\Transaction;
use App\Wallet;

class StatementController extends Controller
{
    public function index()
    {
        $userID = \Auth::id();

        $bets = GameEntry::with('result')->where('user_id', $userID)->latest()->get();
        foreach ($bets as $bet) {
            //Translate details
            switch ($bet->game) {
                case '1':
                    $bet->details = $bet->extra[0];
                    $types = [];
                    for ($i = 1; $i < count($bet->extra); $i++) {
                        $types[] = $bet->extra[$i][0] . 'D';
                    }
                    $bet->details = $bet->details . '<br>' . implode(', ', $types);
                    if ($bet->result) {
                        $bet->resultStr = $bet->result->qianzi;
                    }

                    $bet->info = '';
                    for ($i = 1; $i < count($bet->extra); $i++) {
                        $bet->info .= $bet->extra[$i][0] . 'D : ' . $bet->extra[$i][1] . '<br>';
                    }

                    break;
                case '2':
                    $bet->details = '';
                    $types = [];
                    for ($i = 0; $i < count($bet->extra); $i++) {
                        $types[] = __('game.ZODIAC_' . $bet->extra[$i][0]);
                    }
                    $bet->details = implode(', ', $types);
                    if ($bet->result) {
                        $bet->resultStr = __('game.ZODIAC_' . $bet->result->zodiac);
                    }

                    $bet->info = '';
                    for ($i = 1; $i < count($bet->extra); $i++) {
                        $bet->info .= __('game.ZODIAC_' . $bet->extra[$i][0]) . ' : ' . $bet->extra[$i][1] . '<br>';
                    }

                    break;
                case '3':
                    $bet->details = '';
                    $types = [];
                    for ($i = 0; $i < count($bet->extra); $i++) {
                        $types[] = str_pad($bet->extra[$i][0], 2, '0', STR_PAD_LEFT);
                    }
                    $bet->details = implode(', ', $types);

                    if ($bet->result) {
                        $bet->resultStr = str_pad($bet->result->zihua, 2, '0', STR_PAD_LEFT);
                    }

                    $bet->info = '';
                    for ($i = 1; $i < count($bet->extra); $i++) {
                        $bet->info .= str_pad($bet->extra[$i][0], 2, '0', STR_PAD_LEFT) . ' : ' . $bet->extra[$i][1] . '<br>';
                    }

                    break;
                case '4':
                    $bet->details = '';
                    $types = [];
                    for ($i = 0; $i < count($bet->extra); $i++) {
                        $types[] = __('game.HOROSCOPE_' . $bet->extra[$i][0]);
                    }
                    $bet->details = implode(', ', $types);
                    if (is_array($bet->winres) && count($bet->winres) > 0) {
                        $bet->resultStr = __('game.HOROSCOPE_' . $bet->winres[0][0]);
                    }

                    $bet->info = '';
                    for ($i = 0; $i < count($bet->extra); $i++) {
                        $bet->info .= __('game.HOROSCOPE_' . $bet->extra[$i][0]) . ' : ' . $bet->extra[$i][1] . '<br>';
                    }

                    break;
            }

        }
        return view('member.statement', compact('bets'));
    }

    public function commission()
    {
        $userID = \Auth::id();
        $wallet = Wallet::where('user_id', $userID)->where('type', 'BP')->first();
        $data = Transaction::where('wallet_id', $wallet->id)->where('type', 'COMMISSION')->latest()->get();
        foreach ($data as $row) {
            //Translate details
            $extra = json_decode($row->extra, true);
            if (isset($extra['player'])) {
                $row->details = $extra['player'];
            } else {
                $row->details = __('app.bet') . ' #' . $row->ref_id;
            }
        }
        return view('member.commission', compact('data'));

    }
}
