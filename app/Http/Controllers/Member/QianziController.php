<?php

namespace App\Http\Controllers\Member;

use App\GameEntry;
use App\GameResult;
use App\Http\Controllers\Controller;
use App\Jobs\Rebate;
use App\Setting;
use App\Transaction;
use App\Wallet;
use \Illuminate\Http\Request;

class QianziController extends Controller
{
    public function index()
    {
        $gameResults = GameResult::latest()->take(50)->get();
        $qianzi_blacklistRaw = Setting::where('option', 'qianzi_blacklist')->first()->val;
        $qianzi_blacklist = '';
        if ($qianzi_blacklistRaw) {
            $qianzi_blacklist = str_replace("\r\n", ';', $qianzi_blacklistRaw);
        }
        $maxbet = Setting::where('option', 'maxbet_1')->first()->val;

        return view('member.qianzi', compact('gameResults', 'qianzi_blacklist', 'maxbet'));
    }

    public function submit(Request $request)
    {
        $num = $request->input('num');
        if (!$request->input('valid')) {
            return back()->withError(trans('error.invalid_bet'));
        }

        $bet = doubleval($request->input('bet'));
        $types = $request->input('d');
        if (strlen($num) > 3 || $bet <= 0 || !is_array($types) || count($types) == 0) {
            return back()->withError(trans('error.invalid_bet'));
        }
        $user = \Auth::user();
        $userID = $user->id;

        $wallet = Wallet::where('user_id', $userID)->where('type', 'CP')->first();

        $extra = [$num];
        foreach ($types as $k => $v) {
            $extra[] = [$k + 1, $bet];
        }
        $total = $bet * count($types);
        $message = null;

        if (!$wallet || $wallet->balance < $total) {
            throw \Illuminate\Validation\ValidationException::withMessages([
                'amount' => [__('validation.insufficient_funds')],
            ]);
        }

        if (count($extra) > 0) {
            $entry = new GameEntry;
            $entry->user_id = $userID;
            $entry->extra = $extra;
            $entry->status = 0;
            $entry->game = 1;
            $entry->amount = $total;
            $entry->winres = '';
            $entry->save();
            $message = trans('app.entry_submitted');

            $tx = new Transaction();
            $tx->type = "GAME_1";
            $tx->wallet_id = $wallet->id;
            $tx->amount = -$total;
            $tx->ref_type = 'App\GameEntry';
            $tx->ref_id = $entry->id;
            $tx->save();

            Rebate::dispatch($user, $entry, $total);

        } else {
            return back()->withError(trans('error.empty_bet'));
        }
        return redirect(route('member.qianzi'))->with('success', $message);

    }
}
