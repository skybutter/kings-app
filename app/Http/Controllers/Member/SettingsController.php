<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Setting;

class SettingsController extends Controller
{
    public function commissions()
    {
        $user = \Auth::user();
        $members = User::select('*')->where('sponsor_id', $user->id)->get();
        return view('member.settings.commissions', compact('members'));
    }

    public function updateCommission($id, Request $request)
    {
        $user = \Auth::user();
        $maxComm = $user->commission;
        $comm = doubleval($request->get('commission'));
        if($comm > $maxComm){
            return back()->with('error', __('error.comm_exceed_limit'));
        }
        $member = User::select('*')->where('sponsor_id', $user->id)->where('id', $id)->firstOrFail();
        $member->commission = $request->get('commission');
        $member->save();

        return back()->with('success', __('app.request_successful'));
    }
}
