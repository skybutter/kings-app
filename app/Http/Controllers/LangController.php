<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LangController extends Controller
{
    public function index(Request $request, $locale)
    {
        if (!in_array($locale, ['en', 'zh-CN', 'th', 'vn'])) {
            $locale = 'en';
        }
        \Session::put('locale', $locale);
        $nextUrl = url(\URL::previous());
        $parsedUrl = parse_url($nextUrl);
        if (strpos($parsedUrl['path'], '/lang') == 0) {
            $nextUrl = '/';
        }
        return redirect($nextUrl);

    }
}
