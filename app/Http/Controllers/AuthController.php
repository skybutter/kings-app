<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\TransactionIds;
use App\Wallet;
use App\User;

class AuthController extends Controller
{
    public function validate_withdrawal() 
    {

        $headers = apache_request_headers();

        if(!array_key_exists('Apikey',$headers))
        {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        
        
        $apikey = $headers['Apikey'];
        if(!$apikey)
        {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        
        
        $get_apikey = DB::table('apikeys')->where(array('apiKey'=>$apikey))->first();
        if(!$get_apikey)
        {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        
        
        $input_arr = json_decode(file_get_contents('php://input'), true);
        if(!array_key_exists('user_id',$input_arr))
        {
            return response()->json(['error' => 'User Id is mandatory'], 200);
        }
        
        
        $user_id = $input_arr['user_id'];
        if(!$user_id)
        {
            return response()->json(['error' => 'User Id is mandatory'], 200);
        }
        if(!array_key_exists('amount',$input_arr))
        {
            return response()->json(['error' => 'Amount is mandatory'], 200);
        }
        
        
        $amount = $input_arr['amount'];
        if(!array_key_exists('transaction_id',$input_arr))
        {
            return response()->json(['error' => 'Transaction Id is mandatory'], 200);
        }
        
        
        $transaction_id = $input_arr['transaction_id'];
        if(!$transaction_id)
        {
            return response()->json(['error' => 'Transaction Id is mandatory'], 200);
        }
        
        if(!$amount)
        {
            return response()->json(['error' => 'Amount is mandatory'], 200);
        }
        
        
        
        if($amount <= 0)
        {
            return response()->json(['error' => 'Amount must be greater then 0'], 200);
        }
        
        
        $check_trans = TransactionIds::checkTransactionId($user_id,$transaction_id);
        if(!$check_trans)
        {
            return response()->json(['error' => 'Invalid transaction id'], 200);
        }
        
        
        
        $user = DB::table('users')->select('id','name','username','email')->where(array('id'=>$user_id))->first();
        $wallet = Wallet::where('user_id', $user->id)->where('type', 'CP')->first();
        if(!$user)
        {
            return response()->json(['error' => 'Invalid user id'], 200);
        }
        
        
        $user_credit_point = $wallet->balance;
        if($amount > $user_credit_point)
        {
            return response()->json(['status' => 0], 200);
        }

        $update_trans_id_status = DB::table('validate_transaction_ids')->where('id',$check_trans->id)->update(array('status'=>1));
        
        return response()->json(['status' => 1], 200);
        
    }
    
    
    
    public function update_credit_point()
    {
        $headers = apache_request_headers();
        
        if(!array_key_exists('Apikey',$headers))
        {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        $apikey = $headers['Apikey'];
        
        
        if(!$apikey)
        {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        
        $get_apikey = DB::table('apikeys')->where(array('apiKey'=>$apikey))->first();
        
        if(!$get_apikey)
        {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        $input_arr = json_decode(file_get_contents('php://input'), true);

        if(!$input_arr)
        {
            return response()->json(['error' => 'Mandatory parameter missing'], 200);
        }
        
        if(!array_key_exists('type',$input_arr))
        {
            return response()->json(['error' => 'Type is mandatory'], 200);
        }
        $type = $input_arr['type'];
        
        if(!$type)
        {
            return response()->json(['error' => 'Type is mandatory'], 200);
        }
        
        if($type != "add" && $type != "deduct")
        {
            return response()->json(['error' => 'Invalid type'], 200);
        }
        
        if(!array_key_exists('user_id',$input_arr))
        {
            return response()->json(['error' => 'User Id is mandatory'], 200);
        }
        $user_id = $input_arr['user_id'];
        
        if(!$user_id)
        {
            return response()->json(['error' => 'User Id is mandatory'], 200);
        }
        
        if(!array_key_exists('amount',$input_arr))
        {
            return response()->json(['error' => 'Amount is mandatory'], 200);
        }
        $amount = $input_arr['amount'];
        
        if(!$amount)
        {
            return response()->json(['error' => 'Amount is mandatory'], 200);
        }
        
        if($amount <= 0)
        {
            return response()->json(['error' => 'Amount must be greater than 0'], 200);
        }
        
        if(!array_key_exists('lcurrency_amount',$input_arr))
        {
            return response()->json(['error' => 'CNY is mandatory'], 200);
        }
        $cnyAmount = $input_arr['lcurrency_amount'];
        
        if(!$cnyAmount)
        {
            return response()->json(['error' => 'CNY is mandatory'], 200);
        }
        
        if($cnyAmount <= 0)
        {
            return response()->json(['error' => 'CNY must be greater than 0'], 200);
        }

        $user = DB::table('users')->where(array('id'=>$user_id))->first();
        $wallet = Wallet::where('user_id', $user->id)->where('type', 'CP')->first();

        if(!$user)
        {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        $old_credit = $wallet->balance;

        if($type == "add")
        {
            $new_credit = $old_credit + $cnyAmount;
        }
        else
        {
            $new_credit = $old_credit - $cnyAmount;
        }

        DB::table('wallets')->where(array('user_id'=>$user_id))->where('type', 'CP')->update(array('balance'=>$new_credit));

        if($type == "add")
        {
            return response()->json(['success' => 'Amount added'], 200);
        }
        if($type == "deduct")
        {
            return response()->json(['success' => 'Amount deducted'], 200);
        }
    }
}
