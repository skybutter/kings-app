<?php

namespace App\Http\Controllers\Admin;

use App\Approval;
use App\Http\Controllers\Controller;
use App\Transaction;
use Illuminate\Http\Request;

class ApprovalsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function topup()
    {
        $approvals = Approval::where('type', 'TOPUP')->with('ref')->with('user')->orderBy('id', 'desc')->get()->toArray();
        foreach ($approvals as $k => $a) {
            if ($a['content']) {
                $a['content'] = json_decode($a['content'], true);
            }
            $approvals[$k] = $a;
        }
        return view('admin.topup', compact('approvals'));
    }

    public function updateTopup(Request $request, $id)
    {
        $approval = Approval::with('ref')->findOrFail($id);
        $approval->status = intval($request->get('status', 0));
        if ($approval->status == 1) {
            $content = json_decode($approval->content);

            $approval->status = 1;
            $approval->save();
            $tx = new Transaction();
            $tx->type = 'TOPUP';
            $tx->wallet()->associate($approval->ref);
            $tx->ref()->associate($approval);
            $tx->amount = $content->amount;
            $tx->save();

        } else if ($approval->status == 2) {
            $approval->status = 2;
            $approval->save();
        }
        $approval->admin_id = \Auth::id();
        $approval->save();

        return redirect(route('admin.approvals.topup'))->with('success', trans('app.request_successful'));

    }

    public function withdrawal()
    {
        $approvals = Approval::where('type', 'WITHDRAWAL')->with('ref')->with('user')->orderBy('id', 'desc')->get()->toArray();
        foreach ($approvals as $k => $a) {
            if ($a['content']) {
                $a['content'] = json_decode($a['content'], true);
            }
            $approvals[$k] = $a;
        }
        return view('admin.withdrawal', compact('approvals'));
    }

    public function updateWithdrawal(Request $request, $id)
    {
        $approval = Approval::with('ref')->findOrFail($id);
        $approval->status = intval($request->get('status', 0));
        $content = json_decode($approval->content, true);

        if ($approval->status == 1) {
            $txid = $request->get('txid', '');
            $file = $request->file('txfile');
            if (!$txid && !$file) {
                $error = \Illuminate\Validation\ValidationException::withMessages([
                    'txid' => ['Missing proof of payment'],
                ]);
                throw $error;
                return;
            }
            $content['PAYMENT_TXID'] = $txid;
            if ($file) {
                $path = str_replace('public/', '', $file->store('public/attachments'));
                $content['PAYMENT_FILE'] = $path;
            }
            $approval->content = json_encode($content);

            $approval->status = 1;
            $approval->save();

        } else if ($approval->status == 2) {
            $approval->status = 2;
            $approval->save();

            $tx = new Transaction();
            $tx->type = 'WITHDRAWAL_REVERSE';
            $tx->wallet()->associate($approval->ref);
            $tx->ref()->associate($approval);
            $tx->amount = floatval($content['amount']);
            $tx->save();

        }
        $approval->admin_id = \Auth::id();
        $approval->save();

        return redirect(route('admin.approvals.withdrawal'))->with('success', trans('app.request_submitted'));

    }

    public function withdrawalpayment($id)
    {
        return view('admin.withdrawal_payment', compact('id'));
    }

}
