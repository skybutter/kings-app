<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Transaction;
use App\User;
use App\Wallet;
use App\Setting;
use Illuminate\Http\Request;

class WalletsController extends Controller
{
    public function index(Request $request)
    {
        $wallets = [];
        if ($request->get('username')) {
            $user = User::where('username', $request->get('username'))->first();
            if ($user) {
                $wallets = Wallet::where('user_id', $user->id)->with('user')->get();
            }
        }
        $username = $request->get('username', '');
        return view('admin.wallets', compact('wallets', 'username'));
    }

    public function adjust(Request $request)
    {
        $user = \Auth::user();

        $request->validate([
            'amount' => 'numeric|required',
            'password' => 'required',
            'wallet' => 'required']);

        $amount = floatval($request->get('amount', 0));

        $setting = Setting::where('option', 'transfer_pwd')->first();
        if(strcmp($setting->val, $request->get('password')) != 0){
            throw \Illuminate\Validation\ValidationException::withMessages([
                'password' => [__('validation.invalid')],
            ]); 
        } 

        $tx = new Transaction();
        $tx->type = 'ADMIN';
        $tx->wallet_id = $request->get('wallet');
        $tx->amount = $amount;
        $tx->ref()->associate($user);
        $tx->save();
        return redirect(route('admin.wallets') . '?email=chooikw@gmail.com')->with('success', trans('app.request_successful'));
    }

}
