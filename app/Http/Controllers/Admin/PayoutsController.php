<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Jobs\InterestPayouts;
use App\Payout;
use App\Rank;
use App\Setting;
use App\Transaction;
use App\User;
use Illuminate\Http\Request;
use Carbon\Carbon;

class PayoutsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $payouts = Payout::orderBy('id', 'desc')->get()->toArray();
        return view('admin.payouts.index', compact('payouts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $payout = Payout::findOrFail($id)->toArray();
        $transactions = Transaction::with(['wallet', 'wallet.user'])->whereIn('type', ['ROI', 'Group_Bonus'])->where('ref_id', $id)->where('ref_type', 'App\Payout')->orderBy('id', 'desc')->get()->toArray();
        $interests = [];
        $overrides = [];
        $sponsors = [];

        $dt = new Carbon($payout['created_at']);
        $dt = $dt->startOfDay();
        //$dt = $dt->subDays(1);
        $sponsors = Transaction::with(['wallet', 'wallet.user'])
        ->where('type', 'Referral_Bonus')
        ->where(\DB::raw("DATE(created_at)"), $dt->format('Y-m-d'))
        ->get()->toArray();

        $payout['sponsor'] = 0;
        foreach($sponsors as $t){
            $payout['sponsor'] = $payout['sponsor'] + doubleval($t['amount']);
        }

        $ranks = [];
        $ranksRaw = Rank::all();
        foreach ($ranksRaw as $r) {
            $ranks[strval($r->id)] = $r;
        }

        foreach ($transactions as $t) {
            switch ($t['type']) {
                case 'ROI':
                    $interests[] = $t;
                    break;
                case 'Group_Bonus':
                    $overrides[] = $t;
                    break;
                case 'Referral_Bonus':
                    $sponsors[] = $t;
                    break;
            }
        }

        return view('admin.payouts.show', compact('payout', 'interests', 'overrides', 'sponsors'));
    }

    public function create()
    {
        $members = User::active()->select(['id', 'username', 'has_grouprebate'])->where('role', 'member')->get()->toArray();
        return view('admin.payouts.create', compact('members'));
    }

    public function store(Request $request)
    {
        $request->validate(['rate' => 'required|numeric']);

        $payout = new Payout();
        $payout->rate = floatval($request->get('rate'));
        $payout->extra = Setting::where('option', 'felta_ratio')->first()->val;
        $payout->admin_id = \Auth::id();

        $payout->rebate_rate = floatval($request->get('rebate'));
        $rebateMembers = $request->get('rebatemembers');
        $payout->rebate_extra = implode(',', $rebateMembers ? $rebateMembers : []);

        $payout->save();

        $rebateMembersAry = explode(',', $payout->rebate_extra);
        User::whereIn('id', $rebateMembersAry)->update(['has_grouprebate' => true]);
        User::whereNotIn('id', $rebateMembersAry)->update(['has_grouprebate' => false]);

        InterestPayouts::dispatch($payout);

        return redirect(route('admin.payouts.index'))->with('success', 'Payout has been initiated');
    }

}
