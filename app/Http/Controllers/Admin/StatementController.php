<?php

namespace App\Http\Controllers\Admin;

use App\GameEntry;
use App\Http\Controllers\Controller;

class StatementController extends Controller
{

    public function index()
    {

        $totalbetamount = GameEntry::with('amount')
            ->where('user_id', '>', '5')
            ->where('user_id', '!=', '7')
            ->sum('amount');

        $game1betamount = GameEntry::with('amount')
            ->where('game', 1)
            ->where('user_id', '>', '5')
            ->where('user_id', '!=', '7')
            ->sum('amount');

        $game2betamount = GameEntry::with('amount')
            ->where('game', 2)
            ->where('user_id', '>', '5')
            ->where('user_id', '!=', '7')
            ->sum('amount');

        $game3betamount = GameEntry::with('amount')
            ->where('game', 3)
            ->where('user_id', '>', '5')
            ->where('user_id', '!=', '7')
            ->sum('amount');

        $game4betamount = GameEntry::with('amount')
            ->where('game', 4)
            ->where('user_id', '>', '5')
            ->where('user_id', '!=', '7')
            ->sum('amount');

        $topplayer = GameEntry::with('amount', 'user_id')
            ->sum('amount', 'user_id');

        $bets = GameEntry::with('result')->with('user')->latest()->get();
        foreach ($bets as $bet) {
            //Translate details
            switch ($bet->game) {
                case '1':
                    $bet->details = $bet->extra[0];
                    $types = [];
                    for ($i = 1; $i < count($bet->extra); $i++) {
                        $types[] = $bet->extra[$i][0] . 'D';
                    }
                    $bet->details = $bet->details . '<br>' . implode(', ', $types);
                    if ($bet->result) {
                        $bet->resultStr = $bet->result->qianzi;
                    }

                    $bet->info = '';
                    for ($i = 1; $i < count($bet->extra); $i++) {
                        $bet->info .= $bet->extra[$i][0] . 'D : ' . $bet->extra[$i][1] . '<br>';
                    }

                    break;
                case '2':
                    $bet->details = '';
                    $types = [];
                    for ($i = 0; $i < count($bet->extra); $i++) {
                        $types[] = __('game.ZODIAC_' . $bet->extra[$i][0]);
                    }
                    $bet->details = implode(', ', $types);
                    if ($bet->result) {
                        $bet->resultStr = __('game.ZODIAC_' . $bet->result->zodiac);
                    }

                    $bet->info = '';
                    for ($i = 1; $i < count($bet->extra); $i++) {
                        $bet->info .= __('game.ZODIAC_' . $bet->extra[$i][0]) . ' : ' . $bet->extra[$i][1] . '<br>';
                    }

                    break;
                case '3':
                    $bet->details = '';
                    $types = [];
                    for ($i = 0; $i < count($bet->extra); $i++) {
                        $types[] = str_pad($bet->extra[$i][0], 2, '0', STR_PAD_LEFT);
                    }
                    $bet->details = implode(', ', $types);

                    if ($bet->result) {
                        $bet->resultStr = str_pad($bet->result->zihua, 2, '0', STR_PAD_LEFT);
                    }

                    $bet->info = '';
                    for ($i = 1; $i < count($bet->extra); $i++) {
                        $bet->info .= str_pad($bet->extra[$i][0], 2, '0', STR_PAD_LEFT) . ' : ' . $bet->extra[$i][1] . '<br>';
                    }

                    break;
                case '4':
                    $bet->details = '';
                    $types = [];
                    for ($i = 0; $i < count($bet->extra); $i++) {
                        $types[] = __('game.HOROSCOPE_' . $bet->extra[$i][0]);
                    }
                    $bet->details = implode(', ', $types);
                    if (is_array($bet->winres) && count($bet->winres) > 0) {
                        $bet->resultStr = __('game.HOROSCOPE_' . $bet->winres[0][0]);
                    }

                    $bet->info = '';
                    for ($i = 0; $i < count($bet->extra); $i++) {
                        $bet->info .= __('game.HOROSCOPE_' . $bet->extra[$i][0]) . ' : ' . $bet->extra[$i][1] . '<br>';
                    }

                    break;
            }

        }
        return view('admin.admin_statement', compact('bets', 'totalbetamount', 'game1betamount', 'game2betamount', 'game3betamount', 'game4betamount', 'topplayer'));
    }

}
