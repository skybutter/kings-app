<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Setting;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    public function index()
    {
        $setting = Setting::where('option', 'payment_methods')->first();

        $paymentMethodsStr = $setting->val;

        $paymentMethods = json_decode($paymentMethodsStr, true);
        $paymentMethodsAry = [];
        foreach ($paymentMethods as $k => $v) {
            $paymentMethodsAry[] = (isset($v['currency']) ? $v['currency'] : '') . '##' . (isset($v['value']) ? $v['value'] : '') . '##' . (isset($v['details']) ? $v['details'] : '');
        }

        $paymentMethodsInput = implode(";\r\n", $paymentMethodsAry);

        return view('admin.settings', compact('paymentMethodsInput'));
    }

    public function update(Request $request)
    {
        $paymentsInput = $request->get('paymentMethods');
        $paymentsRaw = explode(';', $paymentsInput);
        $paymentMethods = [];
        foreach ($paymentsRaw as $payment) {
            $payment = trim($payment);
            if (strlen($payment) == 0) {
                continue;
            }
            $components = explode('##', $payment);
            if (count($components) == 3) {
                $paymentMethods[] = ['currency' => $components[0], 'value' => $components[1], 'details' => $components[2]];
            }

        }
        Setting::where('option', 'payment_methods')
            ->update(['val' => json_encode($paymentMethods)]);

        $pwd = $request->get('transfer_pwd');
        if (strlen($pwd) > 0) {
            Setting::where('option', 'transfer_pwd')
                ->update(['val' => $pwd]);
        }

        return redirect(route('admin.settings'))->with('success', __('app.request_successful'));
    }

    public function commissions()
    {
        $settings = Setting::where('option', 'like', 'comm_%')->get()->toArray();

        $commissions = [];
        foreach ($settings as $comm) {
            $commissions[$comm['option']] = $comm['val'];
        }

        return view('admin.settings_commissions', compact('commissions'));
    }

    public function updateCommissions(Request $request)
    {
        $commissions = $request->get('commissions');

        foreach ($commissions as $k => $val) {
            Setting::where('option', $k)
                ->update(['val' => $val]);

        }

        return redirect(route('admin.settings.commissions'))->with('success', __('app.request_successful'));
    }

    public function pool()
    {
        $pool = Setting::where('option', 'pool')->first()->val;

        $poolTotal = Setting::where('option', 'poolTotal')->first()->val;

        $poolVals = json_decode(Setting::where('option', 'poolVals')->first()->val);

        return view('admin.settings_pool', compact('poolTotal', 'pool', 'poolVals'));
    }

    public function updatePool(Request $request)
    {
        Setting::where('option', 'pool')
            ->update(['val' => $request->get('pool')]);

        $poolVals = $request->get('poolVals');
        $newPoolVals = [];
        foreach ($poolVals as $v) {
            $newPoolVals[] = $v ? $v : '';
        }

        Setting::where('option', 'poolTotal')
            ->update(['val' => $request->get('poolTotal') ? $request->get('poolTotal') : '']);

        Setting::where('option', 'poolVals')
            ->update(['val' => json_encode($newPoolVals)]);

        return redirect(route('admin.settings.pool'))->with('success', __('app.request_successful'));

    }

    public function game()
    {
        $maxbet = [];
        $maxbetRaw = Setting::where('option', 'like', 'maxbet_%')->get();
        foreach ($maxbetRaw as $v) {
            $k = str_replace('maxbet_', '', $v->option);
            $maxbet[$k . ''] = $v->val;
        }
        $blacklist = Setting::where('option', 'qianzi_blacklist')->first()->val;

        $spinSettings = Setting::where('option', 'spins')->first();
        $spins = json_decode($spinSettings->val);

        $horoscopeWinrate = Setting::where('option', 'horoscope_winrate')->first()->val;

        return view('admin.settings_game', compact('maxbet', 'blacklist', 'horoscopeWinrate', 'spins', 'horoscopeWinrate'));
    }

    public function updateGame(Request $request)
    {
        $maxbets = $request->get('maxbet');

        foreach ($maxbets as $k => $v) {
            Setting::where('option', 'maxbet_' . $k)
                ->update(['val' => $v ? $v : '']);
        }

        Setting::where('option', 'qianzi_blacklist')
            ->update(['val' => $request->get('qianzi_blacklist') ? $request->get('qianzi_blacklist') : '']);

        Setting::where('option', 'horoscope_winrate')
            ->update(['val' => intval($request->get('horoscope_winrate'))]);

        $spinsInput = $request->get('spins');
        $spins = array_map('intval', $spinsInput);
        Setting::where('option', 'spins')
            ->update(['val' => json_encode($spins)]);

        Setting::where('option', 'qianzi_blacklist')
            ->update(['val' => $request->get('qianzi_blacklist') ? $request->get('qianzi_blacklist') : '']);

        return redirect(route('admin.settings.game'))->with('success', __('app.request_successful'));

    }
}
