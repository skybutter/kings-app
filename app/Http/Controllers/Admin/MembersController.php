<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class MembersController extends Controller
{
    public function index()
    {
        $members = User::all();
        $totalmembers = User::max('id');
        $B = User::all('sponsor_id');
        $A = User::get('username')->where('id' , 'like' , '11');
        return view('admin.members', compact('members','totalmembers','A'));
    }
    

   
   
   
   
    
    public function update(Request $request, $id)
    {
        $level = $request->get('level');
        $user = User::findOrFail($id);
        $user->level = $level;
        $user->save();

        if ($level > 0) { //Auto upgrade parents
            $parentId = $user->sponsor_id;
            if ($parentId) {
                while (true) {
                    $parent = User::find($parentId);
                    if ($parent) {
                        if ($parent->level <= $level && $level < 2) {
                            $parent->level = $level + 1;
                            $parent->save();
                        }
                        $parentId = $parent->sponsor_id;
                        if (!$parentId) {
                            break;
                        }
                    } else {
                        break;
                    }
                }
            }

        }
        return redirect(route('admin.members'))->with('success', trans('app.request_successful'));

    }

}
