<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Plan;
use Illuminate\Http\Request;

class PlansController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $plans = Plan::all();
        return view('admin.plans.index', compact('plans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $priceTypes = $this->priceTypes();
        return view('admin.plans.create', compact('priceTypes'));
    }

    protected function priceTypes(){
        return ['ETH', 'FELTA'];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate($this->rules(), $this->messages());
        $plan = new Plan([
            'price' => $request->get('price'),
            'price_type' => 'SP',
            'label' => $request->get('label'),
            'user_comm' => $request->get('user_comm'),
            'company_comm' => 0,//$request->get('company_comm'),
            'comm_limit' => 9999999,//$request->get('comm_limit'),
            'user_bonus' => 0,//$request->get('user_bonus'),
            'company_bonus' => 0,//$request->get('company_bonus'),
            'bonus_limit' => 0,
            'bonus' => $request->get('bonus'),
            'bonus_level' => $request->get('bonus_level'),
        ]);
        $plan->save();
        return redirect(route('admin.plans.index'))->with('success', __('app.plan_created'));
    }

    public function rules()
    {
        return [
            'price' => 'required|numeric',
            'label' => 'required',
            'user_comm' => 'required',
            // 'company_comm' => 'required',
            // 'user_bonus' => 'required',
            // 'company_bonus' => 'required',
            // 'comm_limit' => 'required',
            'bonus' => 'required',
            'bonus_level' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'user_comm.required' => "The investor roi field is required.",
            'company_comm.required' => "The company roi field is required.",
            'user_bonus.required' => "The investor bonus field is required.",
            'company_bonus.required' => "The company bonus field is required.",
            'comm_limit.required' => "The roi limit field is required.",
            'bonus.required' => "The sponsor field is required.",
            'bonus_level.required' => "The group bonus field is required.",
        ];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $plan = Plan::findOrFail($id);
        return view('admin.plans.edit', compact('plan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $plan = Plan::findOrFail($id);
        $request->validate($this->rules(), $this->messages());
        $plan->fill($request->all())->save();
        return redirect(route('admin.plans.index'))->with('success', __('app.plan_updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $plan = Plan::findOrFail($id);
        $plan->delete();
        return redirect(route('admin.plans.index'))->with('success', __('app.plan_deleted'));
    }
}
