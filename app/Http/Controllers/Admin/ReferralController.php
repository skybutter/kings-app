<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Transaction;
use App\User;
use App\Wallet;

class ReferralController extends Controller
{
    public function index()
    {
        $user = \Auth::user();
        $downlines = $user->getDownlines();
        $reflink = route('register', ['refcode' => $user->referralkey]);

        //BP wallet
        $bpWallet = Wallet::where('user_id', $user->id)->where('type', 'BP')->first();

        foreach ($downlines as $row) {
            $row->commission = Transaction::where('type', 'COMMISSION')->where('wallet_id', $bpWallet->id)->where('extra', 'like', '%' . $row->username . '%')->sum('amount');
        }

        return view('admin.referral', compact('downlines', 'reflink'));
    }

    public function tree()
    {

        $rootTree = [];
        $user = \Auth::user();
        $reflink = route('register', ['refcode' => $user->referralkey]);

        $validNames = [$user->username];
        $membersRaw = User::where('parents', 'like', '%,' . $user->id . ',%')->orderBy('id', 'asc')->get();
        $members = [];
        foreach ($membersRaw as $m) {
            $members[] = ['uid' => $m->id, 'id' => 'node-' . $m->username, 'username' => $m->username, 'text' => $m->username . ' (' . __('level.LEVEL_' . $m->level) . ')', 'sponsor_id' => $m->sponsor_id];
            $validNames[] = $m->username;
        }
        $members = array_merge([['uid' => $user->id, 'id' => 'node-' . $user->username, 'username' => $user->username, 'text' => $user->username . ' (' . __('level.LEVEL_' . $user->level) . ')', 'sponsor_id' => 0]], $members);

        $links = array();
        foreach ($members as $m) {
            $links[$m['sponsor_id']][] = $m;
        }

        $tree = $this->createTree($links, array($members[0]));
        $rootTree = json_encode($tree);
        $rootTree = trim(trim($rootTree, '['), ']');
        $username = $user->username;

        $validNames = json_encode($validNames);

        return view('admin.referral_tree', compact('reflink', 'rootTree', 'username', 'validNames'));
    }

    protected function createTree(&$list, $parent)
    {
        $tree = array();
        foreach ($parent as $k => $l) {
            if (isset($list[$l['uid']])) {
                $l['children'] = $this->createTree($list, $list[$l['uid']]);
            }
            $tree[] = $l;
        }
        return $tree;
    }

}