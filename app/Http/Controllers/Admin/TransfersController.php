<?php

namespace App\Http\Controllers\Admin;

use App\Approval;
use App\Http\Controllers\Controller;
use App\Setting;
use App\Transaction;
use App\Transfer;
use App\User;
use App\Wallet;
use Illuminate\Http\Request;

class TransfersController extends Controller
{
    public function index()
    {
        $records = Transfer::with(['toWallet', 'toWallet.user', 'fromWallet', 'fromWallet.user'])->orderBy('id', 'desc')->get()->toArray();
        return view('admin.transfers', compact('records'));
    }

}
