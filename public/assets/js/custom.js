function nextOpen() {
    var now = new Date();

    now.setMilliseconds(0);
    now.setSeconds(0);
    now.setMinutes(Math.ceil((now.getMinutes() + 1) / 10) * 10);
    return now;
}
