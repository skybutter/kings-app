<?php

use App\Events\UserCreated;
use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'email' => 'chooikw@gmail.com',
            'name' => 'Admin',
            'username' => 'admin',
            'role' => 'admin',
            'status' => 1,
            'level' => 0,
            'commission' => 0,
            'password' => bcrypt('admin'),
        ]);
        event(new UserCreated($user, 'admin'));

    }
}
