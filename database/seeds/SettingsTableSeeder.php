<?php

use App\Setting;
use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Setting::create([
            'option' => 'comm_0',
            'val' => '5',
        ]);

        Setting::create([
            'option' => 'comm_1',
            'val' => '10',
        ]);

        Setting::create([
            'option' => 'comm_2',
            'val' => '15',
        ]);

        Setting::create([
            'option' => 'comm_max',
            'val' => '15',
        ]);

        Setting::create([
            'option' => 'comm_extra',
            'val' => '0.5',
        ]);

        Setting::create([
            'option' => 'pool',
            'val' => '5',
        ]);

        // Qianzi
        Setting::create([
            'option' => 'qianzi_odds',
            'val' => '[6,60,660]',
        ]);

        Setting::create([
            'option' => 'zodiac_odds',
            'val' => '7',
        ]);

        Setting::create([
            'option' => 'zihua_odds',
            'val' => '36',
        ]);

        Setting::create([
            'option' => 'payment_methods',
            'val' => '[{"value":"bank","details":"MAYBANK 1234567 7starsnet"},{"value":"usdt","details":"123123123"}]',
        ]);

        Setting::create([
            'option' => 'poolTotal',
            'val' => '',
        ]);

        Setting::create([
            'option' => 'poolVals',
            'val' => '["","","","","","",""]',
        ]);

    }
}
