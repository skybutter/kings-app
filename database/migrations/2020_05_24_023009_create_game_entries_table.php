<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGameEntriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('game_entries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->smallInteger('game');
            $table->bigInteger('user_id')->unsigned();
            $table->smallInteger('status');
            $table->decimal('amount', 24, 2)->default(0);
            $table->decimal('winnings', 24, 2)->default(0);
            $table->text('winres')->nullable();
            $table->text('extra')->nullable();
            $table->bigInteger('winresult_id')->unsigned()->nullable();
            $table->timestamps();
        });
        Schema::table('game_entries', function (Blueprint $table) {
            $table->foreign('winresult_id')->references('id')->on('game_results')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('game_entries');
    }
}
