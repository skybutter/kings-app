<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Setting;
use App\User;

class AddCommissionToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->decimal('commission', 12,2);
        });
        // Set for all current users
        $commissionSettings = Setting::where('option', 'like', 'comm_%')->get()->toArray();
        $commissions = [];
        $maxComm = 0;
        $extraComm = 0;
        foreach ($commissionSettings as $comm) {
            switch ($comm['option']) {
                case 'comm_max':
                    $maxComm = $comm['val'];
                    break;
                case 'comm_extra':
                    $extraComm = $comm['val'];
                    break;
                default:
                    $commissions[$comm['option']] = $comm['val'];
                    break;
            }
        }
        $users = User::all();
        foreach($users as $user){
            $user->commission = $commissions['comm_'.$user->level];
            $user->save();
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('commission');
        });
    }
}
