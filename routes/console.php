<?php

use App\Jobs\GameResultReward;
use App\Jobs\ScanETH;
use Illuminate\Support\Facades\Hash;
/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
 */

Artisan::command('scaneth', function () {
    ScanETH::dispatch();
})->describe('Scan ETH');

Artisan::command('gamereward {hash} {ts}', function ($hash, $ts) {
    GameResultReward::dispatch($hash, $ts);
})->describe('Manual process game reward');

Artisan::command('rebate', function () {
    App\Jobs\Rebate::dispatch(App\User::find(7), App\GameEntry::find(1), 300);
})->describe('Test rebate');

Artisan::command('pool', function () {
    App\Jobs\Pool::dispatch();
})->describe('Weekly Pool');

Artisan::command('test', function () {
    echo Hash::make('admin').'##';
})->describe('Scan ETH');
