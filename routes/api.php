<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('get_credit_point/{id}', 'AuthController@get_credit_point');
Route::post('validate_withdrawal/', 'AuthController@validate_withdrawal');
Route::post('update_credit_point', 'AuthController@update_credit_point');