<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
Auth::routes();

Route::get('/', function () {
    return redirect('/login');
});

Route::get('/lang/{locale?}', 'LangController@index');

Route::get('/home', 'HomeController@index');



Route::group(['namespace' => 'Member', 'prefix' => 'member', 'middleware' => ['auth', 'acl'], 'acl' => ['isMember', 'isSys', 'isAdmin']], function () {
    Route::get('/dashboard', 'DashboardController@index')->name('member.dashboard');
    Route::get('/qianzi', 'QianziController@index')->name('member.qianzi');
    Route::post('/qianzi', 'QianziController@submit')->name('member.qianzi_submit');
    Route::post('/spinresult', 'DashboardController@spinResult')->name('member.spinresult');
    
    Route::get('/wallet/deposit/{id}','WalletController@wallet_deposit')->name('wallet.deposit');
    Route::get('/wallet/withdraw/{id}','WalletController@wallet_withdraw')->name('wallet.withdraw');
    Route::post('/wallet/buy','WalletController@wallet_buy')->name('wallet.buy');
    //Route::get('/wallet/sell/{id}','WalletController@wallet_sell')->name('wallet.sell');

    Route::get('/zodiac', 'ZodiacController@index')->name('member.zodiac');
    Route::post('/zodiac', 'ZodiacController@submit')->name('member.zodiac_submit');

    Route::get('/zihua', 'ZihuaController@index')->name('member.zihua');
    Route::post('/zihua', 'ZihuaController@submit')->name('member.zihua_submit');

    Route::get('/horoscope', 'HoroscopeController@index')->name('member.horoscope');
    Route::post('/horoscope', 'HoroscopeController@submit')->name('member.horoscope_submit');

    Route::get('/wallet', 'WalletController@index')->name('member.wallet');
    Route::post('/wallet/topup', 'WalletController@topup')->name('member.wallet.topup');
    Route::post('/wallet/convert', 'WalletController@convert')->name('member.wallet.convert');
    Route::post('/wallet/convertBP', 'WalletController@convertBP')->name('member.wallet.convertBP');
    Route::post('/wallet/transfer', 'WalletController@transfer')->name('member.wallet.transfer');
    Route::post('/wallet/withdraw', 'WalletController@withdraw')->name('member.wallet.withdraw');

    Route::post('/upload', 'UploadController@index')->name('member.upload');

    Route::get('/statement', 'StatementController@index')->name('member.statement');
    Route::get('/statement/commission', 'StatementController@commission')->name('member.statement.commission');

    Route::get('/pool', 'PoolController@index')->name('member.pool');
    Route::get('/prizestructure', 'PrizestructureController@index')->name('member.prizestructure');
    Route::get('/referral', 'ReferralController@index')->name('member.referral');
    Route::get('/referral/tree', 'ReferralController@tree')->name('member.referral.tree');
    
    Route::get('/profile', 'ProfileController@index')->name('member.profile');
    Route::PATCH('/profile/update', 'ProfileController@update')->name('member.profile.update');
    
    Route::group(['prefix' => 'mailbox'], function () {
        Route::get('/', 'MailboxController@inbox')->name('member.mailbox.inbox');
        Route::get('/delete/{id}', 'MailboxController@delete')->name('member.mailbox.delete');
        Route::get('/mail/{id}', 'MailboxController@show')->name('member.mailbox.show');
        Route::get('/outbox', 'MailboxController@outbox')->name('member.mailbox.outbox');
        Route::get('/compose', 'MailboxController@compose')->name('member.mailbox.compose');
        Route::post('/send', 'MailboxController@send')->name('member.mailbox.send');

        Route::get('/announcements', 'MailboxController@announcements')->name('member.mailbox.announcements');
    });

    Route::get('/settings/commissions', 'SettingsController@commissions')->name('member.settings.commissions');
    Route::PATCH('/settings/commissions/{id}', 'SettingsController@updateCommission')->name('member.settings.updateCommission');

});

Route::group(['namespace' => 'Admin', 'middleware' => ['auth', 'acl'], 'acl' => ['isAdmin'], 'prefix' => 'admin'], function () {
    Route::get('/dashboard', 'DashboardController@index')->name('admin.dashboard');
    Route::get('/wallets', 'WalletsController@index')->name('admin.wallets');
    Route::post('/wallets/adjust', 'WalletsController@adjust')->name('admin.wallets.adjust');

    Route::get('/approvals/topup', 'ApprovalsController@topup')->name('admin.approvals.topup');
    Route::PATCH('/approvals/topup/{id}', 'ApprovalsController@updateTopup')->name('admin.approvals.topup_update');
    Route::get('/approvals/withdrawal', 'ApprovalsController@withdrawal')->name('admin.approvals.withdrawal');
    Route::PATCH('/approvals/withdrawal/{id}', 'ApprovalsController@updateWithdrawal')->name('admin.approvals.withdrawal_update');
    Route::get('/approvals/withdrawal_payment/{id}', 'ApprovalsController@withdrawalPayment')->name('admin.approvals.withdrawalpayment');

    Route::get('/members', 'MembersController@index')->name('admin.members');
    Route::PATCH('/members/update/{id}', 'MembersController@update')->name('admin.members.update');
    
    Route::get('/admin_statement', 'StatementController@index')->name('admin.admin_statement');

    Route::get('/settings', 'SettingsController@index')->name('admin.settings');
    Route::PATCH('/settings/update', 'SettingsController@update')->name('admin.settings.update');
    Route::get('/settings/commissions', 'SettingsController@commissions')->name('admin.settings.commissions');
    Route::PATCH('/settings/updateCommission', 'SettingsController@updateCommissions')->name('admin.settings.updateCommissions');

    Route::get('/settings/pool', 'SettingsController@pool')->name('admin.settings.pool');
    Route::PATCH('/settings/updatePool', 'SettingsController@updatePool')->name('admin.settings.updatePool');

    Route::get('/settings/game', 'SettingsController@game')->name('admin.settings.game');
    Route::PATCH('/settings/updateGame', 'SettingsController@updateGame')->name('admin.settings.updateGame');
    
    Route::get('/referral', 'ReferralController@index')->name('admin.referral');
    Route::get('/referral/tree', 'ReferralController@tree')->name('admin.referral.tree');


    Route::group(['prefix' => 'mailbox'], function () {
        Route::get('/', 'MailboxController@inbox')->name('admin.mailbox.inbox');
        Route::get('/delete/{id}', 'MailboxController@delete')->name('admin.mailbox.delete');
        Route::get('/mail/{id}', 'MailboxController@show')->name('admin.mailbox.show');
        Route::get('/outbox', 'MailboxController@outbox')->name('admin.mailbox.outbox');
        Route::get('/compose', 'MailboxController@compose')->name('admin.mailbox.compose');
        Route::post('/send', 'MailboxController@send')->name('admin.mailbox.send');

        Route::get('/announcements', 'MailboxController@announcements')->name('admin.mailbox.announcements');
    });

});

Route::get('qrcode', function () {
    return QrCode::size(250)
        ->backgroundColor(255, 255, 204)
        ->generate('MyNotePaper');
});
